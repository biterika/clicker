// window.scrollTo(0, 500);
// page.scrollPosition = { top: page.scrollPosition + 500, left: 0 };
// document.body.scrollTop = 300
// document.documentElement.scrollTop = 300;
//Firefox
$('#elem').bind('DOMMouseScroll', function(e){
    if(e.detail > 0) {
        //scroll down
        console.log('Down');
    }else {
        //scroll up
        console.log('Up');
    }

    //prevent page fom scrolling
    return false;
});

//IE, Opera, Safari
$('#elem').bind('mousewheel', function(e){
    if(e.wheelDelta< 0) {
        //scroll down
        console.log('Down');
    }else {
        //scroll up
        console.log('Up');
    }

    //prevent page fom scrolling
    return false;
});