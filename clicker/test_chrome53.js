// ../dist/u16_iv3/phantomjs test_chrome53.js http://www2.dev

'use strict';

var random_thumb = require('./actions/random_thumb').exec;

var page_id_counter = 1;

phantom.onError = function (msg, trace) {
    if (msg) console.log('phantom.onError, msg: ' + JSON.stringify(msg));
    if (trace) console.log('phantom.onError, trace: ' + JSON.stringify(trace));
};


var navigator_obj = {
    "vendorSub":                   "",
    "productSub":                  "20030107",
    "vendor":                      "Google Inc.",
    "maxTouchPoints":              "0",
    "hardwareConcurrency":         "4",
    "appCodeName":                 "Mozilla",
    "appName":                     "Netscape",
    "appVersion":                  "5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36",
    "platform":                    "MacIntel",
    "product":                     "Gecko",
    "userAgent":                   "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36",
    "language":                    "ru",
    "languages":                   {
        "0": "ru",
        "1": "th"
    },
    "onLine":                      "true",
    "cookieEnabled":               "true",
    "doNotTrack":                  "nullValue",
    "geolocation":                 {
        "getCurrentPosition": "functionValue",
        "watchPosition":      "functionValue",
        "clearWatch":         "functionValue"
    },
    "plugins":                     {
        "0":         {
            "0":           {
                "type":        "application/x-ppapi-widevine-cdm",
                "suffixes":    "",
                "description": "Widevine Content Decryption Module"
            },
            "name":        "Widevine Content Decryption Module",
            "filename":    "widevinecdmadapter.plugin",
            "description": "Enables Widevine licenses for playback of HTML audio/video content. (version: 1.4.8.903)",
            "length":      "1",
            "item":        "functionValue",
            "namedItem":   "functionValue"
        },
        "1":         {
            "0":           {
                "type":        "application/x-shockwave-flash",
                "suffixes":    "swf",
                "description": "Shockwave Flash"
            },
            "1":           {
                "type":        "application/futuresplash",
                "suffixes":    "spl",
                "description": "Shockwave Flash"
            },
            "name":        "Shockwave Flash",
            "filename":    "PepperFlashPlayer.plugin",
            "description": "Shockwave Flash 23.0 r0",
            "length":      "2",
            "item":        "functionValue",
            "namedItem":   "functionValue"
        },
        "2":         {
            "0":           {
                "type":        "application/pdf",
                "suffixes":    "pdf",
                "description": ""
            },
            "name":        "Chrome PDF Viewer",
            "filename":    "mhjfbmdgcfjbbpaeojofohoefgiehjai",
            "description": "",
            "length":      "1",
            "item":        "functionValue",
            "namedItem":   "functionValue"
        },
        "3":         {
            "0":           {
                "type":        "application/x-nacl",
                "suffixes":    "",
                "description": "Native Client Executable"
            },
            "1":           {
                "type":        "application/x-pnacl",
                "suffixes":    "",
                "description": "Portable Native Client Executable"
            },
            "name":        "Native Client",
            "filename":    "internal-nacl-plugin",
            "description": "",
            "length":      "2",
            "item":        "functionValue",
            "namedItem":   "functionValue"
        },
        "4":         {
            "0":           {
                "type":        "application/x-google-chrome-pdf",
                "suffixes":    "pdf",
                "description": "Portable Document Format"
            },
            "name":        "Chrome PDF Viewer",
            "filename":    "internal-pdf-viewer",
            "description": "Portable Document Format",
            "length":      "1",
            "item":        "functionValue",
            "namedItem":   "functionValue"
        },
        "length":    "5",
        "item":      "functionValue",
        "namedItem": "functionValue",
        "refresh":   "functionValue"
    },
    "mimeTypes":                   {
        "0":         {
            "type":        "application/x-ppapi-widevine-cdm",
            "suffixes":    "",
            "description": "Widevine Content Decryption Module"
        },
        "1":         {
            "type":        "application/x-shockwave-flash",
            "suffixes":    "swf",
            "description": "Shockwave Flash"
        },
        "2":         {
            "type":        "application/futuresplash",
            "suffixes":    "spl",
            "description": "Shockwave Flash"
        },
        "3":         {
            "type":        "application/pdf",
            "suffixes":    "pdf",
            "description": ""
        },
        "4":         {
            "type":        "application/x-nacl",
            "suffixes":    "",
            "description": "Native Client Executable"
        },
        "5":         {
            "type":        "application/x-pnacl",
            "suffixes":    "",
            "description": "Portable Native Client Executable"
        },
        "6":         {
            "type":        "application/x-google-chrome-pdf",
            "suffixes":    "pdf",
            "description": "Portable Document Format"
        },
        "length":    "7",
        "item":      "functionValue",
        "namedItem": "functionValue"
    },
    "webkitTemporaryStorage":      {
        "queryUsageAndQuota": "functionValue",
        "requestQuota":       "functionValue"
    },
    "webkitPersistentStorage":     {
        "queryUsageAndQuota": "functionValue",
        "requestQuota":       "functionValue"
    },
    "serviceWorker":               {
        "controller":          "nullValue",
        "ready":               {},
        "oncontrollerchange":  "nullValue",
        "onmessage":           "nullValue",
        "register":            "functionValue",
        "getRegistration":     "functionValue",
        "getRegistrations":    "functionValue",
        "addEventListener":    "functionValue",
        "removeEventListener": "functionValue",
        "dispatchEvent":       "functionValue"
    },
    "getBattery":                  "functionValue",
    "sendBeacon":                  "functionValue",
    "requestMediaKeySystemAccess": "functionValue",
    "getGamepads":                 "functionValue",
    "webkitGetUserMedia":          "functionValue",
    "javaEnabled":                 "functionValue",
    "vibrate":                     "functionValue",
    "requestMIDIAccess":           "functionValue",
    "credentials":                 {
        "get":                  "functionValue",
        "store":                "functionValue",
        "requireUserMediation": "functionValue"
    },
    "mediaDevices":                {
        "enumerateDevices":        "functionValue",
        "getSupportedConstraints": "functionValue",
        "getUserMedia":            "functionValue",
        "addEventListener":        "functionValue",
        "removeEventListener":     "functionValue",
        "dispatchEvent":           "functionValue"
    },
    "permissions":                 {
        "query": "functionValue"
    },
    "presentation":                {
        "defaultRequest": "nullValue"
    },
    "getUserMedia":                "functionValue",
    "registerProtocolHandler":     "functionValue",
    "unregisterProtocolHandler":   "functionValue"
};
var screen_obj    = {
    "availWidth":  1280,
    "availHeight": 724,
    "width":       1280,
    "height":      800,
    "colorDepth":  24,
    "pixelDepth":  24,
    "availLeft":   0,
    "availTop":    23,
    "orientation": {
        "angle":               0,
        "type":                "landscape-primary",
        "onchange":            null,
        "lock":                "functionValue",
        "unlock":              "functionValue",
        "addEventListener":    "functionValue",
        "removeEventListener": "functionValue",
        "dispatchEvent":       "functionValue"
    }
};


var page   = require('webpage').create(),
    system = require('system'),
    address;

onPageCreated(page);


function onPageCreated(obj) {
    obj.id = 'pageId_' + page_id_counter++;
    console.log('----- ### onPageCreated ### id: ', obj.id);

    obj.settings.userAgent = navigator_obj.userAgent;

    obj.viewportSize = {
        width:  screen_obj.width,
        height: screen_obj.height
    };
    obj.clipRect     = {top: 0, left: 0, width: screen_obj.width, height: screen_obj.height};

    obj.onError = function (msg, trace) {
        if (msg) console.log('objpage phantom.onError, msg: ' + JSON.stringify(msg));
        if (trace) console.log('objpage phantom.onError, trace: ' + JSON.stringify(trace));
    };


    obj.onInitialized = function () {
        console.log(' onInitialized() start id,url', obj.id, obj.url);
        page.evaluate(function (navigator_obj, screen_obj) {

            (function (navigator_obj, screen_obj) {


                window.oldNavigator = window.navigator;
                window.oldScreen    = window.screen;
                window.oldPlugins   = window.navigator.plugins;
                window.oldPlugins   = window.navigator.plugins;

                window.navigator            = {};
                window.navigator            = navigator_obj;
                window.navigator.sendBeacon = function (url, data) {
                    try {
                        var x = new XMLHttpRequest();
                        x.open('POST', url);
                        x.setRequestHeader("Content-Type", "text/plain;charset=UTF-8");
                        x.send(data);
                    } catch (e) {
                        return false;
                    }
                    return true;
                };

                window.navigator.__proto__         = window.oldNavigator.__proto__;
                window.navigator.plugins.__proto__ = window.oldPlugins.__proto__;


                window.screen.availWidth  = 1280;
                window.screen.availHeight = 724;
                window.screen.width       = 1280;
                window.screen.height      = 800;
                window.screen.colorDepth  = 24;
                window.screen.pixelDepth  = 24;
                window.screen.availLeft   = 0;
                window.screen.availTop    = 23;

                //window.screen ={};
                //window.screen = screen_obj;
                window.screen.__proto__ = window.oldScreen.__proto__;
                console.log('\n=========> $$$ (window.screen) = ' + Object.prototype.toString.call(window.screen) + '\n');

                // console.log('\n=========> $$$  Object.prototype.toString.call(Symbol()) = ' +  Object.prototype.toString.call(Symbol())+'\n');


                console.log('window.oldNavigator.userAgent', window.oldNavigator.userAgent);


                //window.navigator.__defineGetter__('userAgent', function () {
                //    window.navigator.sniffed = true;
                //    return userAgent;
                //});
                //
                //window.navigator.__defineGetter__('platform', function () {
                //    window.navigator.sniffed = true;
                //    return platform;
                //});


                delete window.callPhantom;
                delete window._phantom;

            })(navigator_obj, screen_obj);
        }, navigator_obj, screen_obj);
    };

    obj.onError = function (msg, trace) {
        if (msg) console.log('objpage phantom.onError, msg: ' + JSON.stringify(msg));
        if (trace) console.log('objpage phantom.onError, trace: ' + JSON.stringify(trace));
    };

    obj.onPageCreated       = function (obj) {
        onPageCreated(obj);
    };
    obj.onResourceRequested = function (requestData, networkRequest) {
        //onResourceRequested(obj, requestData, networkRequest);
    };
    obj.onResourceReceived  = function (response) {
        //onResourceReceived(obj, response);
    };
    obj.onUrlChanged        = function (targetUrl) {
        console.log('----- # onUrlChanged # ---- ', obj.id, targetUrl);
    };
    obj.onClosing           = function (closingPage) {
        console.log('----- # onClosing # ---- ', obj.id);
    };
    obj.onLoadFinished      = function (status) {
        console.log('----- # onLoadFinished # ---- ', obj.id, status);
    };
    obj.onConsoleMessage    = function (msg) {
        console.log('   console.log ', obj.id, msg);
    };
}


if (system.args.length === 1) {
    console.log('need argument:  <some URL>');
    phantom.exit(1);
}
else {

    address = system.args[1];

    console.log('address = ' + address);

    page.open(address, function (status) {
        if (status !== 'success') {
            console.log('FAIL to load the address');
            phantom.exit();
        }
        else {
            //console.log('page.content = ' + page.content);
            console.log('page loaded');
            page.scrollPosition = {top: 800, left: 0};
            page.sendEvent('mousemove', 150, 150);

            window.setTimeout(function () {

                var thumb = random_thumb(page, screen_obj.width, screen_obj.height);

                if (!thumb.x || !thumb.y) {
                    console.log('      ### no have tumbs ###');
                } else {
                    console.log('      ###  tumbs OK ### ', JSON.stringify(thumb));
                }

                window.setTimeout(function () {
                    page.render('screen1.jpeg', {format: 'jpeg', quality: '100'});
                    page.sendEvent('mousemove', 175, 175);
                    page.render('screen2.jpeg', {format: 'jpeg', quality: '100'});
                    page.sendEvent('click', 175, 175, 'left');
                    page.render('screen3.jpeg', {format: 'jpeg', quality: '100'});

                    window.setTimeout(function () {
                        page.render('screen4.jpeg', {format: 'jpeg', quality: '100'});
                    }, 2500);

                }, 2500);

            }, 2500);


            /*            window.setTimeout(function () {

                            console.log('========= >>>> make click');
                            page.evaluate(function () {
                                (function () {
                                    var el =  document.getElementById('img1');
                                    var ev = document.createEvent("MouseEvent");
                                    // el.setAttribute('target', '_blank');
                                    ev.initMouseEvent(
                                        "click",
                                        true /!* bubble *!/, true /!* cancelable *!/,
                                        window, null,
                                        0, 0, 0, 0, /!* coordinates *!/
                                        false, false, false, false, /!* modifier keys *!/
                                        0 /!*left*!/, null
                                    );

                                    el.dispatchEvent(ev);
                                })();
                            });

                        }, 2500);*/


        }
    });
}
