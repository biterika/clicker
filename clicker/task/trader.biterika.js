var system  = require('system');
var args    = system.args;

//Libs
var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q');
var page    = require('webpage').create();

//Actions
var check_popup = require('./../actions/check_popup').exec;
var do_clicks   = require('./../actions/do_clicks').exec;
var postload    = require('./../actions/postload').exec;

exports.exec = function(page){

    var code = '';

    var deferred = Q.defer();

    Q.fcall(function(){


        return check_popup(page);


    }).then(function () {


        return postload(page);


    }).then(function (result) {

        debug.log('postload result',JSON.stringify(result));

        if(result.status == 'success'){
            return check_popup(page);
        }
        return true;


    }).then(function () {


        return do_clicks(page);


    }).then(function () {


        if(param.have_thumb == 0){
            throw new Error('NOT_FOUND_THUMB');
        }

        var click_task = param.task.click_task;
        var click_ok = param.task.click_ok;

        if(click_task > 0 && click_task == click_ok){

            code = 'TASK_FINISH_FULL';

        }else if(click_ok > 0 && click_ok < click_task){

            code = 'PART_POPUP';

        }else{

            code = 'NO_HAVE_POPUP';

        }


        deferred.resolve(code);


    }).catch(function (e) {


        deferred.reject(e.message);


    }).done(function(){
    });

    return deferred.promise;

}
