var system  = require('system');
var args    = system.args;

//Libs
var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var domain  = require('./../lib/domain');
var random  = require('./../lib/random');
var Q       = require('./../modules/q/q');

var uagent  = require('./../inc/uagent');
var screens = require('./../inc/screen');

var actions = {};

var requested_domains = {};
var useragent = uagent.list[random.int(0,uagent.list.length-1)];
var screen = screens.list[random.int(0,screens.list.length-1)].split('x');
param.task.sx = screen[0];
param.task.sy = screen[1];

var make_click_selector  = require('./../actions/make_click_selector').exec;

exports.exec = function(page) {

    onPageCreated(page);

    var deferred = Q.defer();

    Q.fcall(function () {

        return  wait(10);

    }).then(function (result) {

        debug.log('');
        debug.log(param.task.param);
        actions = JSON.parse(param.task.param);
        debug.log(actions);
        debug.log('');

        phantom.exit();

        var count = 0;
        var time = 100;


        for(var id in actions){
            if(actions.hasOwnProperty(id)){
                var action = actions[id];
                switch (action){
                    case 'friends':
                        count = 50;
                        for(var i=0;i<count;i++){
                            make_click_selector(page,'a img', i*time);
                        }
                        break;
                }
            }
        }

        console.log('add.trader','wait',count+10);
        return  wait(count+10);

    }).then(function (result) {

        var task = [];

        //'new','tube','trader','birge','trash','working'
        debug.log('actions',JSON.stringify(actions));

        task.push(links(page));

        for(var id in actions){
            if(actions.hasOwnProperty(id)){
                var action = actions[id];
                switch (action){
                    case 'analyze':
                        task.push(analyze(page));
                        break;
                    case 'button':
                        task.push(button(page));
                        break;
                }
            }
        }

        return Q.all(task);

    }).then(function (result) {


        debug.log('result',JSON.stringify(result));
        debug.log('requested_domains',JSON.stringify(requested_domains));

        var settings = {
            operation: "POST",
            encoding: "utf8",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify({
                task: param.task,
                actions: result,
                domains: requested_domains
            })
        };
        page.open(param.finish_url, settings, function(status) {
            console.log('Status: ' + status);
            console.log(page.content);
            phantom.exit();
        });

        return deferred.promise;


    }).then(function () {

        var code = 'TASK_FINISH_FULL';

        deferred.resolve(code);


    }).catch(function (e) {


        deferred.reject(e.message);


    }).done(function () {
    });

    return deferred.promise;

};

//======================================================================================================================

function wait(timeout){

    var deferred = Q.defer();

    setTimeout(function(){
        deferred.resolve(true);
    },timeout*1000);

    return deferred.promise;

}

function onPageCreated(obj){

    param.page_id++;
    debug.log('  ', 'DEBUG URL Created', param.page_id);

    param.popupLoading = true;
    param.page_created = true;

    obj.domain = domain.getDomain(obj.url);
    obj.id = param.page_id;
    obj.requests = 0;

    obj.viewportSize = {
        width: param.task.sx||1366,
        height: param.task.sy||768
    };

    obj.settings.userAgent = useragent;
    obj.settings.webSecurityEnabled = false;

    obj.onInitialized = function () {
        obj.evaluate(function (sx, sy) {
            window.screen = {
                width: sx,
                height: sy
            };
            window.navigator.__defineGetter__('javaEnabled', function () {
                return function() { return true; };
            });
        }, param.task.sx||1366, param.task.sy||768);
    };

    obj.onPageCreated = function(obj){
        onPageCreated(obj);
    };
    obj.onResourceRequested = function(requestData, networkRequest) {
        onResourceRequested(obj,requestData,networkRequest);
    };
    obj.onUrlChanged = function(targetUrl) {
        onUrlChanged(obj,targetUrl);
    };
    obj.onLoadFinished = function(status) {
        onLoadFinished(obj,status);
    };

}

function onResourceRequested(obj,requestData,networkRequest){

    var tmp = requestData.url.split(".");
    var ext = tmp[tmp.length-1];
    if(ext != 'gif' && ext != 'jpg' && ext != 'png' && ext != 'jpeg' && ext != 'js' && ext != 'css') {

        //debug.log('  ', 'DEBUG URL >>', obj.id, requestData.url);

        var d = domain.getRootDomain(requestData.url);
        //console.log('onResourceRequested',d);
        if(!requested_domains[d]){
            requested_domains[d] = 0;
        }
        requested_domains[d]++;

    }
}

function onUrlChanged(obj,targetUrl){

    obj.domain = domain.getDomain(targetUrl);

    var d = domain.getRootDomain(targetUrl);
    //console.log('onUrlChanged',d);
    if(!requested_domains[d]){
        requested_domains[d] = 0;
    }
    requested_domains[d]++;

}
function onLoadFinished(obj,status){
    debug.log('  ', 'Event:', 'LoadFinished', obj.id, 'requests:', obj.requests, 'content len:', obj.content.length);
    debug.log('domains',obj.domain,param.task.trader);
    if(obj.domain != param.task.trader){
        make_click_selector(obj,'a img', 100);
    }
}

//======================================================================================================================

function analyze(obj){

    var deferred = Q.defer();

    var thumbs_all = [];
    if(typeof obj != 'undefined' && obj){
        thumbs_all = obj.evaluate(function(selector) {
            function getOffsetRect(elem) {
                var box = elem.getBoundingClientRect();
                var body = document.body;
                var docElem = document.documentElement;
                var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
                var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
                var clientTop = docElem.clientTop || body.clientTop || 0;
                var clientLeft = docElem.clientLeft || body.clientLeft || 0;
                var top  = box.top +  scrollTop - clientTop;
                var left = box.left + scrollLeft - clientLeft;
                var width = elem.offsetWidth;
                var height = elem.offsetHeight;
                return { x: Math.round(left), y: Math.round(top), width: width, height: height }
            }
            var mass = document.querySelectorAll("a img");
            var sizes = {};
            for (var i = 0; i < mass.length; i++) {
                if(mass[i].height > 0){
                    var rect = getOffsetRect(mass[i]);
                    if(rect.x > 0 && rect.y > 0 && rect.width > 0 && rect.height > 0){
                        //console.log(JSON.stringify(rect));
                        var size = rect.width + 'x' + rect.height;
                        if (typeof sizes[size] == 'undefined') sizes[size] = 0;
                        sizes[size]++;
                    }
                }
            }
            return sizes;
        }, 'a img');
    }

    if(thumbs_all.length < 1) {
        debug.log('Thumbs not found');
    }

    deferred.resolve({
        analyze:thumbs_all
    });

    return deferred.promise;

}

function links(obj){

    var deferred = Q.defer();

    var links_all = {};
    if(typeof obj != 'undefined' && obj){
        links_all = obj.evaluate(function(thumb_top_count, selector) {
            function getRootDomain(url) {
                var domain;
                if (url.indexOf("://") > -1) domain = url.split('/')[2];
                else domain = url.split('/')[0];
                domain = domain.split(':')[0];
                var arr = domain.split('.');
                return arr[arr.length-2]+'.'+arr[arr.length-1];
            }
            var links = {};
            var mass = document.querySelectorAll(selector);
            for (var i = 0; i < mass.length; i++) {
                var d = getRootDomain(mass[i].href);
                if(d != '' && !links[d]){
                    links[d] = 0;
                }
                links[d]++;
            }
            return links;
        }, config.const.thumb_top_count, 'a');//[src^="http"]
    }

    deferred.resolve({
        links:links_all
    });

    return deferred.promise;

}

function button(obj){

    var deferred = Q.defer();
    deferred.resolve(false);
    return deferred.promise;

}
