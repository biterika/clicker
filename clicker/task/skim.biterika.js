var system  = require('system');
var args    = system.args;

//Libs
var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var random  = require('./../lib/random');
var Q       = require('./../modules/q/q');

//Actions
var check_popup = require('./../actions/check_popup').exec;
var make_click  = require('./../actions/make_click').exec;

var script_name = 'skim.biterika.js';

exports.exec = function(page) {

    var deferred = Q.defer();

    Q.fcall(function () {
        debug.log(script_name, '');
        debug.log(script_name, '----> start');
        debug.log(script_name,'');

        // delete param.task.click_task;
        param.task.click_task =0;

        var deferred2 = Q.defer();

        debug.log(script_name, 'start');

        debug.log(script_name, 'Wait', config.timeout.birge_after_open / 1000, 'sec');
        setTimeout(function () {
            debug.log(script_name, 'continue');
            deferred2.resolve(true);
        }, config.timeout.birge_after_open);

        return deferred2.promise;


    }).then(function () {

        console.log('                                   -------- first click 50% ---------- ');
        var click = random.int(1, 2); // вероятность 50%
        if (click == 1) {
            param.task.click_task = 1;
            debug.log(script_name,'');
            debug.log(script_name,'Make first click ...');
            debug.log(script_name, 'start  make_click');
            return make_click(page);
        } else {
            debug.log(script_name,'');
            debug.log(script_name,'Don\'t make clicks');
            return false;
        }


    }).then(function (click1) {

        debug.log(script_name, ' finish make_click');

        if (!click1)
            return false;

        console.log('                               -------- second click 20% ---------- ');
        var click = random.int(1, 5); //вероятность 20%, точнее 10%
        if (click == 1) {
            param.task.click_task = 2;
            debug.log(script_name,'');
            debug.log(script_name,'Make second click ...');
            return make_click(page);
        } else {
            debug.log(script_name,'');
            debug.log(script_name,'Don\'t make second click');
            return true;
        }


    }).then(function () {


        var deferred3 = Q.defer();
        debug.log(script_name,'');
        debug.log(script_name,'Wait', config.timeout.birge_before_close / 1000, 'sec');
        setTimeout(function () {
            deferred3.resolve(true);
        }, config.timeout.birge_before_close);
        return deferred3.promise;


    }).then(function () {


        debug.log(script_name,'done');



        var code = 'n/a';
        if (param.task.click_ok >= param.task.click_task ) {
            code = 'TASK_FINISH_FULL';
        } else {
            code = 'TASK_FINISH_PART';
        }


        console.log('                               -------- click_ok '+param.task.click_ok+' --click_task '+param.task.click_task+'-------->>>  '+code);

        deferred.resolve(code);


    }).catch(function (e) {

        debug.log(script_name,'cath', JSON.stringify(e));
        deferred.reject(e);


    }).done(function () {
        debug.log(script_name,'');
        debug.log(script_name,'<---- done');
        debug.log(script_name,'');
    });

    return deferred.promise;

}