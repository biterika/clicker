var system = require('system');
var args   = system.args;

//Libs
var time   = require('./../lib/time');
var domain   = require('./../lib/domain');
var config = require('./../inc/config');
var param  = require('./../inc/param');
var debug  = require('./../lib/debug');
var Q      = require('./../modules/q/q');
var colors = require('./../modules/colors/safe.js');
var page   = require('webpage').create();

//TODO move to config
config.const.token = config.const.token_usb;

//Actions
var check_popup = require('./../actions/check_popup').exec;
var do_clicks   = require('./../actions/do_clicks').exec;
var postload    = require('./../actions/postload').exec;

exports.exec = function (page) {


    var code          = '';

    var deferred = Q.defer();

    Q.fcall(function () {

        param.task.click_task = 4;
        param.task.click_task_ext = 3;

        param.task.only_domain = param.task.trader;
        param.task.mouse_move = false;

        console.log('param.task.trader = '+param.task.trader);
        console.log('param.task.click_task = '+param.task.click_task);
        console.log('page.url = '+page.url, ' domain = '+domain.getDomain(page.url), page.domain);

        if (domain.getDomain(page.url) ==  param.task.trader) {
            console.log('============================================');
            console.log(colors.magenta('           trader on page now - OK!     '+param.task.trader+'        '));
            console.log('============================================');
            param.counters.start = true;
            var log2 = {
                time:         time.time(),
                type:         'start',
                status:       status || null,
                page_id:      page.page_id || null,
                code:         page.code || null,
                url:          page.url || null,
                domain:       domain.getDomain(page.url)||null,
                refer_url:    page.refer_url || null,
                refer_domain: page.refer_domain || null,
                createdAt:    page.createdAt || null,
                content:      page.content.length || null,
                redirect:     page.redirect || null
            };

        }
        else {
            code = 'OPEN_NOT_TRADER';
            deferred.resolve(code);
        }

        console.log(colors.magenta('---> check_popup start'));

        return check_popup(page);


    }).then(function () {

        console.log(colors.blue('<--- check_popup finished'));

        console.log(colors.magenta('---> postload start'));

        return postload(page);

    }).then(function (result) {

        console.log(colors.blue('<--- postload finished, postload result', JSON.stringify(result)));

        if (result.status == 'success') {

            console.log(colors.magenta('---> check_popup after postload start'));
            return check_popup(page);
        }

        return true;

    }).then(function () {

        console.log(colors.magenta('---> do_clicks start'));

        return do_clicks(page);

    }).then(function () {

        console.log(colors.blue('<--- do_clicks finished'));

        if (param.have_thumb == 0) {

            code = 'NOT_FOUND_THUMB';

        } else {

            var click_task    = param.task.click_task;

            var click_external = param.counters.count_ext;
            var click_content = param.counters.count_int;
            var click_all      = click_external + click_content;

            console.log('#### trader.usb result: click_task', click_task, 'click_all:', click_all, 'click_content:', click_content, 'click_external:',click_external);

            if (click_external > 0) {
                code = 'TASK_FINISH_FULL';
            }
            else  if (click_all > 0) {
                code = 'TASK_FINISH_CONTENT';
            } else {
                code = 'TASK_FINISH_NONE';
            }

            /*
            if (click_task > 0 && click_task == click_ok) {
                if (click_content > 0 && click_content == click_ok) {
                    code = 'TASK_FINISH_CONTENT';
                } else {
                    code = 'TASK_FINISH_FULL';
                }
            } else if (click_ok > 0 && click_ok < click_task) {
                code = 'TASK_FINISH_PART';
            } else if (click_task > 0) {
                code = 'TASK_FINISH_NONE';
            }*/

        }

        deferred.resolve(code);

    }).catch(function (e) {


        deferred.resolve(e.message);


    }).done(function () {
    });

    return deferred.promise;

}
