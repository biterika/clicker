var system  = require('system');
var args    = system.args;

//Libs
var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var random  = require('./../lib/random');
var Q       = require('./../modules/q/q');

//Actions
var check_popup = require('./../actions/check_popup').exec;
var make_click  = require('./../actions/make_click').exec;
var click_xy    = require('./../actions/click_xy').exec;

var script_name = 'popunder.biterika.js';

exports.exec = function(page) {

    var deferred = Q.defer();

    Q.fcall(function () {

        delete param.task.click_task;

        var deferred = Q.defer();
        debug.log('');
        debug.log('start Wait', config.timeout.birge_after_open / 1000, 'sec');
        setTimeout(function () {
            debug.log('start Wait - done');
            deferred.resolve(true);
        }, config.timeout.birge_after_open);

        return deferred.promise;

    }).then(function () {

        debug.log(' ');
        debug.log(script_name,'===========pounder.biterika===================================================');
        debug.log(script_name,'Make first click ...');
        debug.log(script_name,'===========pounder.biterika===================================================');
        debug.log(' ');
        //param.task.click_ok++;

        return make_click(page, 'left');

        //click_xy(page, rand_mouse_x ,rand_mouse_y, 'left');

       /* var deferred = Q.defer();
        setTimeout(function () {
            deferred.resolve(true);
        }, config.timeout.birge_after_open);
        return deferred.promise;*/


    }).then(function (click1) {


        var click = random.int(1, 2);
        if (click == 1) {
            var pfc = get_last_tab(page);
            debug.log(' ');
            debug.log('===========pounder.biterika===================================================');
            debug.log(script_name,'     Make second click ... in pfc ',pfc.id,pfc.url);
            debug.log('===========pounder.biterika===================================================');
            debug.log('');
            //param.task.click_ok++;
            return make_click(pfc,'left');
        } else {
            click = random.int(1, 3);
            if (click !== 1) {

                debug.log(' ');
                debug.log('===========pounder.biterika===================================================');
                debug.log(script_name,'     Make second click ... in page ',page.id,page.url);
                debug.log('===========pounder.biterika===================================================');
                debug.log('');
                //param.task.click_ok++;
                return make_click(page,'left');
            } else {
                debug.log(' ');
                debug.log('===========pounder.biterika===================================================');
                debug.log(script_name,'Don\'t make second click');
                debug.log('===========pounder.biterika===================================================');
                debug.log('');
                return false;
            }
        }

    }).then(function (click2) {


        if (!click2)  return false;

        var click = random.int(1, 3);
        if (click == 1) {
            var pfc = get_last_tab(page);
            debug.log(' ');
            debug.log('===========pounder.biterika===================================================');
            debug.log(script_name,'Make 3-click ... in pfc ',pfc.id,pfc.url);
            debug.log('===========pounder.biterika===================================================');
            debug.log('');
            //param.task.click_ok++;
            return make_click(pfc,'left');
        } else {
            debug.log('');
            debug.log('Don\'t make 3-click');
            debug.log(script_name,'===========pounder.biterika===================================================');
            debug.log('');
            return true;
        }

    }).then(function (click3) {


        if (!click3)  return false;

        var click = random.int(1, 5);
        if (click == 1) {
            var pfc = get_last_tab(page);
            debug.log(' ');
            debug.log('===========pounder.biterika===================================================');
            debug.log(script_name,'Make 4-click ... in pfc ',pfc.id,pfc.url);
            debug.log('===========pounder.biterika===================================================');
            debug.log('');
            //param.task.click_ok++;
            return make_click(pfc,'left');
        } else {
            debug.log('');
            debug.log('Don\'t make 4-click');
            debug.log(script_name,'===========pounder.biterika===================================================');
            debug.log('');
            return true;
        }


    }).then(function (click4) {


        var deferred = Q.defer();
        debug.log('');
        debug.log('finish Wait start', config.timeout.birge_before_close / 1000, 'sec');
        setTimeout(function () {
            debug.log('finish wait done');
            deferred.resolve(true);
        }, config.timeout.birge_before_close);
        return deferred.promise;


    }).then(function () {

        var code = 'TASK_FINISH_FULL';

        deferred.resolve(code);


    }).catch(function (e) {

        /*console.log('popunder.biterika.js catch');
        var e_catch = 'n/a';
        if (e) e_catch = e.message||JSON.stringify(e);

        console.log('popunder.biterika.js catch '+e_catch);*/

        deferred.reject(e.message);


    }).done(function () {
    });

    return deferred.promise;

}

//======================================================================================================================

function get_last_tab(obj){

    var res = obj;
    var max_id = res.id;

    for(var id in obj.pages){
        if(obj.pages.hasOwnProperty(id)){
            var page_id = obj.pages[id].id;
            if(page_id > max_id){
                max_id = obj.pages[id].id;
                res = obj.pages[id];
            }
        }
    }

    return res;


}