

function getNavigator(lang){



    var plugins = {
        "0": {
            "0": {
                "type": "application/pdf",
                "suffixes": "pdf",
                "description": ""
            },
            "name": "Chromium PDF Viewer",
            "filename": "mhjfbmdgcfjbbpaeojofohoefgiehjai",
            "description": "",
            "length": "1",
            "item": "functionValue",
            "namedItem": "functionValue"
        },
        "1": {
            "0": {
                "type": "application/x-nacl",
                "suffixes": "",
                "description": "Native Client Executable"
            },
            "1": {
                "type": "application/x-pnacl",
                "suffixes": "",
                "description": "Portable Native Client Executable"
            },
            "name": "Native Client",
            "filename": "internal-nacl-plugin",
            "description": "",
            "length": "2",
            "item": "functionValue",
            "namedItem": "functionValue"
        },
        "2": {
            "0": {
                "type": "application/x-google-chrome-pdf",
                "suffixes": "pdf",
                "description": "Portable Document Format"
            },
            "name": "Chromium PDF Viewer",
            "filename": "internal-pdf-viewer",
            "description": "Portable Document Format",
            "length": "1",
            "item": "functionValue",
            "namedItem": "functionValue"
        },
        "length": "3",
        "item": "functionValue",
        "namedItem": "functionValue",
        "refresh": "functionValue"
    };
    var mimeTypes ={
        "0": {
            "type": "application/pdf",
            "suffixes": "pdf",
            "description": ""
        },
        "1": {
            "type": "application/x-nacl",
            "suffixes": "",
            "description": "Native Client Executable"
        },
        "2": {
            "type": "application/x-pnacl",
            "suffixes": "",
            "description": "Portable Native Client Executable"
        },
        "3": {
            "type": "application/x-google-chrome-pdf",
            "suffixes": "pdf",
            "description": "Portable Document Format"
        },
        "length": "4",
        "item": "functionValue",
        "namedItem": "functionValue"
    };
    var navigator_res= {
        "vendorSub": "",
        "productSub": "20030107",
        "vendor": "Google Inc.",
        "maxTouchPoints": "0",
        "hardwareConcurrency": "4",
        "appCodeName": "Mozilla",
        "appName": "Netscape",
        "appVersion": "5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2671.0 Safari/537.36",
        "platform": "MacIntel",
        "product": "Gecko",
        "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/601.4.4 (KHTML, like Gecko) Version/9.0.3 Safari/601.4.4",
        "language": "ru",
        "languages": {
            "0": "ru-RU",
            "1": "ru",
            "2": "en-US",
            "3": "en"
        },
        "onLine": true,
        "cookieEnabled": true,
        "bluetooth": {},
        "doNotTrack": null,
        "geolocation": {
            "getCurrentPosition": function(){ return null; },
            "watchPosition": function(){ return null; },
            "clearWatch": function(){ return null; }
        },
       // "plugins": plugins,
       // "mimeTypes": mimeTypes,
        "webkitTemporaryStorage": {
            "queryUsageAndQuota": function(){ return null; },
            "requestQuota": function(){ return null; }
        },
        "webkitPersistentStorage": {
            "queryUsageAndQuota": function(){ return null; },
            "requestQuota": function(){ return null; }
        },
        "serviceWorker": {
            "controller": null,
            "ready": {},
            "oncontrollerchange": null,
            "onmessage": null,
            "register": function(){ return null; },
            "getRegistration": function(){ return null; },
            "getRegistrations": function(){ return null; },
            "addEventListener": function(){ return null; },
            "removeEventListener": function(){ return null; },
            "dispatchEvent": function(){ return null; }
        },
        "getBattery": function(){ return null; },
        "sendBeacon": function(){ return null; },
        "getGamepads": function(){ return null; },
        "webkitGetUserMedia": function(){ return null; },
        javaEnabled: function(){ return true; },
        "vibrate": function(){ return null; },
        "requestMIDIAccess": function(){ return null; },
        "credentials": {
            "get": function(){ return null; },
            "store": function(){ return null; },
            "requireUserMediation": function(){ return null; }
        },
        "mediaDevices": {
            "enumerateDevices": function(){ return null; }
        },
        "permissions": {
            "query": function(){ return null; }
        },
        "presentation": {
            "defaultRequest": null
        },
        "requestMediaKeySystemAccess": function(){ return null; },
        "registerProtocolHandler": function(){ return null; },
        "unregisterProtocolHandler": function(){ return null; }
    };

    return {
        plugins: plugins,
        mimeTypes: mimeTypes,
        navigator_res: navigator_res,
    };
}





exports.getNavigator = getNavigator;