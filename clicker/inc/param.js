var random = require('./../lib/random');

exports.task = {
    is_direct:      false,
    target_url:     '',
    task_uniq_id:   'undefined',
    task_id:        'undefined',
    server:         'undefined',
    login:          'undefined',
    token:          'undefined',
    balancer:       'undefined',
    ip:             '',
    fake:           0,
    tds:            false,
    site:           false,
    trader:         false,
    task_type:      false,
    click_task:     random.int(1, 3),
    click_task_ext: 3,
    click_task_int: 4,
    click_ok:       0,
    click_content:  0,
    click_external: 0,
    delta:          0,
    code:           'UNDEFINED',
    sx:             1024,
    sy:             768,
    only_domain:    null,
    mouse_move:     true,
};

exports.page_created       = false;
exports.args               = [];
exports.make_screen        = false;
exports.task_url           = '';
exports.finish_url         = '';
exports.verbose            = true;
exports.token              = '';
exports.step               = 0;
exports.page_id            = 0;
exports.close_popup        = 0;
exports.popupLoading       = false;
exports.have_thumb         = 0;
exports.log_id             = '';
exports.log                = [];
exports.url_log            = [];
exports.url_log2           = [];
exports.url_log_page       = {};
exports.url_log_page_count = {};
exports.back_case_no_count       = {};
exports.counters           = {
    start:     false,
    count_all: 0,
    count_int: 0,
    count_ext: 0,
    count_pop: 0,
    pages_all: [],
    pages_int: [],
    pages_ext: [],
    pages_pop: [],
};
exports.navigator_json     = '';
exports.navigator_obj      = null;
exports.mouse              = {
    x: random.int(1, 700),
    y: random.int(1, 400),
};


