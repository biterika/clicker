exports.const = {
    server          : 'cl3',
    log_file        : './clicker.log',
    debug           : false,
    send_log        : false,
    make_screen     : false,
    try_count_max   : 3,
    thumb_top_count : 10,
    task_error_limit: 3
};

exports.timeout = {
    //timeout in ms
    task_get        : 30*1000,
    time_for_task   : 60*8*1000,
    exit            : 1*1000,
    exit_error      : 5*1000,
    loading         : 30*1000,
    loading_popup   : 30*1000,
    wait_popup      : 2*1000,
    before_click    : 8*1000,
    trader_after_open   : 15*1000, // todo вернуть на 10
    birge_after_open    : 15*1000, // todo вернуть на 15
    birge_before_close  : 15*1000
};

exports.errors = {
    TASK_NOT_LOADED : 'Can not load task',
    TASK_NOT_PARSED : 'Can not parse task json',
    TRY_COUNT_MAX   : 'Reached limit of try click task',
    SITE_BODY_EMPTY : 'No have html body in opened site',
    TASK_TIMEOUT    : 'Task exec timeout',
    NOT_FOUND_THUMB : 'Not found thumbs'
};

exports.path = '/usr/share/biterika/clicker-headless2/clicker';
