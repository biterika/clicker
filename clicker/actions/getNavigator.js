var config = require('./../inc/config');
var param = require('./../inc/param');
var debug = require('./../lib/debug');
var random = require('./../lib/random');
var Q = require('./../modules/q/q.js');
var fs = require('fs');

var navigators_dir = config.path + '/inc/navigators';

var script_name = 'getNavigator.js';

var content_obj, content, user_agent,
    navigators_files_arr,
    rand_idx;

exports.exec = function (lang) {

    //console.log('navigators_dir = ', navigators_dir);
    //navigators_files_arr = fs.list(navigators_dir);
    //console.log('navigators_files_arr.length = ' + navigators_files_arr.length);
    //rand_idx = random.int(2, navigators_files_arr.length-1);
    //console.log('rand_idx = ' + rand_idx);
    //console.log('rand_navigator_file = ' + navigators_files_arr[rand_idx]);
    //content = fs.read(navigators_dir+'/'+navigators_files_arr[rand_idx]);
    //content_obj = JSON.parse(JSON.stringify(JSON.parse(content)));

    content_obj = param.navigator_obj;

    //if (lang && !(lang.match(/en-US/i)) &&  !lang.match(/en/i) ) {
    //    content_obj.language=lang;
    //    content_obj.languages = [lang,'en-US','en'];
    //} else {
    //    content_obj.language='en-US';
    //    content_obj.languages = ['en-US','en'];
    //}

    if (content_obj.language) param.task.lang = content_obj.language;
    else if (content_obj.browserLanguage) param.task.lang = content_obj.browserLanguage;
    else if (content_obj.systemLanguage) param.task.lang = content_obj.systemLanguage;
    // else if (content_obj.languages) param.task.lang = content_obj.languages;
    else {
        console.error('[ERROR] not found language in navigator, set default en-US');
        param.task.lang = 'en-US';
        content_obj.language='en-US';
    }

/*

    var plugins_for_rename=['Shockwave Flash','Silverlight Plug-In','application/x-shockwave-flash','ShockwaveFlash.ShockwaveFlash'];


    for (var i in content_obj.plugins) {
        if (content_obj.plugins.hasOwnProperty(i) && content_obj.plugins[i] ) {
            if (content_obj.plugins[i].name && plugins_for_rename.indexOf(content_obj.plugins[i].name) >=0 ) {
                content_obj.plugins[i].name = random.char(32);
            }
            else if (plugins_for_rename.indexOf(i) >=0 ) {
                content_obj.plugins[random.char(32)] = content_obj.plugins[i];
                delete content_obj.plugins[i];
            }
        }
    }


    console.log('getNavigator [result navigator]',JSON.stringify(content_obj,'','\t'));
*/


    if (content_obj.hasOwnProperty('userAgent')) {
        console.log('content_obj.userAgent = ' + content_obj.userAgent);
        user_agent = content_obj.userAgent;
        return {
            navigator_json: JSON.stringify(content_obj),
            user_agent:     user_agent
        };

    } else {
        console.log('[ERROR] critical error: navigatoruser_agent is empty, exit');
        phantom.exit(33);
    }
};

