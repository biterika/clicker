var config = require('./../inc/config');
var param = require('./../inc/param');
var domain = require('./../lib/domain');
var debug = require('./../lib/debug');
var Q = require('./../modules/q/q.js');

var make_screen = require('./make_screen').exec;

var script_name = 'site_open.js';


exports.exec = function (page) {

    param.step++;

    debug.log(script_name, '');
    debug.log(script_name, 'Open site:', param.task_url);

    var deferred = Q.defer();

    try {

        var result = {};
        page.open(param.task_url, function (status) {
            debug.log(script_name, 'First site open:', status);

            if (page.url) {
                debug.log('[task_url] [' + page.url + ']');
            }

            if (page.url.indexOf('no.task') >= 0) {

                debug.log(script_name, 'NO_TASK');
                result = {status: 'NO_TASK'};
                deferred.resolve(result);

            } else if (page.domain == 'bad.proxy') {

                debug.log(script_name, 'BAD_PROXY');
                result = {status: 'BAD_PROXY'};
                deferred.resolve(result);

            } else if (page.domain == 'request.error') {

                debug.log(script_name, 'REQUEST_ERROR');
                result = {status: 'REQUEST_ERROR'};
                deferred.resolve(result);

            } else if (page.domain == 'no.proxy') {

                debug.log(script_name, 'NO_PROXY');
                result = {status: 'NO_PROXY'};
                deferred.resolve(result);

            } else if (status == 'success') {

                debug.log(script_name, 'First site open: success, page.content.length: '+page.content.length+' page.url: '+page.url+' page.domain'+page.domain);

                check();

            } else {

                debug.log(script_name, 'TASK_GET_FAIL');
                result = {status: 'TASK_GET_FAIL'};
                deferred.resolve(result);

            }
        });

        var check_count = 0;

        function check() {


            debug.log(script_name, '  ', 'check:', check_count, 'changed:', page.changed, 'domain:', page.domain, 'popupLoading:', param.popupLoading,'pageLoading:', page.pageLoading, 'site:', param.task.site, 'trader:', param.task.trader, 'url:', page.url);

            debug.log('    bool loading() const; '+JSON.stringify(page.loading)+' page_id: '+page.id);

            debug.log('    int loadingProgress() const; '+JSON.stringify(page.loadingProgress));


            if (page.domain && page.domain == 'no.task') {

                debug.log(script_name, 'NO_TASK -> page.domain == no.task');
                deferred.resolve({status: 'NO_TASK'});

            }
            else if (
                page.domain && page.domain == 'bad.proxy'
                || ( page.url && domain.getDomain(page.url) == 'bad.proxy')
            ) {

                debug.log(script_name, 'page.domain == bad.proxy - >  BAD_PROXY');

                deferred.resolve({status: 'BAD_PROXY'});

            }
            else if (
                page.domain && page.domain == 'no.proxy'
                || ( page.url && domain.getDomain(page.url) == 'no.proxy')
            ) {

                debug.log(script_name, 'NO_PROXY');

                deferred.resolve({status: 'NO_PROXY'});

            }
            else if (
                !param.task.is_direct &&
                (page.domain == 'direct' || param.task.site == 'direct')
            ) {

                debug.log(script_name, ' ');
                debug.log(script_name, '$$$$$$$$$$$ IS_DIRECT $$$$$ page.domain == \'direct\' || param.task.site == \'direct\'');
                debug.log(script_name, '$$$$$$$$$$$ param.task.target_url = '+param.task.target_url);
                debug.log(script_name, ' ');

                param.task.is_direct = true;
                param.task.site = domain.getDomain(param.task.target_url);

                var result_direct = {};

                page.open(param.task.target_url, function (status) {

                    debug.log(script_name, '[INFO] direct site opening: '+param.task.target_url+' status = '+ status);

                    if (
                        page.domain && page.domain == 'bad.proxy'
                        || ( page.url && domain.getDomain(page.url) == 'bad.proxy')
                    ) {

                        debug.log(script_name, 'BAD_PROXY');

                        result = {status: 'BAD_PROXY'};
                        deferred.resolve(result);

                    }
                    else if (
                        page.domain && page.domain == 'no.proxy'
                        || ( page.url && domain.getDomain(page.url) == 'no.proxy')
                    ) {

                        debug.log(script_name, 'NO_PROXY');

                        result = {status: 'NO_PROXY'};
                        deferred.resolve(result);

                    }
                    else if (status == 'success') {

                        page.changed++;
                        debug.log(script_name, '[OK] direct site start open success');

                        check();

                    }
                    else {

                        debug.log(script_name, '[ERROR] direct site PAGE_OPEN_ERROR status= '+JSON.stringify(status));

                        //result_direct = {status: 'PAGE_OPEN_ERROR'};
                        //deferred.resolve(result_direct);

                        check();

                    }
                });

            }
            else {
                setTimeout(function () {

                    check_count++;
                    //TODO  && page.domain != param.task.site
                    if (
                        page.changed >= 3 &&
                        !page.pageLoading &&
                        param.task.site != 'blank' &&
                        page.loadingProgress==100
                    ) {

                        debug.log(script_name, ' FINISH ', 'check:', check_count, 'changed:', page.changed||'n/a', 'domain:', page.domain||'n/a', 'popupLoading:', param.popupLoading||'n/a','pageLoading:', page.pageLoading||'n/a', 'site:', param.task.site||'n/a', 'trader:', param.task.trader||'n/a', 'url:', page.url||'n/a');

                        finish();

                    }
                    else if (
                        check_count > 60 &&
                        param.task.trader && page.changed >= 3 &&
                        page.domain == param.task.trader &&
                        param.task.site != 'blank'
                    ) {

                          debug.log(script_name, ' FINISH#2 ', 'check:', check_count, 'changed:', page.changed||'n/a', 'domain:', page.domain||'n/a', 'loading:', param.popupLoading||'n/a', 'site:', param.task.site||'n/a', 'trader:', param.task.trader||'n/a', 'url:', page.url||'n/a');
                          finish();

                    }
                    else {

                        if (check_count > 90) {

                            if (
                                page.changed >= 3  &&
                                page.content.length>100 &&
                                param.task.site != 'blank'
                            ) {
                                debug.log(script_name, '[ERROR] Open trader half success', page.content.length||'n/a');
                                debug.log(script_name, '[ERROR] Open trader half success', page.content||'n/a');

                                finish();
                            }
                            else {
                                debug.log(script_name, '[ERROR] Open trader error', page.content.length||'n/a');
                                debug.log(script_name, '[ERROR] Open trader error', page.content||'n/a');

                                result = {status: 'PAGE_OPEN_TIMEOUT'};
                                deferred.resolve(result);
                            }

                        }
                        else {

                            check();
                        }
                    }

                }, 1000);

            }

        }

        function finish() {
            debug.log(script_name, '[OK] Open trader success', page.content.length||'n/a', 'wait:', config.timeout.trader_after_open / 1000 + 's');



            if (
                page.domain && page.domain == 'bad.proxy'
                || ( page.url && domain.getDomain(page.url) == 'bad.proxy')
            ) {

                debug.log(script_name, 'BAD_PROXY');
                result = {status: 'BAD_PROXY'};
                deferred.resolve(result);

            }
            else if (page.domain && page.domain == 'no.proxy' || ( page.url && domain.getDomain(page.url) == 'no.proxy')) {

                debug.log(script_name, 'NO_PROXY');
                result = {status: 'NO_PROXY'};
                deferred.resolve(result);

            }
            else {

                setTimeout(function () {


                    if (
                        page.domain && page.domain == 'bad.proxy'
                        || ( page.url && domain.getDomain(page.url) == 'bad.proxy')
                    ) {

                        debug.log(script_name, 'BAD_PROXY');
                        result = {status: 'BAD_PROXY'};
                        deferred.resolve(result);

                    }
                    else if (
                        page.domain && page.domain == 'no.proxy'
                        || ( page.url && domain.getDomain(page.url) == 'no.proxy')
                    ) {

                        debug.log(script_name, 'NO_PROXY');
                        result = {status: 'NO_PROXY'};
                        deferred.resolve(result);

                    }
                    else {
                        result = {status: 'success'};
                        deferred.resolve(result);
                    }

                }, config.timeout.trader_after_open);

            }
        }

    } catch (e) {
        debug.log(script_name,' CATCH ', e.message||JSON.stringify(e));
        deferred.resolve(e.message);
    }

    return deferred.promise;

};
