var config = require('./../inc/config');
var param  = require('./../inc/param');
var random = require('./../lib/random');
var debug  = require('./../lib/debug');
var Q      = require('./../modules/q/q.js');

var random_thumb = require('./random_thumb').exec;
var click_xy     = require('./click_xy').exec;
var check_pages  = require('./check_pages').exec;
//var make_screen = require('./../actions/make_screen2file').exec;

var script_name = 'make_click.js';

var thumb;
var obj_main      = false;
var obj_for_click = false;

exports.exec = function (obj, button) {

    if (!button) {
        button = 'left'; // middle
    }

    var deferred = Q.defer();

    Q.fcall(function () {
        debug.log(script_name, '');
        debug.log(script_name, '----> start');
        debug.log(script_name, ' ');

        debug.log(script_name, ' wait ' + config.timeout.before_click + 'ms');

    }).delay(config.timeout.before_click).then(function () {
        //wait timeout before click=

        debug.log(script_name, ' wait fin, continue ');

        if (!obj_main) {
            obj_main = obj;
        }
        if (!obj_for_click) {
            obj_for_click = obj_main;
            debug.log(script_name, ' change obj_for_click (1) to ',obj_main.id, obj_main.url)
        }


        var max_id = 0;
        if (obj == undefined || obj == null || (obj + '') == 'null') {
            debug.log(script_name, ' !obj ');
        }
        else {
            for (var id in obj.pages) {
                if (obj.pages.hasOwnProperty(id)) {
                    debug.log(obj.pages[id].id, obj.pages[id].domain, obj.pages[id].url);
                    if (obj.pages[id].domain == param.task.trader && obj.pages[id].id > max_id) {
                        max_id        = obj.pages[id].id;
                        obj_for_click = obj.pages[id];
                        debug.log(script_name, ' change obj_for_click (2) to ', obj_for_click.id, obj_for_click.url)
                    }
                }
            }
        }


        //is a thumbs have in page?
        thumb = random_thumb(obj_for_click);
        if (!thumb.x || !thumb.y) {
            debug.log(script_name, '### no have tumbs ###');
            debug.log(script_name, 'click', '!thumb.x || !thumb.y');
            debug.log(script_name, 'url', obj_for_click.url);
        }

    }).then(function () {

        debug.log(script_name, 'step 2');

        var x = random.int(5, 600);
        var y = random.int(5, 400);


        param.popupLoading = false;
        param.page_created = false;

        if (thumb.x && thumb.y) {

            debug.log(script_name, '### tumbs have ###');
            param.have_thumb++;

            x = random.int(thumb.x + 5, thumb.x + thumb.width - 5);
            y = random.int(thumb.y + 5, thumb.y + thumb.height - 5);

            debug.log(script_name, '### click_xy() ### at thumb coord: ', x, 'x', y);

            return click_xy(obj_for_click, x, y, button);

        }
        else {

            debug.log(script_name, '### click_xy() ### no_thumbs (first click) -> random coord: ', x, 'x', y);

            return click_xy(obj_for_click, x, y, button);

        }

    }).delay(2000).then(function () {

        debug.log(script_name, 'step 3');

        if (!thumb.x || !thumb.y) {
            var x              = random.int(100, 700);
            var y              = random.int(150, 600);
            param.popupLoading = false;
            param.page_created = false;
            console.log(script_name, ' ### click_xy() ### no_thumbs (second click)-> random coord: ', x, 'x', y);

            return click_xy(obj_for_click, x, y, button);
        }

    }).delay(config.timeout.wait_popup).then(function () {

        debug.log(script_name, ' ### click_xy() finished ### ');
        debug.log(script_name, ' step 4 ');

        if (thumb.x && thumb.y) {

            debug.log(script_name, 'wait when popup loading will be finished ,  param.popupLoading = '+param.popupLoading);

            //wait when popup loading will be finished
            waitFor(function () {
                return !param.popupLoading;
            }, function () {

                debug.log(script_name, 'promise waitFor - run check_pages(obj_main),  param.popupLoading = '+param.popupLoading);

                obj_for_click = check_pages(obj_main);

                debug.log(script_name, ' promise waitFor - finished !param.popupLoading,  param.popupLoading = '+param.popupLoading);

                deferred.resolve({status: 'success'});

            }, config.timeout.loading_popup);

        }

    }).catch(function (e) {

        debug.log(script_name, '  [ERROR] catch! ');
        debug.log(script_name, 'catch: ', e.message||JSON.stringify(e));

        deferred.reject(e);

    }).done(function () {

        debug.log(script_name, '');
        debug.log(script_name, '<---- done ( look promise waitFor)');
        debug.log(script_name, '');

        deferred.resolve({status: 'success'});

    });

    return deferred.promise;
};

//wait for something
function waitFor(testFx, onReady, timeOut, timeFrequency) {
    var maxtimeOutMillis = timeOut ? timeOut : 3000,
        start            = new Date().getTime(),
        condition        = false,
        interval         = setInterval(function () {
            if ((new Date().getTime() - start < maxtimeOutMillis) && !condition) {
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx());
            } else {
                if (!condition) {
                    debug.log(script_name, "'waitFor()' timeout");
                    clearInterval(interval);
                    typeof(onReady) === "string" ? eval(onReady) : onReady();
                }
                else {
                    debug.log(script_name, "'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                    clearInterval(interval);
                    typeof(onReady) === "string" ? eval(onReady) : onReady();
                }
            }
        }, timeFrequency);
}
