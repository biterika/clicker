// 1.15.0
var domain       = require('./../lib/domain');
var debug        = require('./../lib/debug');
var time         = require('./../lib/time');
var random       = require('./../lib/random');
var config       = require('./../inc/config');
var uagent       = require('./../inc/uagent');
var screens      = require('./../inc/screen');
var param        = require('./../inc/param');
var getNavigator = require('./getNavigator').exec;
var Q            = require('./../modules/q/q.js');
var colors       = require('./../modules/colors/safe.js');
var retry_back   = 0;


phantom.onError = function (msg, trace) {

    if (msg) debug.log(colors.bgBlack.red('phantom.onError, msg: ' + JSON.stringify(msg)));
    if (trace) debug.log(colors.bgBlack.red('phantom.onError, trace: ' + JSON.stringify(trace)));
};

var timers    = {};
var screen    = screens.list[random.int(0, screens.list.length - 1)].split('x');
param.task.sx = screen[0];
param.task.sy = screen[1];

param.newNavigator = getNavigator(/*param.task.lang || 'en-US'*/);
param.ua_save      = param.newNavigator.user_agent;
var useragent      = param.newNavigator.user_agent;
debug.log('events [START NEW NAVIGATOR]', 'useragent= ', useragent, 'param.task.lang = ', param.task.lang, ' screen =', screen /* , ' navigator=', param.newNavigator*/);

var url_actions = {};
var script_name = 'events.js';
debug.log('events start screen', screen, 'useragent', useragent);


exports.exec = function (obj) {

    param.step++;

    debug.log('');
    debug.log('Apply events...');

    var deferred = Q.defer();

    onPageCreated(obj);

    var result = {
        status: 'success'
    };
    deferred.resolve(result);

    return deferred.promise;

};

exports.setTimeoutStop = function (obj, timeout) {

    setTimeout(function () {
        if (typeof obj != 'undefined' && obj) {
            try {
                debug.log(script_name, ' setTimeoutStop -> obj.stop();', 'obj.id', obj.id, 'obj.url', obj.url);
                obj.stop();
            }
            catch (e) {
                debug.log(script_name, ' setTimeout CATCH obj.stop(); ', e.message || JSON.stringify(e))
            }
        }
    }, timeout);

};

function onPageCreated(obj) {

    param.page_id++;
    obj.id           = param.page_id;
    obj.page_open_ts = time.time_int();

    if (param.close_popup) {

        debug.log(script_name, 'Event: ClosePopup - start  id', obj.id, ' url', obj.url);

        if (obj && !obj.page_closed_already) {

            obj.page_closed_already = true;
            obj.freeze              = true;

            obj.navigationLocked = true;

            setTimeout(function (obj) {
                try {
                    debug.log(script_name, ' onPageCreated -> (param.close_popup) -> obj.stop() timeout 0 ', 'obj.id', obj.id, 'obj.url', obj.url);
                    obj.stop();
                }
                catch (e) {
                    debug.log(script_name, ' CATCH onPageCreated -> (param.close_popup) -> obj.stop()  ');
                }

                try {
                    debug.log(script_name, ' onPageCreated -> (param.close_popup) -> obj.close() timeout 100 ', 'obj.id', obj.id, 'obj.url', obj.url);
                    obj.close();
                }
                catch (e) {
                    debug.log(script_name, ' CATCH onPageCreated -> (param.close_popup) -> obj.close() timeout 100  ');
                }
            }, 100, obj);


            debug.log(script_name, colors.bgBlack.red('Event: ClosePopup - finished'));
        }
        else {
            debug.log(script_name, 'Event: ClosePopup - [skip] finished');
        }

    }
    else {


        param.popupLoading = true;
        param.page_created = true;

        debug.log('obj.pageLoading = true');
        obj.pageLoading = true;

        obj.domain = domain.getDomain(obj.url);

        obj.requests     = 0;
        obj.requests_ext = {};
        obj.load_fin     = 0;
        obj.recived_ext  = {};

        obj.load_resource_fin = 0;
        obj.changed           = 0;

        obj.viewportSize = {
            width:  param.task.sx || 1366,
            height: param.task.sy || 768
        };
        obj.clipRect     = {top: 0, left: 0, width: param.task.sx || 1366, height: param.task.sy || 768};

        obj.settings.userAgent          = param.ua_save;
        obj.settings.webSecurityEnabled = false;
        obj.customHeaders               = {
            'User-Agent':      param.ua_save,
            'Accept-Language': param.task.lang
        };


        obj.onInitialized = function () {


            obj.settings.webSecurityEnabled = false;

           /* function navigator_init_complete(page_id) {
                console.log('********* navigator_init_complete ***** obj.id: ' + page_id);
            }*/

            obj.evaluate(function (sx, sy, newNavigator, page_id, navigator_init_complete) {


                window.screen.availWidth  = sy;
                window.screen.availHeight = sx - 40;
                window.screen.width       = sy;
                window.screen.height      = sx;

                window.screen.colorDepth = 24;
                window.screen.pixelDepth = 24;

                window.screen.availLeft = 0;
                window.screen.availTop  = 23;

                window.screen.orientation = {
                    "angle":               0,
                    "type":                "landscape-primary",
                    "onchange":            null,
                    "lock":                function () {
                        return true;
                    },
                    "unlock":              function () {
                        return true;
                    },
                    "addEventListener":    function () {
                        return true;
                    },
                    "removeEventListener": function () {
                        return true;
                    },
                    "dispatchEvent":       function () {
                        return true;
                    }
                };

                delete window._phantom;
                delete window.callPhantom;


                var navigator_obj = newNavigator = JSON.parse(newNavigator);


                function deserializeNavparam(param, value) {

                    try {
                        if (param == 'length') {
                            return +value;
                        } else {
                            switch (value) {
                                case 'functionValue':
                                    return function () {
                                        return false;
                                    };
                                    break;
                                case 'nullValue':
                                    return null;
                                    break;
                                case 'true':
                                    return true;
                                    break;
                                case 'false':
                                    return false;
                                    break;
                                default:
                                    return value;
                                    break;
                            }
                        }
                    }
                    catch (err) {
                        console.log(' CATCH deserializeNavparam() ');
                        console.log(' CATCH deserializeNavparam()  err: '+err.message||JSON.stringify(err));
                    }
                }

                try {
                for (var param_a in navigator_obj) {

                    if (typeof navigator_obj[param_a] == 'string') {
                        window.navigator[param_a] = deserializeNavparam(param_a, navigator_obj[param_a]);
                    }
                    else if (typeof navigator_obj[param_a] === null) {
                        window.navigator[param_a] = null;
                    }
                    else if (typeof navigator_obj[param_a] == 'object') {

                        if (!window.navigator[param_a]) window.navigator[param_a] = {};

                        for (var param_a2 in navigator_obj[param_a]) {


                            if (typeof navigator_obj[param_a][param_a2] == 'string') {
                                window.navigator[param_a][param_a2] = deserializeNavparam(param_a2, navigator_obj[param_a][param_a2]);
                            }
                            else {
                                window.navigator[param_a][param_a2] = navigator_obj[param_a][param_a2];
                            }
                        }
                    }
                    else {
                        window.navigator[param_a] = navigator_obj[param_a];
                    }

                }
                }
                catch (err) {
                    console.log(' CATCH [for loop in navigator_obj ]');
                    console.log(' CATCH [for loop in navigator_obj ] err: '+err.message||JSON.stringify(err));
                }

                var browser = 'n/a';
                if (newNavigator.userAgent.indexOf('Chrome') >= 0) browser = 'chrome';
                else if (newNavigator.userAgent.indexOf('Firefox') >= 0) browser = 'firefox';
                else if (newNavigator.userAgent.indexOf('Safari') >= 0) browser = 'safari';
                else if (newNavigator.userAgent.indexOf('Trident') >= 0) browser = 'ie';

                if (browser !== 'Safari' && browser !== 'Trident') {
                    window.navigator.sendBeacon = function (url, data) {
                        try {
                            var x = new XMLHttpRequest();
                            x.open('POST', url);
                            x.setRequestHeader("Content-Type", "text/plain;charset=UTF-8");
                            x.send(data);
                        } catch (e) {
                            return false;
                        }
                        return true;
                    };
                }


                window.navigator.javaEnabled = function () {
                    return true;
                };

                window.navigator.cookieEnabled = true;
                window.navigator.onLine        = true;

                console.log('@ onInitialized, set navigator userAgent for obj.id: ' + page_id + ' is set: ' + window.navigator.userAgent);

                //console.log('navigator.userAgent ' + navigator.userAgent);

               // navigator_init_complete(page_id);

            }, param.task.sx || 1366, param.task.sy || 768, param.newNavigator.navigator_json || '', obj.id || 'unknown');
            debug.log('onInitialized, Set JS variables  - finished');
        };

        obj.onError = function (msg, trace) {
            if (msg) debug.log(colors.bgBlack.red('objpage phantom.onError, msg: ' + JSON.stringify(msg)));
            if (trace) debug.log(colors.bgBlack.red('objpage phantom.onError, trace: ' + JSON.stringify(trace)));
        };

        obj.onPageCreated       = function (obj) {
            onPageCreated(obj);
        };
        obj.onResourceRequested = function (requestData, networkRequest) {
            onResourceRequested(obj, requestData, networkRequest);
        };
        obj.onResourceReceived  = function (response) {
            onResourceReceived(obj, response);
        };
        obj.onUrlChanged        = function (targetUrl) {
            onUrlChanged(obj, targetUrl);
        };
        obj.onClosing           = function (closingPage) {
            onClosing(closingPage);
        };
        obj.onLoadFinished      = function (status) {
            onLoadFinished(obj, status);
        };
        obj.onConsoleMessage    = function (msg, lineNum, sourceId) {
            onConsoleMessage(obj, msg, lineNum, sourceId)
        };
    }

    debug.log('------------------------------------------------------');
    debug.log(script_name, colors.bgMagenta.white(' onPageCreated() start, id: ', obj.id, 'url: ', obj.url));
    debug.log('------------------------------------------------------');

}

function onResourceRequested(obj, requestData, networkRequest) {

    var url = requestData.url||'no_url';

    if (!param.task.task_type && requestData.url.indexOf('/redirect_task.html?') >= 0) {

        param.args = qsParseArray(requestData.url);

        param.task.task_type    = getQueryVariable(requestData.url, 'action');
        param.task.site         = domain.getDomain(getQueryVariable(requestData.url, 'site'));
        param.task.trader       = domain.getDomain(getQueryVariable(requestData.url, 'trader'));
        param.task.param        = domain.getDomain(getQueryVariable(requestData.url, 'param'));
        param.task.task_id      = domain.getDomain(getQueryVariable(requestData.url, 'task_id'));
        param.task.task_uniq_id = domain.getDomain(getQueryVariable(requestData.url, 'task_uniq_id'));
        param.task.target_url   = decodeURIComponent(getQueryVariable(requestData.url, 'refer'));

        debug.log('  ', '');
        debug.log('  ', '===============-------------===============');
        debug.log('  ', ' ');
        debug.log('  ', '$$$$ Set task_type:', param.task.task_type);
        debug.log('  ', '');
        debug.log('  ', '===============-------------===============');
        debug.log('  ', 'Set task:', JSON.stringify(param.task), JSON.stringify(param.args));
        debug.log('  ', '===============-------------===============');
        debug.log('  ', ' ');
        debug.log('[TASK_DATA] [' + JSON.stringify(param.task) + ']');
    }


    if (requestData.url == 'data:text/html,%3Cscript%3Ewindow.close();%3C/script%3E') {
        networkRequest.abort();
        debug.log('  ', 'Event:', 'Requested in page: ABORT', obj.id, requestData.url);
    }

    if (url.indexOf('http://no.task/') >= 0) {
        debug.log(script_name, '  throw new Error NO_TASK ');
        throw new Error('NO_TASK');
    }

    //obj.freeze

    var tmp = requestData.url.split(".");
    var ext = tmp[tmp.length - 1];
    if (ext != 'gif' && ext != 'jpg' && ext != 'png' && ext != 'jpeg' && ext != 'js' && ext != 'css') {

        debug.log('  ', 'Event:', 'Requested in page:', obj.id, requestData.url);

        if (url_actions[obj.id]) {

            for (var id in requestData.headers) {
                if (requestData.headers.hasOwnProperty(id)) {
                    if (requestData.headers[id].name.toLowerCase() == 'referer') {
                        url_actions[obj.id].refer_url    = requestData.headers[id].value;
                        url_actions[obj.id].refer_domain = domain.getDomain(requestData.headers[id].value);
                    }
                }
            }

        }

    }
    obj.requests++;

}

function onResourceReceived(obj, response) {

    var tmp = response.url.split(".");
    var ext = tmp[tmp.length - 1];
    if (ext != 'gif' && ext != 'jpg' && ext != 'png' && ext != 'jpeg' && ext != 'js' && ext != 'css') {


        if (url_actions[obj.id]) {

            url_actions[obj.id].code = response.status;

            for (var id in response.headers) {
                if (response.headers.hasOwnProperty(id)) {
                    if (response.headers[id].name.toLowerCase() == 'location') {
                        url_actions[obj.id].redirect.push({
                            refer: response.url,
                            url:   response.headers[id].value
                        });

                    }
                }
            }

        }

    }


}

function check_domain(obj) {

    if (!param.task.tds) {

        debug.log('  ', 'set tds domain', obj.domain);
        param.task.tds = obj.domain;

    }
    else if (!param.task.site) {

        debug.log('  ', 'set site domain', obj.domain);
        param.task.site = obj.domain;

    }
    else if (obj.domain == param.task.site) {

        debug.log('  ', 'skip domain', obj.domain);

    }
    else {

        debug.log('  ', 'Set trader domain', obj.domain);
        param.task.trader = obj.domain;

    }

}


function onUrlChanged(obj, targetUrl) {

    debug.log('onUrlChanged', colors.bgBlue.white('   ::onUrlChanged:: ------->>>>>> ', obj.id, targetUrl));

    obj.changed++;
    param.popupLoading = true;
    debug.log('onUrlChanged', 'obj.pageLoading = true');
    obj.pageLoading = true;

    obj.domain                 = domain.getDomain(targetUrl);
    //url_actions[obj.id].url    = targetUrl;
    //url_actions[obj.id].domain = domain.getDomain(targetUrl);
    debug.log('[_urls_log] [' + targetUrl + ']');


    var log2 = {
        time:         time.time(),
        type:         'onUrlChanged',
        status:       status || null,
        page_id:      obj.id || null,
        page_page_id: obj.page_id || null,
        code:         obj.code || null,
        url:          targetUrl || null,
        domain:       domain.getDomain(targetUrl) || null,
        refer_url:    obj.refer_url || null,
        refer_domain: obj.refer_domain || null,
        createdAt:    obj.createdAt || null,
        content:      obj.content.length || null,
        redirect:     obj.redirect || null
    };

    if (!param.url_log_page[obj.id]) param.url_log_page[obj.id] = [];
    param.url_log_page[obj.id].push(targetUrl);

    if (!param.url_log_page_count[obj.id]) param.url_log_page_count[obj.id] = {};
    if (!param.url_log_page_count[obj.id][targetUrl]) param.url_log_page_count[obj.id][targetUrl] = 0;
    param.url_log_page_count[obj.id][targetUrl]++;


    param.url_log2.push(log2);
    debug.log('onUrlChanged '+ JSON.stringify(log2));
    if (!log2.url) debug.log('!log2.url');
    if (!log2.domain) debug.log('!log2.url');
    if (log2.url && log2.domain) {

        if (!param.counters.start) console.log('!param.counters.start');
        if (param.counters.start) {

            /*if (!param.task.trader) debug.log('!param.task.trader');
            if (param.popupLoading) {
                debug.log(' ================================================================================== ');
                debug.log('           popunder open     '+ log2.domain+ '     '+ log2.url);
                debug.log(' ================================================================================== ');
                debug.log('[_urls_log_pop] [' + targetUrl + ']');
                param.counters.pages_pop.push(log2.url);
                param.counters.count_pop++;
            }*/
            if (param.task.trader) {
                var skip = false;

                var click_external = param.counters.count_ext;
                var click_content  = param.counters.count_int;
                var click_all      = click_external + click_content;

                if (click_all >= param.task.click_ok) {
                    skip = true;
                    debug.log(' --- skip count opening pages =>click_all ' + click_all + ' >= param.task.click_ok ' + param.task.click_ok + '   ------- ');
                }

                if (!skip && log2.domain == param.task.trader) {


                    if (param.back_case_no_count[obj.id] && param.back_case_no_count[obj.id][obj.url]) {
                        skip = true;
                        delete param.back_case_no_count[obj.id][obj.url];
                        debug.log(' ---- skip count page after go back -----');
                    }
                    else {

                        debug.log('no skip, param.back_case_no_count '+ JSON.stringify(param.back_case_no_count));
                    }

                    if (!skip && param.counters.pages_int.indexOf(log2.url) == -1) {
                        param.counters.pages_int.push(log2.url);
                        param.counters.count_int++;
                        debug.log(colors.bgBlue.white('onUrlChanged, ------->>>>>> param.counters.count_int++ '+ log2.url+ ' count '+ param.counters.count_int));
                    }
                    else {
                        debug.log('param.counters.pages_int.indexOf(log2.url) =  '+ param.counters.pages_int.indexOf(log2.url)+'   '+ log2.url);
                    }
                }
                else if (log2.url !== 'about:blank') {
                    if (param.counters.pages_ext.indexOf(log2.url) == -1) {
                        param.counters.pages_ext.push(log2.url);
                        param.counters.count_ext++;
                        debug.log(colors.bgBlue.white('onUrlChanged, ------->>>>>> param.counters.count_ext++ '+ log2.url+ ' count '+ param.counters.count_ext));
                    }
                    else {
                        debug.log('param.counters.pages_ext.indexOf(log2.url) = '+ param.counters.pages_ext.indexOf(log2.url)+'   '+ log2.url);
                    }
                }
            }
        }

        if (param.counters.pages_all.indexOf(log2.url) == -1) {
            param.counters.pages_all.push(log2.url);
            param.counters.count_all++;
            debug.log(colors.bgBlue.white('onUrlChanged, ------->>>>>> param.counters.count_all++ '+ log2.url));
        } else {
            debug.log('param.counters.pages_all.indexOf(log2.url) = '+ param.counters.pages_all.indexOf(log2.url)+' '+ log2.url);
        }
    }
    debug.log('onUrlChanged end');


    debug.log(colors.bgWhite.magenta('param.counters.count_int / ext : ' + param.counters.count_int + ' / ' + param.counters.count_ext));
    debug.log(colors.bgWhite.black('param.url_log_page = '+ JSON.stringify(param.url_log_page[obj.id], '', '\t')));


    if (param.url_log_page_count[obj.id] &&
        param.url_log_page_count[obj.id][targetUrl] &&
        param.url_log_page_count[obj.id][targetUrl] > 3) {

        debug.log(colors.bgYellow.black('   Event: obj.stop(); back x2, obj.id: '+ obj.id+' url: '+ targetUrl));

        obj.stop();

        if (!param.back_case_no_count[obj.id]) param.back_case_no_count[obj.id] = {};

        if (param.url_log_page.length > 1) {
            debug.log('param.url_log_page.length>1 '+ param.url_log_page);
            if (param.url_log_page[param.url_log_page.length - 2]) {

                debug.log('param.url_log_page[param.url_log_page.length-2] '+ param.url_log_page[param.url_log_page.length - 2]);
                param.back_case_no_count[obj.id][param.url_log_page[param.url_log_page.length - 2]] = true;
            }
        }
        debug.log('param.back_case_no_count '+ JSON.stringify(param.back_case_no_count));

        window.history.back();
        window.history.back();
    }
    else if (param.task.task_type && param.task.task_type.substr(0, 7) == 'trader.') {

        debug.log(script_name, 'check ext page ', param.task.task_type, param.task.trader, obj.domain, param.task.site, targetUrl);


        if (
            param.counters.start &&
            param.task.trader &&
            obj.domain != param.task.trader &&
            targetUrl != 'about:blank' &&
            targetUrl != 'bad.proxy'
        ) {
            debug.log('   Event: STOP', obj.id, targetUrl);

            if (retry_back <= 3) {
                debug.log(script_name,'retry_back <= 3',' obj.stop() ');
                obj.stop();

                if (!param.back_case_no_count[obj.id]) param.back_case_no_count[obj.id] = {};

                if (param.url_log_page.length > 1) {
                    debug.log('param.url_log_page.length>1 '+ param.url_log_page);

                    if (param.url_log_page[param.url_log_page.length - 2]) {

                        debug.log('param.url_log_page[param.url_log_page.length-2] '+ param.url_log_page[param.url_log_page.length - 2]);
                        param.back_case_no_count[obj.id][param.url_log_page[param.url_log_page.length - 2]] = true;
                    }
                }
                debug.log('param.back_case_no_count '+ JSON.stringify(param.back_case_no_count));
                retry_back++;
                debug.log(colors.bgYellow.black('Event: back: retry_back<=3 retry_back = ', retry_back, targetUrl));
                obj.evaluate(function () {
                    window.history.back();
                    //  window.history.back();
                });


                param.task.click_external++;
            }
            else if (retry_back <= 5) {

                obj.stop();
                debug.log(' retry_back <= 5 -----> obj.stop(); ');

                if (!param.back_case_no_count[obj.id]) param.back_case_no_count[obj.id] = {};
                if (param.url_log_page.length > 1) {
                    debug.log('retry_back <= 5 && param.url_log_page.length>1 '+ param.url_log_page);
                    if (param.url_log_page[param.url_log_page.length - 2]) {

                        debug.log('param.url_log_page[param.url_log_page.length-2] ', param.url_log_page[param.url_log_page.length - 2]);
                        param.back_case_no_count[obj.id][param.url_log_page[param.url_log_page.length - 2]] = true;
                    }
                }
                debug.log('param.back_case_no_count ', JSON.stringify(param.back_case_no_count));

                retry_back++;
                debug.log(colors.bgYellow.black('Event: STOP: retry_back<=5 retry_back = ', retry_back, targetUrl));
                obj.evaluate(function () {
                    window.history.back();
                    window.history.back();
                });
                // if (param.counters.count_int>0) param.counters.count_int--;
                param.task.click_external++;
            }
            else {
                debug.log(colors.bgYellow.black('Event: STOP: no did back, retry_back try over limit'));
            }

            debug.log(script_name, colors.bgWhite.red('param.task.click_external++ click_external = ' + param.task.click_external));
        }
    }

    console.log('onUrlChanged finished');
}

function onLoadFinished(obj, status) {


    debug.log(colors.bgMagenta.white('    ::onLoadFinished:: ------->>>>>> ', obj.id, obj.url));

    var log2 = {
        time:         time.time(),
        type:         'onLoadFinished',
        status:       status || null,
        page_id:      obj.id || null,
        page_page_id: obj.page_id || null,
        code:         obj.code || null,
        url:          obj.url || null,
        domain:       obj.domain || null,
        refer_url:    obj.refer_url || null,
        refer_domain: obj.refer_domain || null,
        createdAt:    obj.createdAt || null,
        content:      obj.content.length || null,
        redirect:     obj.redirect || null
    };


    param.url_log2.push(log2);
    debug.log('onLoadFinished', JSON.stringify(log2));
    if (log2.url && log2.domain) {
        if (param.counters.start) {
            if (param.task.trader) {
                if (log2.domain == param.task.trader) {
                    if (param.counters.pages_int.indexOf(log2.url) == -1) {
                        param.counters.pages_int.push(log2.url);
                        param.counters.count_int++;
                        debug.log('onLoadFinished, ------->>>>>> param.counters.count_int++ ', log2.url, ' count ', param.counters.count_int);
                    }
                }
                else if (log2.url !== 'about:blank') {
                    if (param.counters.pages_ext.indexOf(log2.url) == -1) {
                        param.counters.pages_ext.push(log2.url);
                        param.counters.count_ext++;
                        debug.log('onLoadFinished, ------->>>>>> param.counters.count_ext++ ', log2.url, ' count ', param.counters.count_ext);
                    }
                }
            }
        }

        if (param.counters.pages_all.indexOf(log2.url) == -1) {
            param.counters.pages_all.push(log2.url);
            param.counters.count_all++;
            debug.log('onLoadFinished, ------->>>>>> param.counters.count_all++ ', log2.url, ' count', param.counters.count_all);
        }
    }

    if (!url_actions[obj.id]) debug.log('!url_actions[obj.id]');
    if (url_actions[obj.id]) {

        var log = {
            page_id:      url_actions[obj.id].page_id,
            code:         url_actions[obj.id].code,
            url:          url_actions[obj.id].url,
            domain:       url_actions[obj.id].domain,
            refer_url:    url_actions[obj.id].refer_url,
            refer_domain: url_actions[obj.id].refer_domain,
            createdAt:    url_actions[obj.id].createdAt,
            content:      obj.content.length,
            redirect:     url_actions[obj.id].redirect
        };
        param.url_log.push(log);
        debug.log('log url_actions[obj.id] = ', JSON.stringify(url_actions[obj.id]));


        url_actions[obj.id].code         = 0;
        url_actions[obj.id].url          = '';
        url_actions[obj.id].domain       = '';
        url_actions[obj.id].refer_url    = '';
        url_actions[obj.id].refer_domain = '';
        url_actions[obj.id].redirect     = [];

    }

    if (param.task.trader) {

        clearTimeout(timers[obj.id]);
        delete timers[obj.id];

        debug.log(script_name,'  if (param.task.trader) -> obj.freeze = true, id ',obj.id);
        obj.freeze = true;

        //obj.navigationLocked = true;

        debug.log(' ----- ');
        debug.log('  ', 'Event:', '* LoadFinished *', obj.id, 'requests:', obj.requests, 'content len:', obj.content.length);
        debug.log(' ----- ');
        //debug.log('  ', obj.content);

        var content_short = obj.content.substr(0, 512) + '...';

        debug.log();
        debug.log('events.js', 'content:', 'page_id: ', obj.id, 'url: ', obj.url);
        debug.log('content:');
        debug.log('==================================================================================');
        console.log(content_short);
        debug.log('==================================================================================');
        debug.log();


        param.popupLoading = false;

        debug.log(script_name, 'param.popupLoading = false, obj.pageLoading = false');
        obj.pageLoading = false;


    }

}
function onClosing(closingPage) {

    debug.log('  ', 'Event:', 'Closing', closingPage.id, closingPage.url);

}
function onConsoleMessage(obj, msg, lineNum, sourceId) {

    debug.log('  obj.id/url: ',obj.id,obj.url,'msg: ', colors.bgWhite.red(msg));

}

function getQueryVariable(query, variable) {
    var vars = query.split("?")[1].split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
}

function qsParseArray(query) {
    var res  = [];
    var vars = query.split("?")[1].split("&");
    for (var i = 0; i < vars.length; i++) {
        res.push(vars[i].split("="));
    }
    return res; //decodeURIComponent
}


function parseTag(html, tag1, tag2, opt) {

    var rexp   = new RegExp(tag1 + '(.*?)' + tag2, 'g');
    var rexp2  = new RegExp(tag2, 'g');
    var result = html.match(rexp).map(function (val) {
        return val.replace(rexp2, '');
    });

    return result;

}


