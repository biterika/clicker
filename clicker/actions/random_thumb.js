var config = require('./../inc/config');
var param  = require('./../inc/param');
var random = require('./../lib/random');
var debug  = require('./../lib/debug');
var Q      = require('./../modules/q/q.js');

exports.exec = function (obj, screen_x, screen_y) {

    if (screen_x) param.task.sx = screen_x;
    if (screen_y) param.task.sy = screen_y;

    var content_html = obj.content;
    if (content_html && content_html.length > 256) {
        debug.log('random_thumb() html tag = ', content_html.substr(0, 256))
    }else {
        debug.log('random_thumb() error no obj.content or len<256 ');
        return {};
    }

    debug.log('random_thumb() param.task.sx = ' + param.task.sx + ' param.task.sy = ' + param.task.sy, 'obj.id: ',obj.id, ' url ', obj.url);
    debug.log('random_thumb() obj.scrollPosition = '+ JSON.stringify(obj.scrollPosition));

    var thumbs_all = [];
    if (typeof obj != 'undefined' && obj) {
        thumbs_all = obj.evaluate(function (thumb_top_count, param_sx, param_sy) {

                var scroll_body_elem = document.body || false;
                if (scroll_body_elem) {
                    console.log('random_thumb() scrollTop = ' + document.body.scrollTop || 'err no scrollTop');
                }

                function getOffsetRect(elem) {
                    var box    = elem.getBoundingClientRect();
                    var left   = box.left;
                    var top    = box.top;
                    var width  = box.width;
                    var height = box.height;
                    return {
                        x:      Math.round(left),
                        y:      Math.round(top),
                        width:  width,
                        height: height,
                        src:    elem.src || 'n/a'
                    }
                }

                var thumb_top_res = [];
                var mass          = document.querySelectorAll("a img");
                var sizes         = {};
                for (var i = 0; i < mass.length; i++) {
                    if (mass[i].height >= 50 && mass[i].width >= 50) {
                        var rect = getOffsetRect(mass[i]);
                        if (rect.x > 0 && rect.y > 0 && rect.width > 0 && rect.height > 0) {
                            var size = rect.width + 'x' + rect.height;
                            if (!sizes[size]) sizes[size] = [];
                            sizes[size].push(rect);
                        }
                    }
                }
                var len = 0;
                var thumbs_size = 'no have';
                var thumbs_amount = 'no have';
                for (var this_size in sizes) {
                    console.log('random_thumb(), size '+this_size+' amount '+sizes[this_size].length);
                    if (len < sizes[this_size].length) {
                        len           = sizes[this_size].length;
                        thumb_top_res = sizes[this_size];
                        thumbs_size = this_size;
                        thumbs_amount = len;
                    }
                }
                console.log('random_thumb(), selected thumbs:  '+JSON.stringify(thumb_top_res));
                console.log('random_thumb(), selected thumbs_size '+thumbs_size+' thumbs_amount '+ thumbs_amount);

                return thumb_top_res;
            },
            config.const.thumb_top_count, param.task.sx, param.task.sy);
    }

    if (!thumbs_all || thumbs_all.length < 1) {

        debug.log('--------------- THUMBS NOT FINDED ! --- url: ', obj.url || 'n/a', ' params: ', config.const.thumb_top_count, param.task.sx, param.task.sy, ' page content = ', obj.content, '---------------');
        debug.log('---------------END page content (Thumbs not found)---------------');
        return {};
    }
    else {

        var selected_thumb = thumbs_all[random.int(0, thumbs_all.length - 1)];

        debug.log(' selected_thumb = '+JSON.stringify(selected_thumb,'','\t'));

        return selected_thumb;

    }

};
