var config  = require('./../inc/config');
var param   = require('./../inc/param');
var random  = require('./../lib/random');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

exports.exec = function(obj,screen_x,screen_y){

    if (screen_x) param.task.sx= screen_x;
    if (screen_y) param.task.sy= screen_y;

    console.log('random_thumb() param.task.sx = '+param.task.sx+' param.task.sy = '+param.task.sy, ' url ',obj.url);

    //console.log('random_thumb() obj.scrollPosition = ', JSON.stringify(obj.scrollPosition));

    var thumbs_all = [];
    if(typeof obj != 'undefined' && obj){
        thumbs_all = obj.evaluate(function(thumb_top_count, param_sx,param_sy) {

            //console.log('random_thumb() scrollTop = '+document.body.scrollTop);

                function getOffsetRect(elem) {
                var box = elem.getBoundingClientRect();
                var body = document.body;
                var docElem = document.documentElement;
                var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
                var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
                var clientTop = docElem.clientTop || body.clientTop || 0;
                var clientLeft = docElem.clientLeft || body.clientLeft || 0;
                var top  = box.top +  scrollTop - clientTop;
                var left = box.left + scrollLeft - clientLeft;
                var width = elem.offsetWidth;
                var height = elem.offsetHeight;
                return { x: Math.round(left), y: Math.round(top), width: width, height: height }
            }
            var thumb_top_res = [];
            var mass = document.querySelectorAll("a img");
            var sizes = {};
            for (var i = 0; i < mass.length; i++) {
                if(mass[i].height >= 50){
                    var rect = getOffsetRect(mass[i]);
                    if(rect.x > 0 && rect.y > 0 && rect.width > 0 && rect.height > 0){

                        if (rect.x < param_sx && rect.y < param_sy) {
                            console.log(JSON.stringify(rect), mass[i].src||'n/a', JSON.stringify(mass[i].getBoundingClientRect()));
                            var size = rect.width + 'x' + rect.height;
                            if (typeof sizes[size] == 'undefined') sizes[size] = [];
                            sizes[size].push(rect);
                        }
                    }
                }
            }
            var len = 0;
            for (var this_size in sizes) {
                if (len < sizes[this_size].length) {
                    len = sizes[this_size].length;
                    thumb_top_res = sizes[this_size].slice(0, thumb_top_count);
                }
            }
            return thumb_top_res;
        },
            config.const.thumb_top_count, param.task.sx, param.task.sy);
    }

    if(thumbs_all.length < 1) {

        debug.log('Thumbs not found. url: ',obj.url||'n/a',' params: ',config.const.thumb_top_count, param.task.sx, param.task.sy, ' page content = ', obj.content);
        debug.log('END page content (Thumbs not found)');
        return {};
    }
    else{

        return thumbs_all[random.int(0, thumbs_all.length-1)];

    }

};
