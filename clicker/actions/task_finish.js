var fs      = require('fs');

var time    = require('./../lib/time');
var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

//var setTimeoutStop = require('./../actions/events').setTimeoutStop;

exports.exec = function(task){

    param.step++;

    debug.log('');
    debug.log('Task finish:', task.code, 'Clicks:', task.click_ok, '/', task.click_task);

    var deferred = Q.defer();

    var page_task = require('webpage').create();
    //setTimeoutStop(page_task, config.timeout.loading);

    var url = param.finish_url +
        '&code='    + task.code +
        '&server='  + task.server +
        '&login='   + task.login +
        '&token='   + task.token +
        '&balancer=' + task.balancer +
            //'&token_cache=' + task.token_cache +
            //'&ip='      + task.ip +
            //'&fake='    + task.fake +
        '&site='    + task.site +
        '&trader='  + task.trader +
        '&task_type=' + task.task_type +
        '&click_task=' + task.click_task +
        '&click_ok=' + task.click_ok +
        '&delta='   + task.delta;

    for(var id in param.args){
        if(param.args.hasOwnProperty(id)){
            url += '&'+param.args[id][0]+'='+param.args[id][1];
        }
    }

    debug.log('finish_url', url);

    var now = new Date().getTime();
    var time_start = parseInt(now, 10);

    page_task.open(url, function (status) {

        console.log(new Date(),status);
        console.log(JSON.stringify(param.task));

        setTimeout(function(){ phantom.exit(0); }, 0);

        /*var now = new Date().getTime();
        var time_end = parseInt(now, 10);
        var time_delta = time_end-time_start;

        debug.log('status', status);
        var result = {
            status: status,
            data: page_task.plainText
        };
        page_task.close();
        deferred.resolve(result);*/

    });

    return deferred.promise;

};
