var domain       = require('./../lib/domain');
var debug        = require('./../lib/debug');
var time         = require('./../lib/time');
var random       = require('./../lib/random');
var config       = require('./../inc/config');
var uagent       = require('./../inc/uagent');
var screens      = require('./../inc/screen');
var param        = require('./../inc/param');
var getNavigator = require('./getNavigator').exec;
var Q            = require('./../modules/q/q.js');
var colors       = require('./../modules/colors/safe.js');
var retry_back   = 0;


phantom.onError = function (msg, trace) {

    if (msg) debug.log(colors.bgBlack.red('phantom.onError, msg: ' + JSON.stringify(msg)));
    if (trace) debug.log(colors.bgBlack.red('phantom.onError, trace: ' + JSON.stringify(trace)));
};

var timers    = {};
//var useragent = uagent.list[random.int(0,uagent.list.length-1)];
var screen    = screens.list[random.int(0, screens.list.length - 1)].split('x');
param.task.sx = screen[0];
param.task.sy = screen[1];

param.newNavigator = getNavigator(/*param.task.lang || 'en-US'*/);
param.ua_save      = param.newNavigator.user_agent;
var useragent      = param.newNavigator.user_agent;
debug.log('events [START NEW NAVIGATOR]', 'useragent= ', useragent, 'param.task.lang = ', param.task.lang, ' screen =', screen, ' navigator=', param.newNavigator);

var url_actions = {};
var script_name = 'events.js';
debug.log('events start screen', screen, 'useragent', useragent);


exports.exec = function (obj) {

    param.step++;

    debug.log('');
    debug.log('Apply events...');

    var deferred = Q.defer();

    onPageCreated(obj);

    var result = {
        status: 'success'
    };
    deferred.resolve(result);

    return deferred.promise;

};

exports.setTimeoutStop = function (obj, timeout) {

    setTimeout(function () {
        if (typeof obj != 'undefined' && obj) {
            try {
                debug.log(script_name, ' setTimeoutStop -> obj.stop();', 'obj.id', obj.id, 'obj.url', obj.url);
                obj.stop();
            } catch (e) {
            }
        }
    }, timeout);

};

function onPageCreated(obj) {

    param.page_id++;
    obj.id = param.page_id;
    obj.page_open_ts = time.time_int();

    if (param.close_popup) {
        debug.log(script_name, 'Event: ClosePopup - start  id', obj.id, ' url', obj.url);
        if (obj && !obj.page_closed_already) {

            obj.page_closed_already = true;
            obj.freeze              = true;

            obj.navigationLocked = true;

            setTimeout(function (obj) {
                try {
                    debug.log(script_name, ' onPageCreated -> (param.close_popup) -> obj.stop() timeout 0 ', 'obj.id', obj.id, 'obj.url', obj.url);
                    obj.stop();
                } catch (e) {
                    debug.log(script_name, ' CATCH onPageCreated -> (param.close_popup) -> obj.stop()  ');
                }

                try {
                    debug.log(script_name, ' onPageCreated -> (param.close_popup) -> obj.close() timeout 100 ', 'obj.id', obj.id, 'obj.url', obj.url);
                    obj.close();
                } catch (e) {
                    debug.log(script_name, ' CATCH onPageCreated -> (param.close_popup) -> obj.close() timeout 100  ');
                }
            }, 100, obj);


            debug.log(script_name, colors.bgBlack.red('Event: ClosePopup - finished'));
        } else {
            debug.log(script_name, 'Event: ClosePopup - [skip] finished');
        }

    }
    else {


        param.popupLoading = true;
        param.page_created = true;

        debug.log('obj.pageLoading = true');
        obj.pageLoading = true;

        obj.domain = domain.getDomain(obj.url);

        obj.requests     = 0;
        obj.requests_ext = {};
        obj.load_fin     = 0;
        obj.recived_ext  = {};

        obj.load_resource_fin = 0;
        obj.changed           = 0;

        obj.viewportSize = {
            width:  param.task.sx || 1366,
            height: param.task.sy || 768
        };
        obj.clipRect     = {top: 0, left: 0, width: param.task.sx || 1366, height: param.task.sy || 768};

        obj.settings.userAgent          = param.ua_save;
        obj.settings.webSecurityEnabled = false;
        obj.customHeaders               = {
            'User-Agent':      param.ua_save,
            'Accept-Language': param.task.lang
        };


        obj.onInitialized = function () {


            obj.settings.webSecurityEnabled = false;

            function navigator_init_complete(page_id) {
                console.log('********* navigator_init_complete ***** obj.id: ' + page_id);
            }

            obj.evaluate(function (sx, sy, newNavigator, page_id, navigator_init_complete) {


                window.screen.availWidth  = sy;
                window.screen.availHeight = sx - 40;
                window.screen.width       = sy;
                window.screen.height      = sx;

                window.screen.colorDepth  = 24;
                window.screen.pixelDepth  = 24;

                window.screen.availLeft   = 0;
                window.screen.availTop    = 23;

                window.screen.orientation = {
                        "angle":               0,
                        "type":                "landscape-primary",
                        "onchange":            null,
                        "lock":                function () {
                            return true;
                        },
                        "unlock":              function () {
                            return true;
                        },
                        "addEventListener":    function () {
                            return true;
                        },
                        "removeEventListener": function () {
                            return true;
                        },
                        "dispatchEvent":       function () {
                            return true;
                        }
                };

                delete window._phantom;
                delete window.callPhantom;


                var navigator_obj = newNavigator = JSON.parse(newNavigator);


                function deserializeNavparam(param, value) {

                    if (param == 'length') {
                        return +value;
                    } else {
                        switch (value) {
                            case 'functionValue':
                                return function () {
                                    return false;
                                };
                                break;
                            case 'nullValue':
                                return null;
                                break;
                            case 'true':
                                return true;
                                break;
                            case 'false':
                                return false;
                                break;
                            default:
                                return value;
                                break;
                        }
                    }
                }

                for (var param_a in navigator_obj) {

                    if (typeof navigator_obj[param_a] == 'string') {
                        window.navigator[param_a] = deserializeNavparam(param_a, navigator_obj[param_a]);
                    }
                    else if (typeof navigator_obj[param_a] === null) {
                        window.navigator[param_a] = null;
                    }
                    else if (typeof navigator_obj[param_a] == 'object') {

                        if (!window.navigator[param_a]) window.navigator[param_a] = {};

                        for (var param_a2 in navigator_obj[param_a]) {


                            if (typeof navigator_obj[param_a][param_a2] == 'string') {
                                window.navigator[param_a][param_a2] = deserializeNavparam(param_a2, navigator_obj[param_a][param_a2]);
                            }
                            else {
                                window.navigator[param_a][param_a2] = navigator_obj[param_a][param_a2];
                            }
                        }
                    }
                    else {
                        window.navigator[param_a] = navigator_obj[param_a];
                    }

                }


                var browser = 'n/a';
                if (newNavigator.userAgent.indexOf('Chrome') >= 0) browser = 'chrome';
                else if (newNavigator.userAgent.indexOf('Firefox') >= 0) browser = 'firefox';
                else if (newNavigator.userAgent.indexOf('Safari') >= 0) browser = 'safari';
                else if (newNavigator.userAgent.indexOf('Trident') >= 0) browser = 'ie';

                if (browser !== 'Safari' && browser !== 'Trident') {
                    window.navigator.sendBeacon = function (url, data) {
                        try {
                            var x = new XMLHttpRequest();
                            x.open('POST', url);
                            x.setRequestHeader("Content-Type", "text/plain;charset=UTF-8");
                            x.send(data);
                        } catch (e) {
                            return false;
                        }
                        return true;
                    };
                }


                window.navigator.javaEnabled = function () {
                    return true;
                };

                window.navigator.cookieEnabled = true;
                window.navigator.onLine = true;

                console.log('@ onInitialized, set navigator userAgent for obj.id: ' + page_id + ' is set: ' + window.navigator.userAgent);

                console.log('navigator.userAgent '+navigator.userAgent);

                navigator_init_complete(page_id);

            }, param.task.sx || 1366, param.task.sy || 768, param.newNavigator.navigator_json || '', obj.id || 'unknown', navigator_init_complete);
            debug.log('Set JS variables (l169), onInitialized - finished');
        };

        obj.onError = function (msg, trace) {
            if (msg) debug.log(colors.bgBlack.red('objpage phantom.onError, msg: ' + JSON.stringify(msg)));
            if (trace) debug.log(colors.bgBlack.red('objpage phantom.onError, trace: ' + JSON.stringify(trace)));
        };

        obj.onPageCreated       = function (obj) {
            onPageCreated(obj);
        };
        obj.onResourceRequested = function (requestData, networkRequest) {
            onResourceRequested(obj, requestData, networkRequest);
        };
        obj.onResourceReceived  = function (response) {
            onResourceReceived(obj, response);
        };
        obj.onUrlChanged        = function (targetUrl) {
            onUrlChanged(obj, targetUrl);
        };
        obj.onClosing           = function (closingPage) {
            onClosing(closingPage);
        };
        obj.onLoadFinished      = function (status) {
            onLoadFinished(obj, status);
        };
        obj.onConsoleMessage    = function (msg, lineNum, sourceId) {
            onConsoleMessage(obj, msg, lineNum, sourceId)
        };
    }

    debug.log('------------------------------------------------------');
    debug.log(script_name, colors.bgMagenta.white(' onPageCreated() start, id: ', obj.id, 'url: ', obj.url));
    debug.log('------------------------------------------------------');

}


function onResourceRequested(obj, requestData, networkRequest) {

    obj.requests++;

    var url       = obj.url;
    var tmp       = requestData.url.split(".");
    var ext       = tmp[tmp.length - 1];
    var know_exts = ['json', 'css', 'js', 'jpg', 'jpeg', 'png', 'gif'];
    var know_ext  = 'other';
    if (know_exts.indexOf(ext) >= 1)  know_ext = ext;
    if (!obj.requests_ext[know_ext]) obj.requests_ext[know_ext] = 0;
    obj.requests_ext[know_ext]++;

    if (know_ext=='js') {
        debug.log(colors.magenta('onResource - Requested (JS)'),' obj.id: ',obj.id, ' num:', obj.requests_ext[know_ext], ' ', requestData.url);
    }

    if (requestData.url.indexOf('c.cleanburg.com') >= 0) {
        debug.log('onResourceRequested', ' ', requestData.url);
    }


    if (!param.task.task_type && requestData.url.indexOf('/redirect_task.html?') >= 0) {

        param.args = qsParseArray(requestData.url);

        param.task.task_type = getQueryVariable(requestData.url, 'action');
        param.task.site      = domain.getDomain(getQueryVariable(requestData.url, 'site'));
        param.task.trader    = domain.getDomain(getQueryVariable(requestData.url, 'trader'));
        param.task.param     = domain.getDomain(getQueryVariable(requestData.url, 'param'));
        //param.task.lang = domain.getDomain(getQueryVariable(requestData.url, 'lang').split(',')[0]);
        //param.task.useragent = domain.getDomain(getQueryVariable(requestData.url, 'useragent'));
        param.task.task_id      = domain.getDomain(getQueryVariable(requestData.url, 'task_id'));
        param.task.task_uniq_id = domain.getDomain(getQueryVariable(requestData.url, 'task_uniq_id'));
        param.task.target_url   = decodeURIComponent(getQueryVariable(requestData.url, 'refer'));

        debug.log('  ', '');
        debug.log('  ', '===============-------------===============');
        debug.log('  ', ' ');
        debug.log(script_name, '$$$$ Set task_type:', param.task.task_type);
        debug.log('  ', '');
        debug.log('  ', '===============-------------===============');
        debug.log(script_name, 'Set task:', JSON.stringify(param.task), JSON.stringify(param.args));
        debug.log('  ', '===============-------------===============');
        debug.log('  ', ' ');
        debug.log(script_name, '[TASK_DATA] [' + JSON.stringify(param.task) + ']');
    }


    if (requestData.url == 'data:text/html,%3Cscript%3Ewindow.close();%3C/script%3E') {
        networkRequest.abort();
        debug.log(script_name, 'Event: Requested in page: ABORT', obj.id, requestData.url);
    }

    if (url.indexOf('http://no.task/') >= 0) {
        throw new Error('NO_TASK');
    }


    if (ext != 'gif' && ext != 'jpg' && ext != 'png' && ext != 'jpeg' && ext != 'js' && ext != 'css') {

        debug.log(script_name, 'Event: Requested in page:', obj.id, requestData.url);


    }
}

function onResourceReceived(obj, response) {

    if (response.url.indexOf('c.cleanburg.com') >= 0) {
        debug.log(colors.bgWhite.black('onResourceReceived obj.id:',obj.id, ' ',response.stage,' ', response.url));
    }



    // onResourceReceived calling twice, we prevent doublicate count
    if (!response || !response.stage || response.stage == 'start') return;

    if (obj.url && obj.redirectURL) {
        debug.log(script_name, ' onResourceReceived ----> obj.redirectURL: ', obj.redirectURL, ' obj.id: ', obj.id, ' url: ', obj.url);
    }

    obj.load_resource_fin++;

    var tmp       = response.url.split(".");
    var ext       = tmp[tmp.length - 1];
    var know_exts = ['json', 'css', 'js', 'jpg', 'jpeg', 'png', 'gif'];
    var know_ext  = 'other';
    if (know_exts.indexOf(ext) >= 1)  know_ext = ext;
    if (!obj.recived_ext[know_ext]) obj.recived_ext[know_ext] = 0;
    obj.recived_ext[know_ext]++;

    if (know_ext == 'js') {
        debug.log(colors.green('onResource-Received (JS)'),'obj.id:',obj.id, ' num:',obj.recived_ext[know_ext], ' ',response.stage, obj.url, ' know_ext ',know_ext);
    }


}

function check_domain(obj) {

    if (!param.task.tds) {

        debug.log(script_name, 'set tds domain', obj.domain);
        //debug.log('  ', 'set tds domain',obj.domain);
        param.task.tds = obj.domain;

    } else if (!param.task.site) {

        debug.log(script_name, 'set site domain', obj.domain);
        //debug.log('  ', 'set site domain',obj.domain);
        param.task.site = obj.domain;

    } else if (obj.domain == param.task.site) {

        debug.log(script_name, 'skip domain', obj.domain);
        //debug.log('  ', 'skip domain',obj.domain);

    } else {

        debug.log(script_name, 'Set trader domain', obj.domain);
        //debug.log('  ', 'Set trader domain',obj.domain);
        param.task.trader = obj.domain;

    }

}


function onUrlChanged(obj, targetUrl) {

    debug.log(script_name, colors.bgBlue.white('   ::onUrlChanged:: ------->>>>>> obj.id:', obj.id, ' url: ', targetUrl));

    obj.changed++;
    param.popupLoading = true;
    debug.log('obj.pageLoading = true');
    obj.pageLoading = true;

    if (
        targetUrl.indexOf('syndication.exoclick.com/splash') >= 0 ||
        targetUrl.indexOf('main.exoclick.com/click') >= 0
    )
    {
        param.counters.count_ext=1;

        debug.log(colors.bgBlue.black('onUrlChanged, ------ POPUNDER ------->>>>>> param.counters.count_ext++ (popunder)', ' count ', param.counters.count_ext, ' obj.id: ', obj.id||'n/a'), ' url: ', targetUrl);
        obj.popunder = true;
        obj.popunder_open_ts = time.time_int();
    }

    obj.domain = domain.getDomain(targetUrl);
    //url_actions[obj.id].url    = targetUrl;
    //url_actions[obj.id].domain = domain.getDomain(targetUrl);
    debug.log('[_urls_log] [' + targetUrl + ']');


    var log2 = {
        time:         time.time(),
        type:         'onUrlChanged',
        status:       status || null,
        page_id:      obj.id || null,
        page_page_id: obj.page_id || null,
        code:         obj.code || null,
        url:          targetUrl || null,
        domain:       domain.getDomain(targetUrl) || null,
        refer_url:    obj.refer_url || null,
        refer_domain: obj.refer_domain || null,
        createdAt:    obj.createdAt || null,
        content:      obj.content.length || null,
        redirect:     obj.redirect || null
    };

    if (!param.url_log_page[obj.id]) param.url_log_page[obj.id] = [];
    param.url_log_page[obj.id].push(targetUrl);

    if (!param.url_log_page_count[obj.id]) param.url_log_page_count[obj.id] = {};
    if (!param.url_log_page_count[obj.id][targetUrl]) param.url_log_page_count[obj.id][targetUrl] = 0;
    param.url_log_page_count[obj.id][targetUrl]++;


    param.url_log2.push(log2);
    debug.log('onUrlChanged', JSON.stringify(log2));
    if (!log2.url) debug.log('onUrlChanged !log2.url');
    if (!log2.domain) debug.log('onUrlChanged !log2.url');
    if (log2.url && log2.domain) {

        if (!param.counters.start) debug.log('onUrlChanged !param.counters.start');
        if (param.counters.start) {

            if (!param.task.trader) debug.log('onUrlChanged !param.task.trader');
            /*if ( param.popupLoading) {
                debug.log(' ================================================================================== ');
                debug.log('           popunder open     ',log2.domain,'     ',log2.url);
                debug.log(' ================================================================================== ');
                debug.log('[_urls_log_pop] [' + targetUrl + ']');
                param.counters.pages_pop.push(log2.url);
                param.counters.count_pop++;
            }*/
            if (param.task.trader) {
                var skip = false;

                var click_external = param.counters.count_ext;
                var click_content  = param.counters.count_int;
                var click_all      = click_external + click_content;

                if (click_all >= param.task.click_ok) {
                    skip = true; // wtf?
                    debug.log('onUrlChanged --- skip count opening pages =>click_all ' + click_all + ' >= param.task.click_ok ' + param.task.click_ok + '   ------- ');
                }

                if (!skip && log2.domain == param.task.trader) {


                    if (param.back_case_no_count[obj.id] && param.back_case_no_count[obj.id][obj.url]) {
                        skip = true;
                        delete param.back_case_no_count[obj.id][obj.url];
                        debug.log('onUrlChanged ---- skip count page after go back -----');
                    }
                    else {

                        debug.log('onUrlChanged no skip, param.back_case_no_count ', JSON.stringify(param.back_case_no_count));
                    }

                    if (!skip && param.counters.pages_int.indexOf(log2.url) == -1) {
                        param.counters.pages_int.push(log2.url);
                        param.counters.count_int++;
                        debug.log(colors.bgBlue.black('onUrlChanged, ------->>>>>> param.counters.count_int++ ', log2.url, ' count ', param.counters.count_int));
                    }
                    else {
                        debug.log('onUrlChanged param.counters.pages_int.indexOf(log2.url) = ', param.counters.pages_int.indexOf(log2.url), log2.url);
                    }
                }
                else if (log2.url !== 'about:blank') {

                    if (param.counters.pages_ext.indexOf(log2.url) == -1) {

                        param.counters.pages_ext.push(log2.url);

                        param.counters.count_ext++;

                        debug.log(colors.bgBlue.black('onUrlChanged, ------->>>>>> param.counters.count_ext++ ', log2.url, ' count ', param.counters.count_ext, ' obj.id: ', obj.id), ' url: ', obj.url);

                    }
                    else {
                        debug.log('onUrlChanged param.counters.pages_ext.indexOf(log2.url) = ', param.counters.pages_ext.indexOf(log2.url), log2.url);
                    }
                }
            }
        }

        if (param.counters.pages_all.indexOf(log2.url) == -1) {
            param.counters.pages_all.push(log2.url);
            param.counters.count_all++;
            debug.log(colors.bgBlue.black('onUrlChanged, ------->>>>>> param.counters.count_all++ ', log2.url, 'count: ', param.counters.count_all));
        } else {
            debug.log('param.counters.pages_all.indexOf(log2.url) = ', param.counters.pages_all.indexOf(log2.url), log2.url);
        }
    }
    debug.log('onUrlChanged end');


    debug.log(colors.bgWhite.magenta('param.counters.count_int / ext : ' + param.counters.count_int + ' / ' + param.counters.count_ext));
    debug.log(colors.bgWhite.black('param.url_log_page = ', JSON.stringify(param.url_log_page[obj.id], '', '\t')));


    if (param.url_log_page_count[obj.id][targetUrl] > 3) {
        debug.log(colors.bgYellow.black('   Event: STOP back x2', obj.id, targetUrl));

        debug.log(script_name, ' onUrlChanged ->  obj.stop(); ', 'obj.id', obj.id, 'obj.url', obj.url);
        obj.stop();
        if (!param.back_case_no_count[obj.id]) param.back_case_no_count[obj.id] = {};
        if (param.url_log_page.length > 1) {
            debug.log('param.url_log_page.length>1 ', param.url_log_page);
            if (param.url_log_page[param.url_log_page.length - 2]) {

                debug.log('param.url_log_page[param.url_log_page.length-2] ', param.url_log_page[param.url_log_page.length - 2]);
                param.back_case_no_count[obj.id][param.url_log_page[param.url_log_page.length - 2]] = true;
            }
        }
        debug.log('param.back_case_no_count ', JSON.stringify(param.back_case_no_count));

        window.history.back();
        window.history.back();
    }
    else if (param.task.task_type && param.task.task_type.substr(0, 7) == 'trader.') {

        debug.log('check ext page ', param.task.task_type, param.task.trader, obj.domain, param.task.site, targetUrl);

        // check ext page
        // param.task.task_type = trader.usb
        // param.task.trader    = ftt2.biterika.com
        // obj.domain           = smartcj.biterika.com
        // param.task.site      = smartcj.biterika.com
        // targetUrl            = http://smartcj.biterika.com/?9121


        if (param.counters.start && param.task.trader && obj.domain != param.task.trader /*&& obj.domain != param.task.site*/
            && targetUrl != 'about:blank' && targetUrl != 'bad.proxy'
        ) {
            debug.log('   Event: STOP', obj.id, targetUrl);

            if (retry_back <= 3) {

                debug.log(script_name, ' onUrlChanged -> (retry_back <= 3) -> obj.stop(); ', 'obj.id', obj.id, 'obj.url', obj.url);
                obj.stop();
                if (!param.back_case_no_count[obj.id]) param.back_case_no_count[obj.id] = {};
                if (param.url_log_page.length > 1) {
                    debug.log('param.url_log_page.length>1 ', param.url_log_page);
                    if (param.url_log_page[param.url_log_page.length - 2]) {

                        debug.log('param.url_log_page[param.url_log_page.length-2] ', param.url_log_page[param.url_log_page.length - 2]);
                        param.back_case_no_count[obj.id][param.url_log_page[param.url_log_page.length - 2]] = true;
                    }
                }

                debug.log('param.back_case_no_count ', JSON.stringify(param.back_case_no_count));
                retry_back++;
                debug.log(colors.bgYellow.black('Event: STOP: retry_back<=3 retry_back = ', retry_back, targetUrl));
                obj.evaluate(function () {
                    window.history.back();
                    //  window.history.back();
                });

                // if (param.counters.count_int>0) param.counters.count_int--;
                param.task.click_external++;
            }
            else if (retry_back <= 5) {

                debug.log(script_name, ' onUrlChanged -> (retry_back <= 5) -> obj.stop();', 'obj.id', obj.id, 'obj.url', obj.url);
                obj.stop();
                if (!param.back_case_no_count[obj.id]) param.back_case_no_count[obj.id] = {};
                if (param.url_log_page.length > 1) {
                    debug.log('param.url_log_page.length>1 ', param.url_log_page);
                    if (param.url_log_page[param.url_log_page.length - 2]) {

                        debug.log('param.url_log_page[param.url_log_page.length-2] ', param.url_log_page[param.url_log_page.length - 2]);
                        param.back_case_no_count[obj.id][param.url_log_page[param.url_log_page.length - 2]] = true;
                    }
                }
                debug.log('param.back_case_no_count ', JSON.stringify(param.back_case_no_count));

                retry_back++;
                debug.log(colors.bgYellow.black('Event: STOP: retry_back<=5 retry_back = ', retry_back, targetUrl));
                obj.evaluate(function () {
                    window.history.back();
                    window.history.back();
                });
                // if (param.counters.count_int>0) param.counters.count_int--;
                param.task.click_external++;
            }
            else {
                debug.log(colors.bgYellow.black('Event: STOP: no did back, retry_back try over limit'));
            }

            debug.log(script_name, colors.bgWhite.red('param.task.click_external++ click_external = ' + param.task.click_external));
        }
    }

}

function onLoadFinished(obj, status) {

    obj.load_fin++;

    debug.log(colors.bgMagenta.white('    ::onLoadFinished:: ------->>>>>> obj.id: ', obj.id, ' url: ', obj.url, '\nrequests_ext: ', JSON.stringify(obj.requests_ext), ' recived_ext: ', JSON.stringify(obj.recived_ext)));

    var log2 = {
        time:         time.time(),
        type:         'onLoadFinished',
        status:       status || null,
        page_id:      obj.id || null,
        page_page_id: obj.page_id || null,
        code:         obj.code || null,
        url:          obj.url || null,
        domain:       obj.domain || null,
        refer_url:    obj.refer_url || null,
        refer_domain: obj.refer_domain || null,
        createdAt:    obj.createdAt || null,
        content:      obj.content.length || null,
        redirect:     obj.redirect || null
    };


    param.url_log2.push(log2);
    debug.log('onLoadFinished', JSON.stringify(log2));
    if (log2.url && log2.domain) {
        /*if (param.counters.start) {
            if (param.task.trader) {
                if (log2.domain == param.task.trader) {
                    if (param.counters.pages_int.indexOf(log2.url) == -1) {
                        param.counters.pages_int.push(log2.url);
                        param.counters.count_int++;
                        debug.log('onLoadFinished, ------->>>>>> param.counters.count_int++ ',log2.url,' count ',param.counters.count_int);
                    }
                }
                else if ( log2.url !=='about:blank') {
                    if (param.counters.pages_ext.indexOf(log2.url) == -1) {
                        param.counters.pages_ext.push(log2.url);
                        param.counters.count_ext++;
                        debug.log('onLoadFinished, ------->>>>>> param.counters.count_ext++ ',log2.url,' count ',param.counters.count_ext);
                    }
                }
            }
        }*/

        /* if (param.counters.pages_all.indexOf(log2.url) == -1) {
             param.counters.pages_all.push(log2.url);
             param.counters.count_all++;
             debug.log('onLoadFinished, ------->>>>>> param.counters.count_all++ ',log2.url, ' count',param.counters.count_all);
         }*/
    }

    /*if (!url_actions[obj.id]) debug.log('!url_actions[obj.id]');
    if (url_actions[obj.id]) {

        var log = {
            page_id:      url_actions[obj.id].page_id,
            code:         url_actions[obj.id].code,
            url:          url_actions[obj.id].url,
            domain:       url_actions[obj.id].domain,
            refer_url:    url_actions[obj.id].refer_url,
            refer_domain: url_actions[obj.id].refer_domain,
            createdAt:    url_actions[obj.id].createdAt,
            content:      obj.content.length,
            redirect:     url_actions[obj.id].redirect
        };
        param.url_log.push(log);
        debug.log('log url_actions[obj.id] = ',JSON.stringify(url_actions[obj.id]));




        //debug.json(JSON.stringify(log));
        url_actions[obj.id].code         = 0;
        url_actions[obj.id].url          = '';
        url_actions[obj.id].domain       = '';
        url_actions[obj.id].refer_url    = '';
        url_actions[obj.id].refer_domain = '';
        url_actions[obj.id].redirect     = [];

    }*/

    if (param.task.trader) {

        clearTimeout(timers[obj.id]);
        delete timers[obj.id];
        obj.freeze = true;
        //obj.navigationLocked = true; // todo убрать

        debug.log(' ');
        debug.log(' ------------------------------------------------------- ');
        debug.log(script_name, colors.bgBlack.green('Event: * LoadFinished *  obj.id: ', obj.id, 'requests:', obj.requests, ' load_resource_fin: ', obj.load_resource_fin, ' content len:', obj.content.length, ' url: ', obj.url));

        var content_short = obj.content.substr(0, 512) + '...';
        if (obj.url.indexOf('/redirect_task.html?') >= 0)  {
            debug.log('indexOf redirect_task.html');
        }


        debug.log(' ');
        debug.log(script_name, 'content.len:', obj.content.length, 'obj.id: ', obj.id, 'url: ', obj.url);
        debug.log('content:');
        debug.log('==============================================');
        debug.log(content_short);
        debug.log('==============================================');
        debug.log(' ')
        ;
        debug.log(' ------------------------------------------------------- ');


        param.popupLoading = false;
        obj.pageLoading    = false;
        debug.log('obj.pageLoading = false');


    }

}

function onClosing(closingPage) {

    debug.log(script_name, colors.bgBlack.red('onClosing(), Event: Closing id, url', closingPage.id, closingPage.url));

}

function onConsoleMessage(obj, msg, lineNum, sourceId) {
    debug.log(script_name, colors.bgWhite.red('ConsoleMessage, obj.id: '+obj.id+'  msg: '+msg, ' lineNum: '+lineNum));
}

function getQueryVariable(query, variable) {
    var vars = query.split("?")[1].split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
}

function qsParseArray(query) {
    var res  = [];
    var vars = query.split("?")[1].split("&");
    for (var i = 0; i < vars.length; i++) {
        res.push(vars[i].split("="));
    }
    return res; //decodeURIComponent
}


function parseTag(html, tag1, tag2, opt) {

    /*
     The m (multiline) modifier makes ^ and $ match lines rather than the whole string.
     g выводит больше соответсвий но в одном списке
     */

    /*
     var str = "<b>Bob</b>, I'm <b>20</b> years old, I like <b>programming</b>.";

     var result = str.match(/<b>(.*?)<\/b>/g).map(function(val){
     return val.replace(/<\/?b>/g,'');
     });
     //result -> ["Bob", "20", "programming"]
     */


    var rexp   = new RegExp(tag1 + '(.*?)' + tag2, 'g');
    var rexp2  = new RegExp(tag2, 'g');
    var result = html.match(rexp).map(function (val) {
        return val.replace(rexp2, '');
    });

    return result;
    //return result;

}


