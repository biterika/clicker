var fs      = require('fs');

var time    = require('./../lib/time');
var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

//var setTimeoutStop = require('./../actions/events').setTimeoutStop;

exports.exec = function(task){

    param.step++;

    console.log('');
    console.log('[task_result] ['+task.code+']');
    console.log('[task_click_ok] ['+task.click_ok+']');
    console.log('[task_click_content] ['+param.counters.count_int+']');
    console.log('[task_click_external] ['+param.counters.count_ext+']');
    console.log('[task_click_pop] ['+param.counters.count_pop+']');
    console.log('[task_click_task] ['+task.click_task+']');
    console.log('');


    var deferred = Q.defer();

    var page_task = require('webpage').create();

    var url = param.finish_url +
        '&task_id='    + encodeURIComponent(task.task_id) +
        '&code='    + encodeURIComponent(task.code) +
        '&server='  + encodeURIComponent(task.server) +
        '&b='       + encodeURIComponent(task.login) +
        '&token='   + encodeURIComponent(task.token) +
        '&balancer=' + encodeURIComponent(task.balancer) +
        '&site='    + encodeURIComponent(task.site) +
        '&trader='  + encodeURIComponent(task.trader) +
        '&task_type=' + encodeURIComponent(task.task_type) +
        '&click_task=' + encodeURIComponent(task.click_task) +
        '&click_ok=' + encodeURIComponent(task.click_ok) +
        '&delta='   + encodeURIComponent(task.delta);


    console.log('[user-agent] ['+param.ua_save+']');
    console.log('[action] ['+task.task_type+']');
    console.log('[task_delta] ['+task.delta+']');
    console.log('[task_server] ['+task.server+']');
    console.log('[task_token] ['+task.token+']');
    console.log('[task_site] ['+task.site+']');
    console.log('[task_trader] ['+task.trader+']');
    console.log('[screen] ['+param.task.sx+'x'+param.task.sy+']');

    var provider = '';
    for(var id in param.args){
        if(param.args.hasOwnProperty(id)){
            if(param.args[id][0] == 'p'){
                provider = param.args[id][1];
                console.log('[provider] ['+provider+']');
            }

            if (param.args[id][0] == 'useragent') {
                url += '&useragent=' + encodeURIComponent(param.ua_save);
            } else if (param.args[id][0] == 'task_uniq_id') {
                debug.log('[subid] ['+param.args[id][1]+']');
                url += '&subid=' + encodeURIComponent(param.args[id][1]);
            } else {
                url += '&' + param.args[id][0] + '=' + encodeURIComponent(param.args[id][1]);
            }
        }
    }


    debug.log('finish_url', url);

    var now = new Date().getTime();
    var time_start = parseInt(now, 10);

    var settings = {
        operation: "POST",
        encoding: "utf8",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify({
            task: param.task,
            provider: provider,
            url: url,
            log: param.log,
            url_log: param.url_log
        })
    };

/*    page_task.settings.userAgent = param.ua_save;
    page_task.open(url, function(status) {

        debug.log('finish url status  '+status);
        //console.log('[url_task_finish_status] ['+status+']');

        debug.log('finish url answer: '+page_task.plainText);
        //console.log('[url_task_finish_answer] ['+page_task.plainText+']');

        setTimeout(function(){
            console.log('[TASK_FINISH_FULL]');
            console.log('phantom.exit');
            phantom.exit(0);
        }, 2000);
    });*/

/*    setTimeout(function(){
        console.log('[TASK_FINISH_FULL]');
        console.log('phantom.exit');
        phantom.exit(0);
    }, 2000);*/


    setTimeout(function(){
        console.log('[TASK_FINISH_FULL]');
        console.log('phantom.exit');
        phantom.exit(0);
    }, 2000);

    return deferred.promise;

};
