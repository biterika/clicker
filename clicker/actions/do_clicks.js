var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

var make_click  = require('./make_click').exec;
//var check_pages   = require('./check_pages').exec;

exports.exec = function(obj, x, y, button){

    var deferred = Q.defer();



    var task_finished = false;
    var try_count_max = config.const.try_count_max + param.task.click_task;
    var click_task = param.task.click_task;

    var click_external_start = false;
    var click_external_this_counter = 0;

   /* if (param.task.task_type && param.task.task_type.substr(0, 7) == 'trader.') {
        try_count_max = 5;
    }*/

    var try_count = 0;

    promiseWhile(function () { return !task_finished; }, function (result) {


        var click_external = param.counters.count_ext;
        var click_content = param.counters.count_int;
        var click_all      = click_external + click_content;

        try_count++;

      //  check_pages();


            debug.log('------------------------------------------------------------------------------------------------ ');
            debug.log(' ');
            debug.log('    ',
                ' Running task try/max:  ', try_count+'/'+try_count_max,
                ' clicks_try:', param.task.click_ok+'/'+click_task,
                ' click_external/internal: '+click_external+'/'+click_content,  ' click_all '+click_all
            );
            debug.log(' ');
            debug.log('------------------------------------------------------------------------------------------------ ');

        //is it enough?

        if (param.task.task_type && param.task.task_type.substr(0, 7) == 'trader.') {


            var continue_click = false;

            // если сделали первый клик по внешеней ссылке
            if (!click_external_start && click_external > 0) {

                console.log('##### do_clicks() - FIRST_click_external #### -- go make click next');
                click_external_start = true;
                continue_click = true;
                click_external_this_counter = click_external;
            }
            else if (click_external_start) {

                // если сделали последующие клики по внешеней ссылке
                if (click_external_start &&
                    click_external > click_external_this_counter
                ) {
                    click_external_this_counter = click_external;

                    // если кликов по внешней ссылке больше или равна 3 - завершаемся
                    if (click_external > 3) {

                        console.log('##### do_clicks() -  click_external > 3-- clicks enought');
                        deferred.resolve({
                            status: 'success'
                        });
                        task_finished = true;
                        return;

                    }
                    else {
                        // продолжаем
                        console.log('##### do_clicks() -  continue clicks (before have ext click)');
                        continue_click = true;
                    }

                }
                else { // если последующие клики не по внешней ссылке - завершаемся
                    console.log('##### do_clicks() -  stop clicks (before have int click)');
                    deferred.resolve({
                        status: 'success'
                    });
                    task_finished = true;
                    return;
                }

            }

            //перестаем кликать если лимит  достигнут
            if(
                (!continue_click && (try_count >= try_count_max || param.task.click_ok >= 4 ))
                || click_external >= 3
            ){

                console.log('##### do_clicks() -  -- clicks enought');

                deferred.resolve({
                    status: 'success'
                });
                task_finished = true;
                return;
            }

        }
        else {

            if (param.task.click_ok >= click_task || param.task.click_external >= click_task) {

                console.log('##### clicks enough');

                deferred.resolve({
                    status: 'success'
                });
                task_finished = true;
                return;

            }

            //reached the limit?
            if(try_count > try_count_max){
                deferred.resolve({
                    status: 'error'
                });
                task_finished = true;
                return;
            }
        }



        return make_click(obj);

    }).then(function () {

        deferred.resolve({
            status: 'error'
        });

    }).done();

    return deferred.promise;

};

//while loop with promises
function promiseWhile(condition, body) {
    var done = Q.defer();
    function loop() {
        if (!condition()) return done.resolve();
        Q.when(body(), loop, done.reject);
    }
    Q.nextTick(loop);
    return done.promise;
}
