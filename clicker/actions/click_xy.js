var config = require('./../inc/config');
var param = require('./../inc/param');
var debug = require('./../lib/debug');
var random = require('./../lib/random');
var Q = require('./../modules/q/q.js');
var colors = require('./../modules/colors/safe.js');
var make_screen = require('./make_screen').exec;

var script_name = 'click_xy';

var rand1 = random.int(500,1500);
var rand2 = random.int(500,1500);
var rand3 = random.int(500,1500);
var rand4 = random.int(500,1500);
var rand5 = random.int(500,1500);

var y;

exports.exec = function (obj, x, y_in, button) {

    y = y_in;

    var deferred = Q.defer();

    debug.log(script_name,' screen '+param.task.sx+'x'+param.task.sy+ ' requested coords x='+x+' y='+y);

    obj.click_now = true;

    if (!param.task.sx || !param.task.sy /*|| x >= param.task.sx || y >= param.task.sy*/) {
        debug.log(script_name, '[ERROR] [CANCEL_CLICK] becouse no coordinate');
        deferred.resolve({ status: 'success' });
    }
    else {

        Q.fcall(function () {
            debug.log(script_name, ' ');
            debug.log(script_name, ' ----> start');
            debug.log(script_name, ' ');

            debug.log(script_name, 'coordinate : ', x,'x', y, 'button:', button, 'in page:', obj.id, obj.url);

            debug.log(script_name, '  start make_random_mouseMove()');

            if (param.task.mouse_move) {
                return make_many_random_mouseMove(obj, random.int(1, 2));
            } else {
                debug.log('dont make make_many_random_mouseMove');
                return true;
            }





        }).delay(rand2).then(function (res) {


            debug.log(script_name, '+++++++++++++++ Wont CLICK ' + x + ' x ' + y + ' ++++++++++++++++++');

            //make_screen(obj);

            if (y >= param.task.sy) {

                debug.log(script_name, ' need correct scroll position  y = '+y);

                return make_scroll(obj,y);

            } else {
                console.log(script_name, ' no need correct scroll position ');
                return true;
            }


        }).delay(rand1).then(function (res) {

            debug.log(script_name, '  start make_mouseMove() to target '+x+' x '+y);

            return make_mouseMove(obj, x, y);

        }).delay(rand2).then(function (res) {


            if (param.task.task_type && param.task.task_type.substr(0, 7) == 'trader.') {

                if (param.counters.count_ext > 3) {
                    debug.log(script_name, '##### click_x_y() - stop click trader - clicks enought #### param.counters.count_ext= ',param.counters.count_ext);
                    var result = {
                        status: 'success'
                    };
                    deferred.resolve(result);
                }
            }

            //make_screen(obj);

            debug.log(script_name, '+++++++++++++++ CLICK ' + x + ' x ' + y + ' ++++++++++++++++++');

            debug.log(script_name, '   1  mousedown', x + ' x ' + y + ' button= ' + button);

            if (!param.close_popup) {
                param.task.click_ok++;
                debug.log(script_name, colors.bgWhite.blue('param.task.click_ok++ click_ok = ' + param.task.click_ok));
            }

            obj.sendEvent('mousedown', x, y, button);

        }).delay(rand3).then(function () {


            //debug.log(script_name, '      click finished' + res);

            debug.log(script_name, '   2  mouseup', x + ' x ' + y + ' button= ' + button);

            obj.sendEvent('mouseup', x, y, button); // todo тут он уже открывает страницу

        }).then(function () {

            //debug.log(script_name, '      mouseup finished');
            debug.log(script_name, '   3  click', x + ' x ' + y + ' button= ' + button);
            debug.log(script_name, 'wait');

            //obj.sendEvent('click', x, y, button);

        }).then(function () {

            debug.log(script_name, '      mouseup finished');
            debug.log(script_name, '+++++++++++++++ /CLICK +++++++++++++++++++++++++++++');

            debug.log(script_name, 'wait');

        }).catch(function (e) {

            obj.click_now = false;
            debug.log(script_name,'catch: ', JSON.stringify(e));
            deferred.reject(e);


        }).delay(7000).done(function () {
            debug.log(script_name, '');
            debug.log(script_name, '<---- done');
            debug.log(script_name, '');

            obj.click_now = false;

            var result = {
                status: 'success'
            };
            deferred.resolve(result);
        });



    }


    return deferred.promise;
};



function make_many_random_mouseMove(obj,amount_of_move) {

    var deferred3 = Q.defer();

    if (!amount_of_move || amount_of_move < 1) { amount_of_move = 1;}


    debug.log(script_name, '  make_many_random_mouseMove() start amount_of_move ='+amount_of_move);

    var random_wait = random.int(10,3000);
    var index = 0; var rnd_wait_shortOrLong;

    promiseWhile(function () {
        return index < amount_of_move;
    }, function () {

        index++;

        debug.log(script_name,'obj.url',obj.url);
        var amount_move_obj = {move_num: index, of_move: amount_of_move};
        debug.log(script_name, '  make_many_random_mouseMove() make number '+index+' of '+amount_of_move);


        // long or short wait
        rnd_wait_shortOrLong = random.int(1, 4);
        if (rnd_wait_shortOrLong == 1) {
            random_wait = random.int(1200, 8000); // в 25% случаях
        } else {
            random_wait = random.int(10, 1200); // в 75% случаях
        }

        debug.log(script_name, '  make_many_random_mouseMove() random_wait= '+random_wait);
        return make_random_mouseMove(obj,amount_move_obj,random_wait);



        //return Q.delay(1000); // arbitrary async
    }).done(function () {
        deferred3.resolve(1);
    });
    return deferred3.promise;


}


function make_random_mouseMove(obj, amount_move_obj, wait) {

    //var deferred2 = Q.defer();
    if (!amount_move_obj) amount_move_obj = {};
    if (!wait) wait  = 10;

    var max_x, max_y;
    if (param.task.sx) {
        max_x = Math.ceil(param.task.sx * 0.9);
    } else {
        max_x = 700;
    }
    if (param.task.sy) {
        max_y = Math.ceil(param.task.sy * 0.8);
    } else {
        max_y = 500;
    }

    var x = random.int(5, max_x);
    var y = random.int(5, max_y);

    debug.log(script_name, 'generated random target mouse coordinate '+x+' x '+y);
    debug.log(script_name, 'current mouse position coordinate '+param.mouse.x+' x '+param.mouse.y);
    return make_mouseMove(obj, x, y, amount_move_obj,wait);
    //return make_mouseMove(obj, x, y);
    //deferred2.resolve(res);

    //return deferred2.promise;
}

function make_scroll(obj,y_in,amount_move_obj,wait) {

    debug.log(script_name, ' make_scroll start ');

    if (!amount_move_obj) amount_move_obj = {};
    if (!wait) wait  = 10;


    var deferred2 = Q.defer();

    debug.log(script_name, ' make_scroll start [obj,y_in,amount_move_obj,wait]',obj,y_in,amount_move_obj,wait);

    var target_scroll_y = y_in -  Math.round(param.task.sy / 3);

    var scroll_before = obj.evaluate(function () {
        return document.body.scrollTop;
    });

    if (!scroll_before) {
        debug.log(script_name, '  ');
    }

    debug.log(script_name, 'make_scroll(),  scrollPosition.top: ',scroll_before, ' target_scroll_y:  =',target_scroll_y, ' x ',param.mouse.x);

    var cord = mouseMove(param.mouse.x, scroll_before, param.mouse.x, target_scroll_y, 100);

    debug.log(script_name, ' make_scroll(),  cord have ' + JSON.stringify(cord));

    var index = 0;
    promiseWhile(function () {
        return index < cord.length;
    }, function () {

        obj.scrollPosition = {top: cord[index].y, left: 0};
        index++;
        return Q.delay(10); // arbitrary async

    }).delay(+wait).done(function () {


        var now_scrollTop = obj.evaluate(function () {
            return document.body.scrollTop;
        });

        obj.scrollPosition.top = now_scrollTop;

        var scroll_delta = scroll_before - now_scrollTop;

        debug.log(script_name, ' make_scroll(),  scroll_delta = ',scroll_delta,' scroll_before ', scroll_before, ' now ', now_scrollTop);

        //make_screen(obj);

        debug.log(script_name, ' make_scroll(),  start correct y-target with scroll delta ');
        debug.log(script_name, ' make_scroll(),  correct y-target with scroll delta  y (before) = '+ y);
        y = y + scroll_delta;
        debug.log(script_name, ' make_scroll(),  correct y-target with scroll delta  y (corrected) = ', y);

        deferred2.resolve(1);
    });
    return deferred2.promise;
}

function make_mouseMove(obj, x, y,amount_move_obj,wait) {

    debug.log(script_name, 'make_mouseMove() ', x||'n/a', 'x', y||'n/a');

    if (!amount_move_obj) amount_move_obj = {};
    if (!wait) wait  = 10;


    var deferred2 = Q.defer();

    var cord = mouseMove(param.mouse.x, param.mouse.y, x, y, 100);

    //debug.log(script_name, '  cord have ' + JSON.stringify(cord));
    var index = 0;
    promiseWhile(function () {
        return index < cord.length;
    }, function () {

        obj.sendEvent('mousemove',cord[index].x, cord[index].y);
        param.mouse.x = cord[index].x;
        param.mouse.y = cord[index].y;

        index++;

        return Q.delay(10); // arbitrary async

    }).delay(+wait).done(function () {
       deferred2.resolve(1);
    });
    return deferred2.promise;
}

function mouseMove(start_x, start_y, end_x, end_y, steps) {

    //var deferred2 = Q.defer();

    var bezier = function (t, p0, p1, p2, p3) {
        var cX = 3 * (p1.x - p0.x),
            bX = 3 * (p2.x - p1.x) - cX,
            aX = p3.x - p0.x - cX - bX;

        var cY = 3 * (p1.y - p0.y),
            bY = 3 * (p2.y - p1.y) - cY,
            aY = p3.y - p0.y - cY - bY;

        var x = (aX * Math.pow(t, 3)) + (bX * Math.pow(t, 2)) + (cX * t) + p0.x;
        var y = (aY * Math.pow(t, 3)) + (bY * Math.pow(t, 2)) + (cY * t) + p0.y;

        return {x:  Math.round(x), y:  Math.round(y)};
    };

    var tupost = Math.random();
    var speed = Math.random() * steps;
    var rand_x1 = start_x + Math.random() * (end_x - start_x) + (Math.random() + 20);
    var rand_y1 = start_y + Math.random() * (end_y - start_y) + (Math.random() + 20);
    var rand_x2 = end_x;
    var rand_y2 = end_y;
    if (tupost < 0.05) {
        rand_x1 = Math.random() * start_x;
        rand_x2 = Math.random() * start_y;
        rand_y1 = Math.random() * end_y;
        rand_y2 = Math.random() * end_y;
    }
    var p0 = {x: start_x, y: start_y},
        p1 = {x: rand_x1, y: rand_y1},
        p2 = {x: rand_x2, y: rand_y2},
        p3 = {x: end_x, y: end_y};
    var coordinates = [];
    var step = 1 / speed;

    for (var i = 0; i < 1; i += step) {
        coordinates.push(bezier(i, p0, p1, p2, p3));
    }
    return coordinates;
    //deferred2.resolve(coordinates);

    //return deferred2.promise;
}


// `condition` is a function that returns a boolean
// `body` is a function that returns a promise
// returns a promise for the completion of the loop
function promiseWhile(condition, body) {
    var done = Q.defer();

    function loop() {
        // When the result of calling `condition` is no longer true, we are
        // done.
        if (!condition()) return done.resolve();
        // Use `when`, in case `body` does not return a promise.
        // When it completes loop again otherwise, if it fails, reject the
        // done promise
        Q.when(body(), loop, done.reject);
    }

    // Start running the loop in the next tick so that this function is
    // completely async. It would be unexpected if `body` was called
    // synchronously the first time.
    Q.nextTick(loop);

    // The promise
    return done.promise;
}
