var config  = require('./../inc/config');
var param   = require('./../inc/param');
var time    = require('./../lib/time');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');
var fs      = require('fs');

var setTimeoutStop = require('./events').setTimeoutStop;

exports.exec = function(obj){

    //param.step++;

    debug.log('');
    debug.log('Make screenshot:', obj.url);

    var deferred = Q.defer();

    if(config.const.make_screen){

        obj.evaluate(function() {
            document.body.bgColor = 'black';
        });

        //obj.zoomFactor = 0.10;
        //var screen = obj.renderBase64('JPEG');
        //obj.zoomFactor = 1;

        setTimeout(function(){

            debug.log('render /usr/share/biterika/clicker_phantom/screen/'+Date.now()+'.jpg');
            obj.render('/usr/share/biterika/clicker_phantom/screen/'+Date.now()+'.jpg', {format: 'jpeg', quality: '10'});

            var result = {
                status: 'success'
            };
            deferred.resolve(result);

        }, 1000);

    }else{

        var result = {
            status: 'success'
        };
        deferred.resolve(result);

    }

    return deferred.promise;

};
