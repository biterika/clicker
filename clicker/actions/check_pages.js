var config = require('./../inc/config');
var param  = require('./../inc/param');
var debug  = require('./../lib/debug');
var Q      = require('./../modules/q/q.js');
var colors = require('./../modules/colors/safe.js');

var script_name = 'check_pages.js';

exports.exec = function (obj) {

    var page_for_click    = obj;
    var page_for_click_id = obj.id;
    var click             = false;
    var click_content     = false;
    var close             = {};

    //find first page with the same domain - it is category
    for (var id in obj.pages) {
        if (obj.pages.hasOwnProperty(id)) {
            debug.log(script_name, '  ', 'Page opened:', obj.pages[id].id, obj.pages[id].domain, obj.pages[id].url, param.task.trader);

            //is it the same domain?
            if (obj.pages[id].domain == param.task.trader) {

                //is have id's less then we have?
                if (obj.pages[id].id < page_for_click_id || page_for_click_id == 1) {
                    if (page_for_click_id > 1) {
                        close[obj.pages[id].id]  = false;
                        close[page_for_click_id] = obj.pages[page_for_click_id];
                        debug.log(script_name, '  ', 'click = true | domain (' + obj.pages[id].domain + ') == trader (' + param.task.trader + ') | page_for_click_id > 1, id ',obj.pages[id].id, ' url ',obj.pages[id].url);
                        click         = true;
                        click_content = true;
                    }
                    page_for_click    = obj.pages[id];
                    page_for_click_id = obj.pages[id].id;
                }
                else {
                    debug.log(script_name, '  ', ' add page to close ----->> click = true | domain (' + obj.pages[id].domain + ') == trader (' + param.task.trader + ')  | else,   id ',obj.pages[id].id, ' url ',obj.pages[id].url);
                    click                   = true;
                    close[obj.pages[id].id] = obj.pages[id];
                    //obj.pages[id].close();
                }

            } else {
                if (obj.pages[id].domain != 'blank') {
                    click = true;
                    debug.log(script_name, '  ', 'click = true | domain != trader');
                }
                close[obj.pages[id].id] = true;
                debug.log(script_name,' add page to close ----->> , id ',obj.pages[id].id, ' url ',obj.pages[id].url);
                //obj.pages[id].close();
            }

        }
    }

    //close other pages
    for (var page_id in close) {
        if (close.hasOwnProperty(page_id)) {
            if (close[page_id] != false) {

                for (var id in obj.pages) {
                    if (obj.pages.hasOwnProperty(id)) {
                        if (obj.pages[id].id == page_id && !obj.pages[id].click_now) {
                            debug.log(script_name, colors.bgBlack.red(' CLOSE PAGE ----> id ',obj.pages[id].id, ' url ',obj.pages[id].url));
                            obj.pages[id].close();
                        }
                    }
                }

            }
        }
    }

    /*if(click) {
        param.task.click_ok++;
        console.log(colors.bgWhite.blue(script_name,' -> param.task.click_ok++ click_ok = ' + param.task.click_ok));
    }*/

/*
    if (click_content) {
        param.task.click_content++;
        console.log(colors.bgWhite.blue(script_name, ' -> param.task.click_content++ click_content = ' + param.task.click_content));
    }*/

    return page_for_click;

};
