var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var random  = require('./../lib/random');
var Q       = require('./../modules/q/q.js');

var click_xy   = require('./click_xy').exec;

exports.exec = function(obj){
    debug.log('');
    debug.log(colors.bold('check_popup()', ' start'));

    param.step++;

    var deferred = Q.defer();


    Q.fcall(function () {

        obj.navigationLocked = true;
        param.close_popup = true;

        return click_xy(obj, random.int(1,5), random.int(1,5), 'middle');

    }).delay(config.timeout.wait_popup).then(function (res) {

        param.close_popup = false;
        obj.navigationLocked = false;

    }).catch(function (err) {
        error('check_popup catch err ' + JSON.stringify(err));
        //process.exit(11);
    }).done(function () {
        debug.log(colors.bold('check_popup()', ' finish'));
        deferred.resolve({status: 'success'});
    });

    return deferred.promise;

};
