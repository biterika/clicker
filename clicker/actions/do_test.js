var config  = require('./../inc/config');
var param   = require('./../inc/param');
var random  = require('./../lib/random');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

var random_thumb    = require('./random_thumb').exec;
var click_xy        = require('./click_xy').exec;
var check_pages     = require('./check_pages').exec;
var make_screen     = require('./make_screen').exec;
var make_screen_rect = require('./make_screen_rect').exec;

var obj_main = false;
var obj_for_click = false;

exports.exec = function(obj){
    debug.log('');

    var deferred = Q.defer();

    if(!obj_main){
        obj_main = obj;
    }
    if(!obj_for_click){
        obj_for_click = obj_main;
    }

    //wait timeout before click
    setTimeout(function(){

        //is a thumbs have in page?
        var thumb = random_thumb(obj_for_click);
        if(!thumb.x || !thumb.y){

           /* setTimeout(function(){
                make_screen(obj_for_click);
            }, 200);*/

            obj_for_click = obj_main;

            deferred.resolve({
                status: 'error'
            });

        }else{

            param.have_thumb++;

            var x = random.int(thumb.x+5,thumb.x+thumb.width-5);
            var y = random.int(thumb.y+5,thumb.y+thumb.height-5);

            var rect = {
                top: y-50,
                left: x-50,
                width: 100,
                height: 100
            };
            make_screen_rect(obj_for_click, rect);

            //click_xy(obj_for_click, x, y, 'middle');
            click_xy(obj_for_click, x, y, 'left');

            //wait timeout before open popups
            setTimeout(function(){

                //wait when popup loading will be finished
                waitFor(function(){
                    return !param.popupLoading;
                }, function(){

                    obj_for_click = check_pages(obj_main);

                    deferred.resolve({
                        status: 'success'
                    });

                }, config.timeout.loading_popup);

            }, config.timeout.wait_popup);

        }

    }, config.timeout.before_click);

    return deferred.promise;

};

//wait for something
function waitFor(testFx, onReady, timeOut, timeFrequency) {
    var maxtimeOutMillis = timeOut ? timeOut : 3000,
        start = new Date().getTime(),
        condition = false,
        interval = setInterval(function() {
            if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx());
            } else {
                if(!condition) {
                    //console.log("'waitFor()' timeout");
                    clearInterval(interval);
                    typeof(onReady) === "string" ? eval(onReady) : onReady();
                } else {
                    //console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                    clearInterval(interval);
                    typeof(onReady) === "string" ? eval(onReady) : onReady();
                }
            }
        }, timeFrequency);
}
