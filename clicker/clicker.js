var system = require('system');
var args   = system.args;

//Libs
var time    = require('./lib/time');
var config  = require('./inc/config');
var param   = require('./inc/param');
var debug   = require('./lib/debug');
var random  = require('./lib/random');
var Q       = require('./modules/q/q');
var colors = require('./modules/colors/safe.js');
var webpage = require('webpage');
var page    = null;//webpage.create();


//======================================================================================================================

//curl -sL http://212.47.245.204/disp/clicker-v5.0-run.sh | sudo -E bash -
//DISPLAY=:0 slimerjs --proxy=10.1.20.150:789 --proxy-auth=phantom:123341234 /usr/share/biterika/clicker-headless2-v6.0/clicker.js 10.1.20.150:789 $HOSTNAME 'http://195.154.223.155/bot.cgi?default&b=phantom' 'http://195.154.223.155/finish.cgi?10&group=v7' phantom 123341234

var timer_stop = false;
var errors     = false;
var lp         = 'undefined:undefined';
var cache      = {};

var task_finish_error = 0;

if (!args[6]) {
    exit('NO_HAVE_ARGS');
}


if (typeof args[1] != 'undefined') param.task.balancer = args[1];
if (typeof args[2] != 'undefined') param.task.server = args[2];
if (typeof args[3] != 'undefined') param.task_url = args[3];
if (typeof args[4] != 'undefined') param.finish_url = args[4];
if (typeof args[5] != 'undefined') param.task.login = args[5];
if (typeof args[6] != 'undefined') param.task.token = args[6];
if (typeof args[7] != 'undefined') {
    param.navigator_json = args[7];
    try {
        param.navigator_obj = JSON.parse(param.navigator_json);
    } catch (e) {
        console.error('error parse navigator_json, error = '+e.message);
        phantom.exit(35);
    }
}

console.log(colors.magenta('balancer'), param.task.balancer, 'server', param.task.server, 'verb', param.verbose);
console.log('task_url', param.task_url);
console.log('finish_url', param.finish_url);
//console.log('make_screen',param.make_screen);
console.log(param.task.login, param.task.token);
console.log('param.navigator_obj = '+JSON.stringify(param.navigator_obj));

//======================================================================================================================

var events      = require('./actions/events').exec;
var preload     = require('./actions/preload').exec;
var site_open   = require('./actions/site_open').exec;
var task_finish = require('./actions/task_finish_post').exec;
var make_screen = require('./actions/make_screen').exec;

var task;
var first          = true;
var action_counter = 0;
var action_path;

//======================================================================================================================

phantom.onError = function (msg, trace) {
    var msgStack = ['PHANTOM ERROR: ' + msg];
    if (trace && trace.length) {
        msgStack.push('TRACE:');
        trace.forEach(function (t) {
            msgStack.push(' -> ' + (t.file || t.sourceURL) + ': ' + t.line + (t.function ? ' (in function ' + t.function + ')' : ''));
        });
    }
    debug.error(msgStack.join('\n'));
};

run_action();

function run_action() {


    Q.fcall(function () {


        return init();


    }).then(function () {


        return events(page);
        /*if( first){
            first = false;
            return events(page);
        }else{
            return true;
        }*/


    }).then(function () {


        return site_open(page);


    }).then(function (result) {

        debug.log('get_task results ', JSON.stringify(result));

        //make screen
        /*page.open('http://185.31.210.60/api/ip_headers2.php', function (status) {
            setTimeout(function(){
                console.log(page.plainText);
                make_screen(page);
                phantom.exit();
            },1000)
        });*/

        if (!result || !result.status || result.status != 'success') {

            return result.status;
            //throw new Error(result.status);

        } else if (page.content.length < 200) {

            return 'SITE_BODY_EMPTY';
            //
            //finish('');
            //throw new Error('SITE_BODY_EMPTY');

        } else {

            //action_path = './task/'+param.task.task_type;
            //task = require.resolve(action_path);

            if (!cache[param.task.task_type]) {
                cache[param.task.task_type] = require('./task/' + param.task.task_type);
            }

            task = cache[param.task.task_type].exec;

            return task(page);

        }


    }).then(function (result) {


        param.task.code = result;


    }).catch(function (e) {


        clearTimeout(timer_stop);
        timer_stop = false;

        errors = true;
        if (e.message) {
            debug.error('ERROR:', e.message, config.errors[e.message]);
            debug.error('ERROR:', e.stack);
        }

        param.task.code = e.message;


    }).done(function () {

        console.log(colors.bgWhite.magenta('#### FINISH #### param.counters = '),JSON.stringify(param.counters,'','\t'));

        finish(param.task.code);

        //debug.json(JSON.stringify(param.url_log));

        /*clearTimeout(timer_stop);
        timer_stop = false;

        delete param.task.tds;

        param.task.delta = time.time_delta();

        if(param.task.code != 'TASK_FINISH_FULL' && param.task.code != 'TASK_FINISH_PART'){
            task_finish_error++;
        }

        console.log(new Date());
        console.log(JSON.stringify(param.task));

        var pt = param.task;
        task_finish(pt);

        //restart();
        //exit(0);*/

    });

}

function init() {

    var deferred = Q.defer();

    action_counter++;

    debug.log('init:', action_counter, 'errors:', task_finish_error);
    //console.log('%BITERIKA_TASK_START%');

    page = webpage.create();
    page.open('about:blank', function (status) {
        debug.log('about:blank', status);
    });

    try {
        for (var id in page.pages) {
            if (page.pages.hasOwnProperty(id)) {
                debug.log('close', page.pages[id].id, page.pages[id].url);
                page.pages[id].close();
            }
        }
    } catch (e) {
        debug.error(e.stack);
    }

    /*try{
        if (page && !first) page.close();
    }catch (e){
        debug.error(e.stack);
    }*/

    try {
        phantom.clearCookies();
    } catch (e) {
        debug.error(e.stack);
    }

    time.reset();

    param.task.ip            = '';
    param.task.fake          = 0;
    param.task.tds           = false;
    param.task.site          = false;
    param.task.trader        = false;
    param.task.task_type     = false;
    param.task.click_task    = random.int(1, 3);
    param.task.click_ok      = 0;
    param.task.click_content = 0;
    param.task.delta         = 0;
    param.task.code          = 'UNDEFINED';
    param.token              = '';
    param.step               = 0;
    param.page_id            = 0;
    param.close_popup        = 0;
    param.popupLoading       = false;
    param.have_thumb         = 0;
    param.log_id             = '';
    param.log                = [];

    deferred.resolve(true);

    return deferred.promise;

}

function restart() {

    debug.log('restart');
    //console.log('%BITERIKA_TASK_FINISH%');

    try {
        page.stop();
        page.close();
        page = null;
        task = null;
    } catch (e) {
        debug.error(e.stack);
    }

    setTimeout(function () {
        run_action();
    }, 1000);

    /*if(task_finish_error >= config.const.task_error_limit){
        exit(0);
    }else{
        run_action();
    }*/

}

function exit(code) {
    debug.log('exit');
    if (page) page.close();
    setTimeout(function () {
        phantom.exit(code);
    }, 0);
    phantom.onError = function () {
    };
    //throw new Error('');
}

function finish(code) {

    if (code) {
        param.task.code = code;
    }

    clearTimeout(timer_stop);
    timer_stop = false;

    delete param.task.tds;

    param.task.delta = time.time_delta();

    if (param.task.code != 'TASK_FINISH_CONTENT' &&
        param.task.code != 'TASK_FINISH_FULL' &&
        param.task.code != 'TASK_FINISH_PART'
    ) {
        task_finish_error++;
    }

    var pt = param.task;
    task_finish(pt);

    /* if(param.task.code == 'NOT_FOUND_THUMB' || param.task.code == 'SITE_BODY_EMPTY'){

        debug.log();
        debug.log('BODY:', status);
        debug.log('==================================================================================');
        debug.log(page_task.content);
        debug.log('==================================================================================');
        debug.log();

        var page_task = require('webpage').create();
        page_task.open('http://185.31.210.60/api/ip_headers.php', function (status) {

            debug.log();
            debug.log('HEADERS:', status);
            debug.log('==================================================================================');
            debug.log(page_task.content);
            debug.log('==================================================================================');
            debug.log();

        });

        setTimeout(function(){
            var pt = param.task;
            task_finish(pt);
        },10000)

    }else{

        var pt = param.task;
        task_finish(pt);

    }*/

}

function make_screen_all_pages() {

    //debug.log('make_screen_all_pages');

    var obj = page;

    make_screen(obj);
    for (var id in obj.pages) {
        if (obj.pages.hasOwnProperty(id)) {
            make_screen(obj.pages[id]);
        }
    }

}