var system = require('system');

parseUrl.options = {
    strictMode: false,
    key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
    q:   {
        name:   "queryKey",
        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    }
};

function parseUrl (str) {
    var	o   = parseUrl.options,
        m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i   = 14;

    while (i--) uri[o.key[i]] = m[i] || "";

    uri[o.q.name] = {};
    uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
        if ($1) uri[o.q.name][$1] = $2;
    });

    return uri;
}

function getDomain(url){
    var host = parseUrl(url).host;
    host = host.toLowerCase();
    if(host.substr(0, 4) == 'www.') host = host.substr(4,host.length-4);
    return host;
}

function getRootDomain(url) {
    var domain;
    if (url.indexOf("://") > -1) domain = url.split('/')[2];
    else domain = url.split('/')[0];
    domain = domain.split(':')[0];
    var arr = domain.split('.');
    return arr[arr.length-2]+'.'+arr[arr.length-1];
}

exports.getDomain = getDomain;
exports.getRootDomain = getRootDomain;
