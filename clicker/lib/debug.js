var config  = require('./../inc/config');
var param   = require('./../inc/param');
var random  = require('./random');
var time_start = parseInt(new Date().getTime()/1000);

var time = function(){
    var d = new Date();
    var h = d.getHours();if (h <= 9) h = "0" + h;
    var m = d.getMinutes();if (m <= 9) m = "0" + m;
    var s = d.getSeconds();if (s <= 9) s = "0" + s;
    return h+':'+m+':'+s;
};
var time_delta = function(){
    var time_now = parseInt(new Date().getTime()/1000);
    return time_now - time_start;
};
var time_int = function(){
    return parseInt(new Date().getTime()/1000)
};

param.log_id = random.char(32);

function log(){
    var text = '';
    for (i = 0; i < arguments.length; i++) {
        text += (i > 0 ? ' ' + arguments[i] : arguments[i]);
    }
    if(param.verbose) console.log(time()+" "+time_delta()+" "+'Step:'+" "+param.step+" "+text);
    param.log.push(time()+" "+time_delta()+" "+'Step:'+param.step+'  '+text);
}

function error(){
    var text = '';
    for (i = 0; i < arguments.length; i++) {
        text += (i > 0 ? ' ' + arguments[i] : arguments[i]);
    }
    if(param.verbose) console.error(time()+" "+time_delta()+" "+'Step:'+" "+param.step+" "+text);
    param.log.push(time()+" "+time_delta()+" "+'Step:'+param.step+'  '+text);
}

function json(json_text,level){
    //console.log(json_text);
    if (typeof level === 'undefined') {
        level = 0;
    }
    try {
        var text = JSON.parse(json_text);
        for (var prop in text) {
            if(param.verbose) console.log(time()+" "+" "+" "+time_delta()+" "+ "    "+" "+"     "+" "+repeat_str(" ",level)+" "+prop+': '+text[prop]);
            param.log.push(time()+" "+time_delta()+"    "+"     "+repeat_str(" ",level)+prop+': '+text[prop]);
            if (typeof text[prop] === 'object') {
                json(JSON.stringify(text[prop]),level+1);
            }else{
            }
        }
    } catch(err) {
        log('ERROR:');
        log(err.message);
        log(json_text);
    }
}

function repeat_str(str,num){
    for(var i=0;i<num;i++){
        str += str;
    }
    return str;
}

exports.log = log;
exports.error = error;
exports.json = json;
