<?php

return [
    'TASK_GET_URL' => 'http://get.task',
     'TASK_FINISH_URL' => 'http://c.cleanburg.com/postback?',
    // 'TASK_FINISH_URL' => 'http://task-2.biterika.com/finish.php?',
    //    'TASK_FINISH_URL' => [
    //        0=> 'http://task-1.biterika.com/finish.php?',
    //        1=> 'http://task-2.biterika.com/finish.php?',
    //    ],

    'TASK_FINISH_URL_PROXY' => 'http://finish.task',

    'ALERT_SERVICE_URL' => 'http://frontend.biterika-admin.master.cleanburg.com:888/events/logger/add?token=123',

    'PROXY_USER' => 'phantom',
    'PROXY'=> '163.172.160.155:789',
    // 'PROXY'=> '212.47.248.13:789',
    'WORK_DIR' => '/usr/share/biterika/clicker-headless2/',
    'LOCK_FILE' => '/usr/share/biterika/clicker-headless2/lock',
];