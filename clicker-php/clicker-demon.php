<?php

// php clicker-demon.php 127.0.0.1:8080 'http://greatagain.fixpix.gq' 'trader.usb'

/*
 *          ver.1.0.0
 *
 *              php /usr/share/biterika/clicker-headless2/clicker-php/clicker-demon.php
 *
 *              nohup php /usr/share/biterika/clicker-headless2/run_demon.sh 1 >/dev/null 2>&1 &
 *
 *
 *
 * */

// php clicker-demon.php 192.168.99.150:888 http://tds.tds.master.cleanburg.com/click-trader http://tds.tds.master.cleanburg.com/postback? debug

// php clicker-demon.php 127.0.0.1:8080 'http://ftt2.biterika.com' 'trader.usb'

$config = require_once 'config.php';

$TASK_GET_URL = $config['TASK_GET_URL'];
//$TASK_GET_URL = 'http://get_sandbox.task';

//$TASK_FINISH_URL = $config['TASK_FINISH_URL'];
//'http://c.cleanburg.com/postback?'; // 'http://tds.tds.master.cleanburg.com/postback?';
//$TASK_FINISH_URL = 'http://51.15.53.214:5555/postback?';
$TASK_FINISH_URL_BALANCER =  $config['TASK_FINISH_URL_PROXY']; //'http://finish.task';


$ALERT_SERVICE_URL = $config['ALERT_SERVICE_URL'];

// touch /usr/share/biterika/clicker-headless2/lock
// rm /usr/share/biterika/clicker-headless2/lock
$lock_file = $config['LOCK_FILE']; // /usr/share/biterika/clicker-headless2/lock';


$USER = $config['PROXY_USER']; //'phantom'; // phantom biterika_test
$BALANCER = $config['PROXY']; //163.172.160.155:789';
$NODE = 'srv_'.gethostname(); // $data['task_server'] = $NODE;
$DIR = $config['WORK_DIR']; //'/usr/share/biterika/clicker-headless2/';
//$PHANTOM_path = '/usr/share/biterika/clicker-headless2/dist/sw/phantomjs.2.1.1';

if ($NODE == 'srv_slimmer-parser-2' || $NODE == 'srv_clicker-for-new-sites-2') {
    $PHANTOM_path = $DIR.'/dist/u14_iv2/phantomjs';
} else {
    //$PHANTOM_path = $DIR.'dist/u16_iv3/phantomjs';
    $PHANTOM_path = $DIR.'/dist/25c/phantomjs';
}

//$PHANTOM_path = '/usr/share/biterika/clicker-headless2/dist/u16_iv2/phantomjs';
$CLICKER_path = $DIR.'clicker/clicker.js';
$CLICKER_timeout_idle = 50; // seconds

//$debug_mode_site='http://ftt2.biterika.com';

if (isset($argv[1]) and is_numeric($argv[1])) {
    $demon_id = $argv[1];
} else {
    $demon_id = rand(100, 999);
}

$DEBUG = false;
$debug_mode_site = false;
$debug_mode_action = false;
$debug_mode_referer = false;

// php clicker-demon.php 192.168.99.150:888 http://tds.tds.master.cleanburg.com/click-trader http://tds.tds.master.cleanburg.com/postback? debug
if (isset($argv[1]) and is_string($argv[1]) and !is_numeric($argv[1])) {

    echo '$argv = '.print_r($argv,true).PHP_EOL;

    $BALANCER = $argv[1];
    echo '$BALANCER = '.$BALANCER.PHP_EOL;

    if (isset($argv[2])) {
        $debug_mode_site = $argv[2];
        echo '$debug_mode_site = '.$debug_mode_site.PHP_EOL;
    }

    if (isset($argv[3])) {
        $debug_mode_action = $argv[3];
        echo '$debug_mode_action = '.$debug_mode_action.PHP_EOL;
    }

    if (isset($argv[4])) {
        $debug_mode_referer = $argv[4];
        echo '$debug_mode_referer = '.$debug_mode_referer.PHP_EOL;
    }


    if (isset($argv[1]) && isset($argv[2]) && isset($argv[3])) {
        $DEBUG = true;
        echo '$DEBUG = true'.PHP_EOL;
    }
}

$last_log = '';
$crash_log_arr = [];

$token_finish = [];

$time_zone_random_list = [ // using if time_zone is unknown
    'America/Mexico_City', 'America/New_York', 'America/Los_Angeles', 'America/Chicago',
    'Europe/Paris', 'Europe/Amsterdam', 'Europe/Berlin'
];

if (!defined('SCRIPT_DIR')) {
    define('SCRIPT_DIR', __DIR__);
}

define('CRASH_LOG_DIR', SCRIPT_DIR.'/crash_log');

if (!is_dir(CRASH_LOG_DIR)) {
    mkdir(CRASH_LOG_DIR, 0777, true);
    @chmod(CRASH_LOG_DIR, 0777);
}

$logFile = SCRIPT_DIR . '/result_' . date('Y-m-d') . '.log';

$fields = [
    'TASK_DATA', 'task_url', '_urls_log', '_urls_log_pop','task_result',
    'task_click_ok', 'task_click_task', 'task_click_content', 'task_click_external','task_click_pop',
    'provider', 'user-agent', 'action', 'task_delta',
    'task_server', 'task_token', 'task_site', 'task_trader', 'screen', 'subid',
    'url_task_finish_status', 'url_task_finish_answer'
];

// ================================================================================================================

error_reporting(E_ERROR | E_WARNING);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

echo 'start...' . PHP_EOL;




$time_start_total = microtime(true);
$process = null;
$pipes = null;
$task_results = [];
$loop_count = 0;
$subid = '';
$task_get_params = [];
$cmd_log = '';

while (1) {

    if(file_exists($lock_file)){
        echo PHP_EOL.'have a lock file - exit now'.PHP_EOL;
        exit;
    }

    $subid = '';
    $last_log = '';
    $crash_log_arr = [];
    $last_log = '';
    sleep(rand(2, 5));

    $time_start_loop = microtime(true);
    $loop_count++;

    if ($DEBUG && $loop_count > 1) die('debug mode, exit.'.PHP_EOL);

    $task_results = []; $url_history=[]; $url_history_pop=[]; $task_get_params = [];

    $token = substr(str_replace('-', '', UUID::v4() . UUID::v4()), 11, 40);

    echo PHP_EOL . PHP_EOL . '==========demon id ' . $demon_id . '====demon work number ==================================================='  . PHP_EOL . PHP_EOL.'             $token = '.$token.PHP_EOL.PHP_EOL;


    echo '************  START GET TASK ************ '.PHP_EOL;
    $res  = get_task($BALANCER, $TASK_GET_URL, $USER, $token);

    sleep(3);

    if (!$res)  {
        echo '------  no_redirect_task (TASK_GET_TIMEOUT) res---- $redirect_url empty=> '.PHP_EOL.PHP_EOL;
        print_r($res);
        postBack(['task_result'=>'TASK_GET_TIMEOUT','task_get_params'=>$res['task_get_params']], $token);
        continue;
    };
    print_r($res);
    $redirect_url = $res['redirect_url'];
    $task_get_params = $res['task_get_params'];
    $navigator_json = $res['navigator_json'];



    $task_url_parms=[];
    if (isset($redirect_url) && !empty($redirect_url)) {
        // http://no.task/?task_uniq_id=S114ac464c76d4942d89b&ip=173.214.148.12&country=US&tz=America/New_York
        $query_redirect_url = parse_url($redirect_url)['query'];
        $task_url_parms = proper_parse_str($query_redirect_url);
    }


    echo PHP_EOL.'$task_url_parms = '.PHP_EOL;
    print_r($task_url_parms);

    if (strpos($redirect_url,'http://no.task/') !== false) {
        echo PHP_EOL.'------  NO_TASK ----   $redirect_url => '.$redirect_url.PHP_EOL.PHP_EOL;
        //finish_task($token, 'NO_TASK');
        postBack(['task_result'=>'NO_TASK', 'task_url_parms'=>$task_url_parms, 'task_get_params'=>$res['task_get_params']], $token);
        continue;
    }
    if (strpos($redirect_url,'http://no.proxy/') !== false) {
        echo PHP_EOL.'------  NO_PROXY ----  $redirect_url => '.$redirect_url.PHP_EOL.PHP_EOL;
        //finish_task($token, 'NO_PROXY');
        postBack(['task_result'=>'NO_PROXY', 'task_url_parms'=>$task_url_parms, 'task_get_params'=>$res['task_get_params']], $token);
        continue;
    }
    if (strpos($redirect_url,'http://bad.proxy/') !== false) {
        echo PHP_EOL.'------  BAD_PROXY ---- $redirect_url => '.$redirect_url.PHP_EOL.PHP_EOL;
        //finish_task($token, 'BAD_PROXY');
        postBack(['task_result'=>'CANT_GET_TASK', 'task_url_parms'=>$task_url_parms, 'task_get_params'=>$res['task_get_params']], $token);
        continue;
    }
    if (empty($navigator_json) || !isJson($navigator_json)) {
        echo PHP_EOL.'------  BAD_NAVIGATOR (ERROR_GET_TASK) ----   $navigator_json => '.$navigator_json.PHP_EOL.PHP_EOL;
        //finish_task($token, 'NO_TASK');
        postBack(['task_result'=>'ERROR_GET_TASK', 'task_url_parms'=>$task_url_parms, 'task_get_params'=>$res['task_get_params']], $token);
        continue;
    }

    echo '======== =ACTION>>>  ----   '.$task_get_params['action'].'   ----------'.PHP_EOL;

    $last_log += 'navigator_id: '.$task_get_params['navigator_id'].PHP_EOL;
    $last_log += 'navigator size: '.strlen($navigator_json).PHP_EOL;
    $last_log += 'task: '.print_r($task_get_params,true).PHP_EOL.PHP_EOL;

    echo PHP_EOL.'$navigator_json = '.strlen($navigator_json).PHP_EOL.PHP_EOL;


    if (!$redirect_url || strpos($redirect_url, 'redirect_task.html') === false) {
        echo '------  no redirect_task.html ---- $redirect_url empty=> '.PHP_EOL.PHP_EOL;
        finish_task($token, 'no_redirect_task');
        continue;
    }
    $redirect_url = escapeshellarg($redirect_url);
    $task_get_params['token'] = $token;
    $subid = $task_get_params['task_uniq_id']; // subid

    if (!empty($task_get_params['tz']) && $task_get_params['tz'] !== 'unknown') {
        $TIME_ZONE = $task_get_params['tz'];
    } else {
        $TIME_ZONE = $time_zone_random_list[rand(0,count($time_zone_random_list)-1)];
    }
    $TIME_ZONE = escapeshellarg($TIME_ZONE);
    $navigator_json = escapeshellarg($navigator_json);

    //if (isset($no_proxy) and $no_proxy == true) {
        $cmd = "cd ${DIR} && TZ=${TIME_ZONE} ${PHANTOM_path} --ignore-ssl-errors=true --web-security=false --proxy=${BALANCER} --proxy-auth=${USER}:${token} ${CLICKER_path} ${BALANCER} ${NODE} ${redirect_url} http://finish.task? ${USER} ${token} ${navigator_json}";
    //} else {
    //    $cmd = "cd ${DIR} && ${PHANTOM_path} ${CLICKER_path} ${BALANCER} ${NODE} ${TASK_GET_URL} http://finish.task? ${USER} ${auth}";
    //}

    // for log
    $navigator_json2 = json_encode(json_decode($res['navigator_json'],true));
    $navigator_json2 = escapeshellarg($navigator_json2);
    $cmd_log = "cd ${DIR} && TZ=${TIME_ZONE} ${PHANTOM_path} --ignore-ssl-errors=true --proxy=${BALANCER} --proxy-auth=${USER}:${token} ${CLICKER_path} ${BALANCER} ${NODE} ${redirect_url} http://finish.task? ${USER} ${token} ${navigator_json2}";

    $last_log .= '>>>>>>>>> starting cmd: ' . PHP_EOL . $cmd . PHP_EOL . PHP_EOL;

    echo '>>>>>>>>> starting cmd: ' . PHP_EOL . $cmd . PHP_EOL . PHP_EOL;
    $err = false;

    $descriptorspec = [
        0 => ["pipe", "r"],   // stdin is a pipe that the child will read from
        1 => ["pipe", "w"],   // stdout is a pipe that the child will write to
        2 => ["pipe", "w"]    // stderr is a pipe that the child will write to
    ];
    flush();
    $process = proc_open($cmd, $descriptorspec, $pipes, realpath('./'), []);

    if (is_resource($process)) {

        // fgetsPending( $in,$tv_sec ) - Get a pending line of data from stream $in, waiting a maximum of $tv_sec seconds
        // fgets($pipes[1])
        // fgetsPending( $pipes[1],60 )

        while ($s = fgetsPending($pipes[1], $CLICKER_timeout_idle,$token)) {

            print $s;
            $last_log .= $s;
            array_push($crash_log_arr,$s);

            foreach ($fields as $field) {

                if (preg_match_all('/^' . preg_quote('[' . $field . '] [') . '(.*)' . preg_quote(']') . '/', $s, $res)) {
                    $task_results[$field] = trim($res[1][0]);
                    echo ' in console find [' . $field . ']: ' . trim($res[1][0]) . PHP_EOL;
                }

            }

            if (isset($task_results['_urls_log']) && !empty($task_results['_urls_log'])) {
                array_push($url_history,$task_results['_urls_log']);
                unset($task_results['_urls_log']);
            }
            if (isset($task_results['_urls_log_pop']) && !empty($task_results['_urls_log_pop'])) {
                array_push($url_history_pop,$task_results['_urls_log_pop']);
                unset($task_results['_urls_log_pop']);
            }

            flush();
        }
    }


    echo '[FINISHED]' . PHP_EOL;




    echo 'sending postback...' . PHP_EOL . PHP_EOL;
    
    $answer = postBack($task_results, $token);
    echo 'postBack http_answer: "' . $answer . '"' . PHP_EOL . PHP_EOL;

    sleep(rand(2, 5));

}



function postBack($data, $token)
{
    global $cmd_log, $crash_log_arr, $last_log, $NODE, $task_get_params, $subid, $token_finish, $task_results, $TASK_FINISH_URL,$url_history,$url_history_pop,$time_consume_total_str,$loop_count,$time_consume_loop_str,$loop_speed, $time_start_total, $time_start_loop;

    if ($data['task_result']=='NO_TASK') {
        echo PHP_EOL.'====== NO_TASK ======'.PHP_EOL.PHP_EOL;
        return;
    }

    if ($data['task_result']=='NO_PROXY') {
        echo PHP_EOL.'====== NO_PROXY ======'.PHP_EOL.PHP_EOL;
        return;
    }

    if ($data['task_result']=='CANT_GET_TASK') {
        echo PHP_EOL.'====== CANT_GET_TASK ======'.PHP_EOL.PHP_EOL;
        return;
    }


    $time_consume_loop = microtime(true) - $time_start_loop;
    $time_consume_loop_sec = round($time_consume_loop);
    $time_consume_loop_str = secsToTime($time_consume_loop_sec);

    $time_consume_total = microtime(true) - $time_start_total;
    $time_consume_total_str = secsToTime($time_consume_total);

    $loop_speed = round($loop_count / ($time_consume_total / 60 / 60), 4);

    $data['runtime'] = $time_consume_loop_str;
    $data['runtime_sec'] = $time_consume_loop_sec;
    $data['loop_speed'] = $loop_speed;

    if (isset($token_finish[$token])) return false;
    $token_finish[$token] = true;


    print_r($data);



    echo '$url_history: ';
    print_r($url_history);


    //echo '    $url_history_pop: ';
    //print_r($url_history_pop);


    if (isset ($data['task_url_parms'])) {
        $task_url_parms = $data['task_url_parms'];
    }

//    if (isset($url_history[0]) && !empty($url_history[0])) {
//
//        $query = parse_url($url_history[0])['query'];
//
//        if (isset($query) && !empty($query)) {
//
//            $task_url_parms = proper_parse_str($query);
//
//           /* if (isset($task_url_parms['task_uniq_id']) ) {
//
//            }*/
//        }
//    }

    if (isset($data['task_result']) && !empty($data['task_result'])) {
        $get_params['task_result'] = $data['task_result'];
    }
    else {

        $last_log .= date('Y-m-d_H:i:s').'-+-+-+-+-+-+-DIE_CRASH+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+'.PHP_EOL;
        $get_params['task_result'] = 'DIE_CRASH';
    }
    unset($data['task_result']);


    $last_log .= '$data: '.print_r($data,true).PHP_EOL;
    $last_log .= '$url_history: '.print_r($url_history,true).PHP_EOL;

    if (isset($data['TASK_DATA']['task_uniq_id']) && !empty($data['TASK_DATA']['task_uniq_id'])) {

        $get_params['subid'] = $data['TASK_DATA']['task_uniq_id'];

    }
    elseif (isset($task_url_parms) && isset($task_url_parms['task_uniq_id']) && !empty($task_url_parms['task_uniq_id'])) {

        $get_params['subid'] = $task_url_parms['task_uniq_id'];
    }

    if (!empty($subid)) $get_params['subid'] = $subid;

    $data['history'] = $url_history;
    $data['history_pop'] = $url_history_pop;
    $data['navigator_id'] = $task_get_params['navigator_id'];
    $get_params['task_data'] = $data['TASK_DATA'];
    unset($data['TASK_DATA']);
    $data['task_server'] = $NODE;
    $data['token'] = $token;

    if (!isset($get_params['subid']) || empty($get_params['subid'])) {
        $get_params['subid'] = $data['token'];
    }

    $get_params['task_report'] = json_encode($data);
    $get_params['token'] = $data['token'];
    if (!isset($data['action'])) $data['action'] = 'unknown';
    $get_params['action'] = $data['action'];
    /*$comma='';
    foreach($data as $key=>$value) {
        $get_params['task_report'] .= $comma.$key.':'.$value;
        $comma='|';
    }*/


    echo '$get_params :'.PHP_EOL;
    print_r($get_params);

    //$task_shard = $task_get_params['task_shard'];
    //if(empty($task_shard) || !is_numeric($task_shard)) $task_shard = 0;
    //$TASK_FINISH_URL_START = $TASK_FINISH_URL[$task_shard];
    //$url_get = $TASK_FINISH_URL_START . http_build_query($get_params, '', '&', PHP_QUERY_RFC1738);

    //if (isset($task_get_params['task_finish_url'])) {
        $url_get = $task_get_params['task_finish_url'] .'&'. http_build_query($get_params, '', '&', PHP_QUERY_RFC1738);
    //} else {
    //    $url_get =  $config['TASK_FINISH_URL'] . http_build_query($get_params, '', '&', PHP_QUERY_RFC1738);
    //}

    echo '***** call TASK_FINISH_URL  = ' . $url_get . PHP_EOL;

    $crash_short_report = 'n/a';

    if($get_params['task_result'] == 'DIE_CRASH' || $get_params['task_result'] == 'DIE_HANGUP') {

        echo '************** finish_code '.$get_params['task_result'].' **********************'.PHP_EOL;

        echo ' trader site: '.$task_results['task_trader'].PHP_EOL;
        $file_log = CRASH_LOG_DIR . '/crash_'.date('Y-m-d_H-i-s').'.log';
        file_put_contents($file_log, $last_log);
        echo ' save log to '.$file_log.PHP_EOL;

        alert_service('notice', $get_params['task_result'].' on site '.$task_results['task_trader'].' log: '.$file_log);

        echo '************** END finish_code '.$get_params['task_result'].' **********************'.PHP_EOL;

       $last_line_of_log = array_slice($crash_log_arr, count($crash_log_arr)-5);
       $crash_short_report = 'cmd: '.$cmd_log.PHP_EOL.print_r($last_line_of_log,true).PHP_EOL;

    }
    elseif ($get_params['task_result'] == 'TASK_FINISH_NONE') {

        echo '************** finish_code TASK_FINISH_NONE **********************'.PHP_EOL;

        echo ' trader site: '.$task_results['task_trader'].PHP_EOL;
        $file_log = CRASH_LOG_DIR . '/'.$task_results['task_trader'].'_task_finish_none_' . date('Y-m-d_H-i-s') . '.log';
        file_put_contents($file_log, $last_log);
        echo ' save log to '.$file_log.PHP_EOL;

        alert_service('notice', 'TASK_FINISH_NONE on site '.$task_results['task_trader'].' log: '.$file_log);

        echo '************** END finish_code TASK_FINISH_NONE **********************'.PHP_EOL;

    }
    elseif ($get_params['task_result'] == 'NOT_FOUND_THUMB') {

        echo '************** finish_code NOT_FOUND_THUMB **********************'.PHP_EOL;

        echo ' trader site: '.$task_results['task_trader'].PHP_EOL;
        $file_log = CRASH_LOG_DIR . '/'.$task_results['task_trader'].'_not_found_thumb_' . date('Y-m-d_H-i-s') . '.log';
        file_put_contents($file_log, $last_log);
        echo ' save log to '.$file_log.PHP_EOL;

        alert_service('notice', 'NOT_FOUND_THUMB on site '.$task_results['task_trader'].' log: '.$file_log);

        echo '************** END finish_code NOT_FOUND_THUMB **********************'.PHP_EOL;

    }
    elseif ($get_params['task_result'] == 'OPEN_NOT_TRADER') {

        echo '************** finish_code OPEN_NOT_TRADER **********************'.PHP_EOL;

        echo ' trader site: '.$task_results['task_trader'].PHP_EOL;
        $file_log = CRASH_LOG_DIR . '/'.$task_results['task_trader'].'_open_not_trader_' . date('Y-m-d_H-i-s') . '.log';
        file_put_contents($file_log, $last_log);
        echo ' save log to '.$file_log.PHP_EOL;

        alert_service('notice', 'OPEN_NOT_TRADER on site '.$task_results['task_trader'].' log: '.$file_log);

        echo '************** END finish_code OPEN_NOT_TRADER **********************'.PHP_EOL;

    }

    elseif ($get_params['task_result'] == 'undefined') {

        echo '************** finish_code undefined **********************'.PHP_EOL;

        echo ' trader site: '.$task_results['task_trader'].PHP_EOL;
        $file_log = CRASH_LOG_DIR . '/'.$task_results['task_trader'].'_undefined_' . date('Y-m-d_H-i-s') . '.log';
        file_put_contents($file_log, $last_log);
        echo ' save log to '.$file_log.PHP_EOL;

        alert_service('notice', 'code undefined on site '.$task_results['task_trader'].' log: '.$file_log);

        echo '************** END finish_code undefined **********************'.PHP_EOL;

    }


    $curl = curl_init();
    // curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_URL, $url_get);
    // curl_setopt($curl, CURLOPT_POST, 1);
    // curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 15);
    curl_setopt($curl, CURLOPT_TIMEOUT, 15);

    $result = curl_exec($curl);

    if (curl_errno($curl)) {

        $error_text = curl_error($curl);

        $result = '[ERROR] CURL ERRROR  -------- >  ' . $error_text.'   $url_get = '.$url_get.PHP_EOL ;
        echo $result;
        result_log($result);

    }

    curl_close($curl);

    $proxy_finish_res = finish_task($token, $get_params['task_result']);
    if (!$proxy_finish_res) finish_task($token, $get_params['task_result']);
    if (!$proxy_finish_res) finish_task($token, $get_params['task_result']);

    if (!isset($task_url_parms)) {$task_url_parms=[];}

    result_log(
        ' runtime_total: ' . $time_consume_total_str .
        ' loop_count: ' . $loop_count .
        ' loops_time: ' . $time_consume_loop_str .
        ' speed_loop: ' . $loop_speed . ' (loop/hour) ' .
        ' $task_url_parms: ' . PHP_EOL . print_r($task_url_parms, true).
        ' $task_results: ' . PHP_EOL . print_r($task_results, true).
        ' $get_params: ' . PHP_EOL . print_r($get_params, true).
        ' url_history: '. PHP_EOL.print_r($url_history,true).PHP_EOL.
        ' postback_call (length: '.strlen($url_get).'): '. $url_get.PHP_EOL.
        ' postback_answer: '. $result.PHP_EOL.
        ' $crash_short_report: '.$crash_short_report.PHP_EOL.
        '----------------------------------------------------------------------------------------------------------'.PHP_EOL);

    return $result;
}

function sendFile($url, $file_name_with_full_path, $file_name)
{

    echo 'sendFile() ' . PHP_EOL .
        '   url=' . $url . PHP_EOL .
        '   file_name_with_full_path= ' . $file_name_with_full_path . PHP_EOL .
        '   file_name = ' . $file_name . PHP_EOL;

    $cfile = curl_file_create($file_name_with_full_path, 'application/gzip', $file_name);
    $post = ['test_file' => $cfile];

    print_r($post);

    $curl = curl_init();
    //curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
    //curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 15);
    curl_setopt($curl, CURLOPT_TIMEOUT, 15);
    $result = curl_exec($curl);

    if (curl_errno($curl)) {

        $error_text = curl_error($curl);

        $answer_error = '[ERROR] CURL ERRROR  -------- >  ' . $error_text . '   $url = '.$url.PHP_EOL;
        echo $answer_error;

    }

    curl_close($curl);

    return $result;
}

function unlinkDir($dir)
{
    $dirs = [$dir];
    $files = [];
    for ($i = 0; ; $i++) {
        if (isset($dirs[$i]))
            $dir = $dirs[$i];
        else
            break;

        if ($openDir = opendir($dir)) {
            while ($readDir = @readdir($openDir)) {
                if ($readDir != "." && $readDir != "..") {

                    if (is_dir($dir . "/" . $readDir)) {
                        $dirs[] = $dir . "/" . $readDir;
                    } else {

                        $files[] = $dir . "/" . $readDir;
                    }
                }
            }

        }

    }


    foreach ($files as $file) {
        unlink($file);

    }
    $dirs = array_reverse($dirs);
    foreach ($dirs as $dir) {
        rmdir($dir);
    }
}

function result_log($msg)
{
    global $logFile, $demon_id;
    $msg_total = date('H:i:s') . ' DID:' . $demon_id . ' ' . $msg . PHP_EOL;
    file_put_contents($logFile, $msg_total, FILE_APPEND);
    echo $msg_total;
}

function wlog($msg)
{
    echo $msg . PHP_EOL;
}

function secsToTime($seconds)
{
    $hours = floor($seconds / 3600);
    $mins = floor($seconds / 60 % 60);
    $secs = floor($seconds % 60);

    if (strlen($hours) == 1) {
        $hours = '0' . $hours;
    }
    if (strlen($mins) == 1) {
        $mins = '0' . $mins;
    }
    if (strlen($secs) == 1) {
        $secs = '0' . $secs;
    }

    return $hours . ':' . $mins . ':' . $secs;
}

// fgetsPending( $in,$tv_sec ) - Get a pending line of data from stream $in, waiting a maximum of $tv_sec seconds
function fgetsPending(&$in, $tv_sec = 80, $token)
{
    global $time_start_loop, $loop_count;

    $res = stream_select($read = [$in], $write = NULL, $except = NULL, $tv_sec);

    $time_consume_loop = microtime(true) - $time_start_loop;

    if ($time_consume_loop > 600) {


        kill_cmd_process($token);

        return FALSE;

    }
    else if ($res == 0 or $res === false) {

        result_log('[RESTART] -+- TIMEOUT wait line over ' . $tv_sec . ' secs');
        wlog('-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+');
        wlog('fail (2) [RESTART] TIMEOUT wait line over ' . $tv_sec . ' secs');
        wlog('-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+');

        result_log('fail [RESTART] TIMEOUT wait line over ' . $tv_sec . ' secs');


        kill_cmd_process($token);

        return FALSE;

    } else {

        return fgets($in);
    }

}

function kill_cmd_process($token)
{
    global $last_log, $process, $pipes, $task_results,$task_results;

    $task_results['task_result'] = 'DIE_HANGUP';
    $last_log .= date('Y-m-d_H:i:s').'-+-+-+-+-+-+-DIE_HANGUP+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+'.PHP_EOL;
    postBack($task_results, $token);
//    $get_params['task_result'] = 'DIE_HANG';
//    postBack($task_results);

    wlog(' ');
    wlog('-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+');
    wlog('### kill_cmd_process() - start');

//    $task_results['task_result'] = 'CLICKER_FAIL';
//
//    wlog(' sending postBack ...');
//    postBack($task_results);

    $status = proc_get_status($process);
    wlog('proc_get_status($process)' . $status . '[NO_LIMIT]');
    if ($status['running'] == true) { //process ran too long, kill it
        //close all pipes that are still open

        wlog('close all pipes that are still open');

        //close stdin. By closing stdin, the program should exit
        //after it finishes processing the input
        fclose($pipes[0]);
        fclose($pipes[1]); //stdout
        fclose($pipes[2]); //stderr
        //get the parent pid of the process we want to kill
        $ppid = $status['pid'];
        //use ps to get all the children of this process, and kill them
        $pids = preg_split('/\s+/', `ps -o pid --no-heading --ppid $ppid`);

        foreach ($pids as $pid) {
            $pids2 = preg_split('/\s+/', `ps -o pid --no-heading --ppid $pid`);
            if ($pids2) {
                foreach ($pids2 as $pid2) {
                    $pids3 = preg_split('/\s+/', `ps -o pid --no-heading --ppid $pid2`);
                    if ($pids3) {
                        foreach ($pids3 as $pid3) {
                            if (is_numeric($pid3)) {
                                result_log('Killing (3)' . $pid3);
                                posix_kill($pid3, 9); //9 is the SIGKILL signal
                            }
                        }
                    }
                    if (is_numeric($pid2)) {
                        result_log('Killing (2)' . $pid2);
                        posix_kill($pid2, 9); //9 is the SIGKILL signal
                    }
                }
            }
            if (is_numeric($pid)) {
                result_log('Killing (1)' . $pid);
                posix_kill($pid, 9); //9 is the SIGKILL signal
            }
        }
    }

    wlog('sleep 10');
    sleep(10);

    $status = proc_get_status($process);
    wlog('proc_get_status($process) ' . $status . '[NO_LIMIT]');

    wlog('proc_terminate() ');
    //proc_close($process);
    proc_terminate($process);

    wlog('sleep 10');
    sleep(10);

    $status = proc_get_status($process);
    wlog('proc_get_status($process) ' . $status . '[NO_LIMIT]');


    result_log('### kill_cmd_process() - finish');

    wlog('-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+');
    wlog(' ');

    return false;

}

function proper_parse_str($str) {

    if (!isset($str) || empty($str)) return [];


    # result array
    $arr = array();

    # split on outer delimiter
    $pairs = explode('&', $str);

    # loop through each pair
    foreach ($pairs as $i) {
        # split into name and value
        list($name,$value) = explode('=', $i, 2);

        # if name already exists
        if( isset($arr[$name]) ) {
            # stick multiple values into an array
            if( is_array($arr[$name]) ) {
                $arr[$name][] = $value;
            }
            else {
                $arr[$name] = array($arr[$name], $value);
            }
        }
        # otherwise, simply stick it in a scalar
        else {
            $arr[$name] = $value;
        }
    }

    # return result array
    return $arr;
}


/* get_task()
 * @return array [string redirect_url, array $task_get_params]
 * */
function get_task($BALANCER, $TASK_GET_URL, $USER, $token)
{
    global $DEBUG, $debug_mode_site, $debug_mode_action, $debug_mode_referer;



        $proxyauth = "${USER}:${token}";
        $curl = curl_init();
//curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $TASK_GET_URL);
        curl_setopt($curl, CURLOPT_PROXY, $BALANCER);
        curl_setopt($curl, CURLOPT_PROXYUSERPWD, $proxyauth);
// curl_setopt($curl, CURLOPT_POST, 1);
//curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_VERBOSE, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 15);
        curl_setopt($curl, CURLOPT_TIMEOUT, 18);
        curl_setopt($curl, CURLOPT_HEADER, 1);
        $result = curl_exec($curl);
        // echo '$result = '.$result.PHP_EOL.PHP_EOL;
//echo parseHeaders($result,"Content-Type");

        $headers = get_headers_from_curl_response($result);

        $redirect_url = $headers['location'];
        if (!isset($redirect_url) || empty($redirect_url)) $redirect_url = $headers['Location'];

   /* } else {
        $redirect_url = 'http://gayfreeporn.xyz/redirect_task.html?b=undefined&ip=12.239.105.139&tz=unknown&country=US&action=trader.usb&type=7&site=gayfreeporn.xyz&site_url=http%3A%2F%2Fsexgames.bz&trader=sexgames.bz&site_id=136&trader_id=5103&lang=en-us%2Cen%3Bq%3D0.5&useragent=Mozilla%2F5.0%20(Macintosh%3B%20Intel%20Mac%20OS%20X%2010_11_3)%20AppleWebKit%2F601.4.4%20(KHTML%2C%20like%20Gecko)%20Version%2F9.0.3%20Safari%2F601.4.4&refer=http%3A%2F%2Fsexgames.bz&clicks=1&task_id=11146847&task_uniq_id=S11035ded55d29b44e845&pport=8080&pprovider=direct&navigator_id=us/en-us/undefined/windows/7/undefined/undefined/firefox/44/amd64__44.0__14.json';
    }*/

    echo '$redirect_url ($headers[\'location\']) = '.$redirect_url.PHP_EOL;


    if (strpos($redirect_url,'http://no.task/') !== false) {
        return [
            'redirect_url' => $redirect_url,
            'task_get_params' =>false,
            'navigator_json' => false
        ];
    }
    if (strpos($redirect_url,'http://no.proxy/') !== false) {
        return [
            'redirect_url' => $redirect_url,
            'task_get_params' =>false,
            'navigator_json' => false
        ];
    }
    if (strpos($redirect_url,'http://bad.proxy/') !== false) {
        return [
            'redirect_url' => $redirect_url,
            'task_get_params' =>false,
            'navigator_json' => false
        ];
    }

    $parts = parse_url($redirect_url);
    parse_str($parts['query'], $task_get_params);
    echo 'task_get $task_get_params: '.PHP_EOL;
    print_r($task_get_params);




    if ($debug_mode_site && $debug_mode_action) {
        echo 'get_task $debug_mode_site = '.$debug_mode_site.PHP_EOL;
        echo 'get_task $debug_mode_action = '.$debug_mode_action.PHP_EOL;
        $task_get_params['site_url'] = $debug_mode_site;
        $task_get_params['refer'] = $debug_mode_site;
        $task_get_params['trader'] = substr($debug_mode_site,7,strlen($debug_mode_site));
        $task_get_params['action'] = $debug_mode_action; //'trader.usb';
        $task_get_params['site'] = 'google.com';
        $task_get_params['task_shard'] = 'task_shard';
        if ($debug_mode_referer) {
            echo '$debug_mode_action = '.$debug_mode_action.PHP_EOL;
            $task_get_params['site'] = $debug_mode_referer;
        }

        $redirect_url = 'http://' . $task_get_params['site'].'/redirect_task.html?'. http_build_query($task_get_params, '', '&', PHP_QUERY_RFC1738);
        echo '----> debug rebuild $redirect_url = '.$redirect_url.PHP_EOL;
       // print_r($task_get_params);
    }


    $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
    echo '$header_size = '.$header_size.PHP_EOL;
    $header = substr($result, 0, $header_size);
    $body_json = substr($result, $header_size);
    echo '$body_json size = '.strlen($body_json).PHP_EOL;



    if (curl_errno($curl)) {
        $error_text = curl_error($curl);
        $result = PHP_EOL.' ### get_task ressults ### [ERROR] CURL ERRROR  -------- >  ' . $error_text.'   $TASK_GET_URL =  '.$TASK_GET_URL.PHP_EOL;
        echo $result;
        result_log($result);

        return false;
    }

    curl_close($curl);

    if ($redirect_url && $task_get_params && $body_json) {
        echo PHP_EOL.'  ### get_task ressults ### get_task [OK]'.PHP_EOL;
        return [
            'redirect_url' => $redirect_url,
            'task_get_params' =>$task_get_params,
            'navigator_json' => $body_json,
        ];
    } else {
        echo PHP_EOL.'  ### get_task ressults ### [ERROR] parse http answer for get $redirect_url '.PHP_EOL.' http response is :'.PHP_EOL;

        if (!$redirect_url) echo ' details: !$redirect_url'.PHP_EOL;
        if (!$task_get_params) echo ' details: !$task_get_params'.PHP_EOL;
        if (!$body_json) echo ' details: !$body_json (navigator_json)'.PHP_EOL;

        echo print_r([
            'redirect_url' => $redirect_url,
            'task_get_params' =>$task_get_params,
            'navigator_json' => $body_json,
        ], true).PHP_EOL;

        return false;
    }

}


/*
 * call proxy for free proxy-port
 * */
function finish_task($token, $code)
{
    global $BALANCER, $TASK_FINISH_URL_BALANCER, $USER;

    echo PHP_EOL.'  start call finish url with code = '.$code.' token = '.$token.PHP_EOL.PHP_EOL;

    $URL = $TASK_FINISH_URL_BALANCER.'?code='.$code;
    $proxyauth = "${USER}:${token}";
    $curl = curl_init();
//curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_URL, $URL);
    curl_setopt($curl, CURLOPT_PROXY, $BALANCER);
    curl_setopt($curl, CURLOPT_PROXYUSERPWD, $proxyauth);
// curl_setopt($curl, CURLOPT_POST, 1);
//curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 15);
    curl_setopt($curl, CURLOPT_TIMEOUT, 15);
  //  curl_setopt($curl, CURLOPT_HEADER, 1);

    $result = curl_exec($curl);

    echo PHP_EOL.'TASAK FINISH code = '.$code.' response = '.$result.' token = '.$token.PHP_EOL.PHP_EOL;

    if (curl_errno($curl)) {
        $error_text = curl_error($curl);
        $result = '[ERROR] CURL ERRROR finish url  -------- >  ' . $error_text;
        echo $result;
        result_log($result);

        return false;

    } else {

        curl_close($curl);

        return true;
    }

}


function alert_service($type, $message)
{
    global $ALERT_SERVICE_URL, $NODE;

    echo PHP_EOL.'  start call alert_service $type = '.$type.' $message = '.$message.PHP_EOL;

    $URL = $ALERT_SERVICE_URL;

    $data = [
        'project' => 'clk',
        'sender' =>  $NODE,
        'type' =>    $type,
        'message' => $message
    ];

    $post = json_encode($data);

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $URL);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($curl, CURLOPT_TIMEOUT, 15);

    $result = curl_exec($curl);

    echo PHP_EOL.'call alert_service response = '.$result.PHP_EOL.PHP_EOL;

    if (curl_errno($curl)) {
        $error_text = curl_error($curl);
        $result = '[ERROR] CURL ERRROR finish url  -------- >  ' .$URL. ' err ='. $error_text;
        echo $result.PHP_EOL;
        result_log($result);

        return false;
    }

    curl_close($curl);

    return true;

}


function get_headers_from_curl_response($response)
{
    $headers = array();

    $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));

    foreach (explode("\r\n", $header_text) as $i => $line)
        if ($i === 0)
            $headers['http_code'] = $line;
        else {
            list ($key, $value) = explode(': ', $line);

            $headers[$key] = $value;
        }

    return $headers;
}


class UUID
{
    public static function v3($namespace, $name)
    {
        if (!self::is_valid($namespace)) return false;

        // Get hexadecimal components of namespace
        $nhex = str_replace(['-', '{', '}'], '', $namespace);

        // Binary Value
        $nstr = '';

        // Convert Namespace UUID to bits
        for ($i = 0; $i < strlen($nhex); $i += 2) {
            $nstr .= chr(hexdec($nhex[$i] . $nhex[$i + 1]));
        }

        // Calculate hash value
        $hash = md5($nstr . $name);

        return sprintf('%08s-%04s-%04x-%04x-%12s',

            // 32 bits for "time_low"
            substr($hash, 0, 8),

            // 16 bits for "time_mid"
            substr($hash, 8, 4),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 3
            (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

            // 48 bits for "node"
            substr($hash, 20, 12)
        );
    }

    public static function v4()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public static function v5($namespace, $name)
    {
        if (!self::is_valid($namespace)) return false;

        // Get hexadecimal components of namespace
        $nhex = str_replace(['-', '{', '}'], '', $namespace);

        // Binary Value
        $nstr = '';

        // Convert Namespace UUID to bits
        for ($i = 0; $i < strlen($nhex); $i += 2) {
            $nstr .= chr(hexdec($nhex[$i] . $nhex[$i + 1]));
        }

        // Calculate hash value
        $hash = sha1($nstr . $name);

        return sprintf('%08s-%04s-%04x-%04x-%12s',

            // 32 bits for "time_low"
            substr($hash, 0, 8),

            // 16 bits for "time_mid"
            substr($hash, 8, 4),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 5
            (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

            // 48 bits for "node"
            substr($hash, 20, 12)
        );
    }

    public static function is_valid($uuid)
    {
        return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?' .
            '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
    }
}

function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}