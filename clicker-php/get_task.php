<?php

$TASK_GET_URL = 'http://get.task';
$USER = 'phantom'; // phantom biterika_test
$BALANCER = '163.172.160.155:789';

$token = substr(str_replace('-', '', UUID::v4() . UUID::v4()), 11, 40);

$res  =get_task($BALANCER, $TASK_GET_URL, $USER, $token);

// http://no.proxy/


print_r($res);

/* get_task()
 * @return array [string redirect_url, array $task_get_params]
 * */
function get_task($BALANCER, $TASK_GET_URL, $USER, $token)
{
    $proxyauth = "${USER}:${token}";
    $curl = curl_init();
//curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_URL, $TASK_GET_URL);
    curl_setopt($curl, CURLOPT_PROXY, $BALANCER);
    curl_setopt($curl, CURLOPT_PROXYUSERPWD, $proxyauth);
// curl_setopt($curl, CURLOPT_POST, 1);
//curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
    curl_setopt($curl, CURLOPT_TIMEOUT, 60);
    curl_setopt($curl, CURLOPT_HEADER, 1);

    $result = curl_exec($curl);
    echo $result;
//echo parseHeaders($result,"Content-Type");

    $headers = get_headers_from_curl_response($result);

    $redirect_url = $headers['location'];

    echo '$redirect_url = '.$redirect_url.PHP_EOL;

    $parts = parse_url($redirect_url);
    parse_str($parts['query'], $task_get_params);
    print_r($task_get_params);


    if (curl_errno($curl)) {
        $error_text = curl_error($curl);
        $result = '[ERROR] CURL ERRROR  -------- >  ' . $error_text;
        echo $result;
        result_log($result);

        return false;
    }

    curl_close($curl);

    if ($redirect_url && $task_get_params) {
        return [
            'redirect_url' => $redirect_url,
            'task_get_params' =>$task_get_params
        ];
    } else {
        echo '[ERRROR] parse http answer for get $redirect_url';
        return false;
    }

}


function finish_task($token, $code)
{
    global $BALANCER, $TASK_FINISH_URL_BALANCER, $USER;

    $URL = $TASK_FINISH_URL_BALANCER.'?code='.$code;
    $proxyauth = "${USER}:${token}";
    $curl = curl_init();
//curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_URL, $URL);
    curl_setopt($curl, CURLOPT_PROXY, $BALANCER);
    curl_setopt($curl, CURLOPT_PROXYUSERPWD, $proxyauth);
// curl_setopt($curl, CURLOPT_POST, 1);
//curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
    curl_setopt($curl, CURLOPT_TIMEOUT, 60);
    curl_setopt($curl, CURLOPT_HEADER, 1);

    $result = curl_exec($curl);
    echo PHP_EOL.'TASAK FINISH $result = '.$result.'  token = '.$token.PHP_EOL.PHP_EOL;
//echo parseHeaders($result,"Content-Type");

    if (curl_errno($curl)) {
        $error_text = curl_error($curl);
        $result = '[ERROR] CURL ERRROR finish url  -------- >  ' . $error_text;
        echo $result;
        result_log($result);

        return false;
    }

    curl_close($curl);

    return true;

}




function get_headers_from_curl_response($response)
{
    $headers = array();

    $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));

    foreach (explode("\r\n", $header_text) as $i => $line)
        if ($i === 0)
            $headers['http_code'] = $line;
        else {
            list ($key, $value) = explode(': ', $line);

            $headers[$key] = $value;
        }

    return $headers;
}

class UUID
{
    public static function v3($namespace, $name)
    {
        if (!self::is_valid($namespace)) return false;

        // Get hexadecimal components of namespace
        $nhex = str_replace(['-', '{', '}'], '', $namespace);

        // Binary Value
        $nstr = '';

        // Convert Namespace UUID to bits
        for ($i = 0; $i < strlen($nhex); $i += 2) {
            $nstr .= chr(hexdec($nhex[$i] . $nhex[$i + 1]));
        }

        // Calculate hash value
        $hash = md5($nstr . $name);

        return sprintf('%08s-%04s-%04x-%04x-%12s',

            // 32 bits for "time_low"
            substr($hash, 0, 8),

            // 16 bits for "time_mid"
            substr($hash, 8, 4),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 3
            (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

            // 48 bits for "node"
            substr($hash, 20, 12)
        );
    }

    public static function v4()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public static function v5($namespace, $name)
    {
        if (!self::is_valid($namespace)) return false;

        // Get hexadecimal components of namespace
        $nhex = str_replace(['-', '{', '}'], '', $namespace);

        // Binary Value
        $nstr = '';

        // Convert Namespace UUID to bits
        for ($i = 0; $i < strlen($nhex); $i += 2) {
            $nstr .= chr(hexdec($nhex[$i] . $nhex[$i + 1]));
        }

        // Calculate hash value
        $hash = sha1($nstr . $name);

        return sprintf('%08s-%04s-%04x-%04x-%12s',

            // 32 bits for "time_low"
            substr($hash, 0, 8),

            // 16 bits for "time_mid"
            substr($hash, 8, 4),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 5
            (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

            // 48 bits for "node"
            substr($hash, 20, 12)
        );
    }

    public static function is_valid($uuid)
    {
        return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?' .
            '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
    }
}