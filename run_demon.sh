#!/bin/bash
echo " "
echo "===================================================================================="
echo "=                 DEMON-CLICEKRS RUNNER v.1                                        ="
echo "===================================================================================="
echo " "

AMOUNT=$1
PARAM2=$1

if [ "$AMOUNT" == "" ]; then
      echo "need param - amount of demon-clickers or stop"
      exit 4
fi

if [ "$AMOUNT" == "stop" ]; then


      echo "    killing demon-clickers "

    echo "     "
    echo "    check process clicker-demon.php, phantom, phantomjs"
    ps -ef | grep 'clicker-demon.php' | grep -v grep
    #ps -ef | grep 'phantom' | grep -v grep
    ps -Al | grep -c phantomjs
    sleep 1

    #killall phantomjs.2.1.1
    killall phantomjs
    kill `ps -ef | grep 'clicker-demon.php' | grep -v grep | awk '{print $2}'`
    kill `ps -ef | grep 'phantom' | grep -v grep | awk '{print $2}'`

    sleep 2

    echo "     "
    echo "    check process clicker-demon.php, phantom, phantomjs"
    ps -ef | grep 'clicker-demon.php' | grep -v grep
    #ps -ef | grep 'phantom' | grep -v grep
    ps -Al | grep -c phantomjs
    sleep 1

    exit 0

else

    echo "cleaning dirs..."

    rm -rf /usr/share/biterika/clicker-headless2/screens/
    mkdir /usr/share/biterika/clicker-headless2/screens/

    rm -rf /usr/share/biterika/clicker-headless2/clicker-php/crash_log


     echo "    killing demon-clickers "

    echo "     "
    echo "    check process clicker-demon.php, phantom, phantomjs"
    ps -ef | grep 'clicker-demon.php' | grep -v grep
    #ps -ef | grep 'phantom' | grep -v grep
    ps -Al | grep -c phantomjs
    sleep 1

    killall phantomjs.2.1.1
    #killall phantomjs
    kill `ps -ef | grep 'clicker-demon.php' | grep -v grep | awk '{print $2}'`
    kill `ps -ef | grep 'phantom' | grep -v grep | awk '{print $2}'`

    sleep 2

    echo "     "
    echo "    check process clicker-demon.php, phantom, phantomjs"
    ps -ef | grep 'clicker-demon.php' | grep -v grep
    #ps -ef | grep 'phantom' | grep -v grep
    ps -Al | grep -c phantomjs
    sleep 1


 echo "     "
 
    COUNTER=0
    while [  $COUNTER -lt $AMOUNT ]; do

        let COUNTER=COUNTER+1

          echo "    run demon ${COUNTER} of ${AMOUNT}"
          nohup php /usr/share/biterika/clicker-headless2/clicker-php/clicker-demon.php ${COUNTER} ${PARAM2}  >/dev/null 2>&1 &
          sleep 2

    done

    exit 0

fi