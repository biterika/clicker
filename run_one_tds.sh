#!/bin/bash
echo " "
echo "===================================================================================="
echo "=                 clicker v.1                                                      ="
echo "===================================================================================="
echo " "

AMOUNT=$1

if [ "$AMOUNT" == "" ]; then
      echo "need param - amount of runs"
      exit 4
fi

COUNTER=0
while [  $COUNTER -lt $AMOUNT ]; do
    let COUNTER=COUNTER+1
    echo " "
    echo " "
    echo " "
    echo " ==================================================================================== "
    echo " "
    echo " "
    echo "    run demon ${COUNTER} of ${AMOUNT}"
    echo " "
    echo " "
    echo " "

    echo "check amount phantomjs"
    ps -Al | grep -c phantomjs

    sleep 1

#    echo "killing phantomjs ..."
#    killall phantomjs.2.1.1
#
#    sleep 1
#    echo "check amount phantomjs"
#    ps -Al | grep -c phantomjs
#
#    sleep 1

    echo "start clicker..."
    let BC=`wc -l /usr/share/biterika/clicker-headless2/config/$prefix/balancers.conf` 2>/dev/null
    let "BC=BC+1"
    let "R = $RANDOM % $BC + 1"
    auth=$(echo `date +%s%N` $RANDOM | shasum | sed -e 's/.\{1\}$//')

    echo run $auth

    echo timeout 300 /usr/share/biterika/clicker-headless2/dist/sw/phantomjs.2.1.1 --proxy=163.172.160.155:789 --proxy-auth=phantom:$auth /usr/share/biterika/clicker-headless2/clicker/clicker.js 163.172.160.155:789 sw-trade-node-72-v1-3-1 http://a.cleanburg.com/ctask? http://a.cleanburg.com/postback? phantom $auth


    sleep 1

    timeout 300 /usr/share/biterika/clicker-headless2/dist/sw/phantomjs.2.1.1 --proxy=163.172.160.155:789 --proxy-auth=phantom:$auth /usr/share/biterika/clicker-headless2/clicker/clicker.js 163.172.160.155:789 sw-trade-node-72-v1-3-1 http://a.cleanburg.com/ctask? http://a.cleanburg.com/postback? phantom $auth

    test $? -gt 128 && break;
    wait
    test $? -gt 128 && break;

    echo "finished  , waiting ..."
    sleep 5
    test $? -gt 128 && break;


    echo "continue ..."

done

exit 0