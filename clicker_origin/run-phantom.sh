#!/bin/bash

ver="v1-3-1"


#hostname $HOSTNAME-$ver

killall phantomjs.1.9.0

IFS='-' read prefix type node v <<<$HOSTNAME

echo hostname $HOSTNAME-$ver
echo /usr/share/biterika/clicker-headless2/config/$prefix/$prefix-$type-$node.conf
echo /usr/share/biterika/clicker-headless2/config/$prefix/balancers.conf

while true; do

    date

    config=$(cat /usr/share/biterika/clicker-headless2/config/$prefix/$prefix-$type-$node.conf)

    echo config

    let BC=`wc -l /usr/share/biterika/clicker-headless2/config/$prefix/balancers.conf` 2>/dev/null
    let "BC=BC+1"

    IFS=', ' read PS PD <<<$config

    for line in `echo $config`; do

        IFS=', ' read count login pwd url_task url_finish phantom active <<<$line
        runned=`ps aux | grep $login | grep timeout | grep -v grep -c`
        active=$(echo $active | tr -d '\r\n')

        echo do $count $login active: $active runned: $runned

        if [ $active = "no" ]; then

            if [ $runned -gt 0 ]; then
                echo killall $login
                kill $(ps aux | grep proxy-auth=$login | awk '{print $2}') 2>/dev/null
            fi

        else

            if [ $runned -lt $count ]; then
                let "count = $count-1"
                for i in $(seq $runned $count); do

                    let "R = $RANDOM % $BC + 1"
                    balancer=$(sed -n "$R p" /usr/share/biterika/clicker-headless2/config/$prefix/balancers.conf | tr -d '\r\n')
                    auth=$pwd$(echo `date +%s%N` $RANDOM | shasum | sed -e 's/.\{1\}$//')
                    echo run $i $login:$auth $balancer

                timeout 300 $phantom --proxy=$balancer --proxy-auth=$login:$auth /usr/share/biterika/clicker-headless2/clicker/clicker.js $balancer $HOSTNAME-$ver $url_task $url_finish $login $auth >/dev/null & #>/var/log/clicker.log >/dev/null &

           # timeout 300 $phantom --proxy=$balancer --proxy-auth=$login:$auth /usr/share/biterika/clicker-headless2/clicker/clicker.js $balancer $HOSTNAME-$ver $url_task $url_finish $login $auth >/var/log/clicker.log & # >/dev/null &



                    sleep .1

                done
            fi

        fi

    done

    echo ""

    sleep 10

done
