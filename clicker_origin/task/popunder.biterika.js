var system  = require('system');
var args    = system.args;

//Libs
var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var random  = require('./../lib/random');
var Q       = require('./../modules/q/q');

//Actions
var check_popup = require('./../actions/check_popup').exec;
var make_click  = require('./../actions/make_click').exec;
var click_xy    = require('./../actions/click_xy').exec;

exports.exec = function(page) {

    var deferred = Q.defer();

    Q.fcall(function () {


        delete param.task.click_task;

        var deferred = Q.defer();
        debug.log('');
        debug.log('Wait', config.timeout.birge_after_open / 1000, 'sec');
        setTimeout(function () {
            deferred.resolve(true);
        }, config.timeout.birge_after_open);
        return deferred.promise;


    }).then(function () {


        debug.log('');
        debug.log('Make click 1x1 ...');

        click_xy(page, 1 ,1, 'left');

        var deferred = Q.defer();
        setTimeout(function () {
            deferred.resolve(true);
        }, config.timeout.birge_after_open);
        return deferred.promise;


    }).then(function () {


        var click = random.int(1, 1);
        if (click == 1) {
            var pfc = get_last_tab(page);
            debug.log('');
            debug.log('Make first click ... in',pfc.id,pfc.url);
            param.task.click_ok++;
            return make_click(get_last_tab(page));
        } else {
            debug.log('');
            debug.log('Don\'t make clicks');
            return false;
        }


    }).then(function (click1) {


        if (!click1)
            return false;

        var click = random.int(1, 2);
        if (click == 1) {
            var pfc = get_last_tab(page);
            debug.log('');
            debug.log('Make second click ... in',pfc.id,pfc.url);
            param.task.click_ok++;
            return make_click(pfc);
        } else {
            debug.log('');
            debug.log('Don\'t make second click');
            return true;
        }


    }).then(function () {


        var deferred = Q.defer();
        debug.log('');
        debug.log('Wait', config.timeout.birge_before_close / 1000, 'sec');
        setTimeout(function () {
            deferred.resolve(true);
        }, config.timeout.birge_before_close);
        return deferred.promise;


    }).then(function () {

        var code = 'TASK_FINISH_FULL';

        deferred.resolve(code);


    }).catch(function (e) {


        deferred.reject(e.message);


    }).done(function () {
    });

    return deferred.promise;

}

//======================================================================================================================

function get_last_tab(obj){

    var res = obj;
    var max_id = res.id;

    for(var id in obj.pages){
        if(obj.pages.hasOwnProperty(id)){
            var page_id = obj.pages[id].id;
            if(page_id > max_id){
                max_id = obj.pages[id].id;
                res = obj.pages[id];
            }
        }
    }

    return res;


}