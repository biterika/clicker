var system  = require('system');
var args    = system.args;

//Libs
var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var random  = require('./../lib/random');
var Q       = require('./../modules/q/q');

//Actions
var check_popup = require('./../actions/check_popup').exec;
var make_click  = require('./../actions/make_click').exec;

exports.exec = function(page) {

    var deferred = Q.defer();

    Q.fcall(function () {


        delete param.task.click_task;

        var deferred = Q.defer();
        debug.log('');
        debug.log('Wait', config.timeout.birge_after_open / 1000, 'sec');
        setTimeout(function () {
            deferred.resolve(true);
        }, config.timeout.birge_after_open);
        return deferred.promise;


    }).then(function () {


        var click = random.int(1, 2);
        if (click == 1) {
            debug.log('');
            debug.log('Make first click ...');
            param.task.click_ok++;
            return make_click(page);
        } else {
            debug.log('');
            debug.log('Don\'t make clicks');
            return false;
        }


    }).then(function (click1) {


        if (!click1)
            return false;

        var click = random.int(1, 5);
        if (click == 1) {
            debug.log('');
            debug.log('Make second click ...');
            param.task.click_ok++;
            return make_click(page);
        } else {
            debug.log('');
            debug.log('Don\'t make second click');
            return true;
        }


    }).then(function () {


        var deferred = Q.defer();
        debug.log('');
        debug.log('Wait', config.timeout.birge_before_close / 1000, 'sec');
        setTimeout(function () {
            deferred.resolve(true);
        }, config.timeout.birge_before_close);
        return deferred.promise;


    }).then(function () {

        var code = 'TASK_FINISH_FULL';

        deferred.resolve(code);


    }).catch(function (e) {


        deferred.reject(e.message);


    }).done(function () {
    });

    return deferred.promise;

}