#!/bin/bash

killall phantomjs.1.9.0 phantomjs.2.0 slimerjs Xvfb x11vnc

IFS='-' read prefix type ip <<<$HOSTNAME
hostname=$prefix

echo hostname $hostname

while true; do

    date

    rm balancers
    curl -s http://10.1.2.9/$hostname/balancers -O
    config=$(curl -s http://10.1.2.9/$hostname/config.phantom)

    let BC=`wc -l balancers` 2>/dev/null
    let "BC=BC+1"

    IFS=', ' read PS PD <<<$config

    for line in `echo $config`; do

        IFS=', ' read count login active <<<$line
        runned=`ps aux | grep $login | grep timeout | grep -v grep -c`
        active=$(echo $active | tr -d '\r\n')

        echo do $count $login active: $active runned: $runned

        if [ $active = "no" ]; then

            if [ $runned -gt 0 ]; then
                echo killall $login
                kill $(ps aux | grep proxy-auth=$login | awk '{print $2}') 2>/dev/null
            fi

        else

            if [ $runned -lt $count ]; then
                let "count = $count-1"
                for i in $(seq $runned $count); do

                    let "R = $RANDOM % $BC + 1"
                    balancer=$(sed -n "$R p" balancers | tr -d '\r\n')
                    auth=$(echo `date +%s%N` $RANDOM | shasum | sed -e 's/.\{1\}$//')
                    echo run $i $login:$auth $balancer
                    xvfb-run -a timeout 300 slimerjs --proxy=$balancer --proxy-auth=$login:$auth /usr/share/biterika/clicker-headless2-v6.0/clicker.js $balancer $HOSTNAME 'http://195.154.223.155/bot.cgi?default&b='$login 'http://195.154.223.155/finish.cgi?10&group=v7' $login $auth >/dev/null & #>/var/log/clicker.log >/dev/null &
                    sleep .1

                done
            fi

        fi

    done

    echo ""

    sleep 10

done