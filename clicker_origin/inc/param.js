var random = require('./../lib/random');

exports.task = {
    server      : 'undefined',
    login       : 'undefined',
    token       : 'undefined',
    balancer    : 'undefined',
    ip          : '',
    fake        : 0,
    tds         : false,
    site        : false,
    trader      : false,
    task_type   : false,
    click_task  : random.int(1,3),
    click_ok    : 0,
    click_content : 0,
    delta       : 0,
    code        : 'UNDEFINED'
};

exports.page_created = false;
exports.args = [];
exports.make_screen = false;
exports.task_url = '';
exports.finish_url = '';
exports.verbose = true;
exports.token = '';
exports.step = 0;
exports.page_id = 0;
exports.close_popup = 0;
exports.popupLoading = false;
exports.have_thumb = 0;
exports.log_id = '';
exports.log = [];
exports.url_log = [];
