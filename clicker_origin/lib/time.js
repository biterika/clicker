var time_start = parseInt(new Date().getTime()/1000);
var time = function(){
    var d = new Date();
    var h = d.getHours();if (h <= 9) h = "0" + h;
    var m = d.getMinutes();if (m <= 9) m = "0" + m;
    var s = d.getSeconds();if (s <= 9) s = "0" + s;
    return h+':'+m+':'+s;
};
var time_delta = function(){
    var time_now = parseInt(new Date().getTime()/1000);
    return time_now - time_start;
};
var time_int = function(){
    return parseInt(new Date().getTime()/1000)
};

function reset(){
    time_start = parseInt(new Date().getTime()/1000);
}

exports.reset = reset;
exports.time = time;
exports.time_delta = time_delta;
exports.time_int = time_int;
