var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var random  = require('./../lib/random');
var Q       = require('./../modules/q/q.js');

var random_selector = require('./random_selector').exec;
var click_xy        = require('./click_xy').exec;

exports.exec = function(obj, selector, button){
    debug.log('Click selector:', selector, 'button:', button, 'in page:', obj.id, obj.url);

    var deferred = Q.defer();

    var element = random_selector(obj, selector);
    debug.json(JSON.stringify(element));
    if(!element.x || !element.y){

        debug.log('!element.x || !element.y');

        deferred.resolve({
            status: 'error2'
        });

    }else {

        var x = random.int(element.x+3,element.x+element.width-3);
        var y = random.int(element.y+3,element.y+element.height-3);

        debug.log('click', x , y, 'wait', config.timeout.before_click);

        setTimeout(function(){

            debug.log('do click', x , y);

            click_xy(obj, x, y, button);

            var result = {
                status: 'success'
            };
            deferred.resolve(result);

        }, config.timeout.before_click);

    }

    return deferred.promise;


};
