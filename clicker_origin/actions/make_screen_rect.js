var config  = require('./../inc/config');
var param   = require('./../inc/param');
var time    = require('./../lib/time');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');
var fs      = require('fs');

var setTimeoutStop = require('./events').setTimeoutStop;

exports.exec = function(obj, rect){

    //param.step++;

    debug.log('');
    debug.log('Make screenshot:', obj.url);

    var deferred = Q.defer();

    if(config.const.make_screen){

        obj.evaluate(function() {
            document.body.bgColor = 'white';
        });

        obj.clipRect = rect;

        var screen = obj.renderBase64('JPEG');

        setTimeout(function(){

            var page_screen = require('webpage').create();
            page_screen.customHeaders = {"Content-Type":"application/json"};
            setTimeoutStop(page_screen, config.timeout.loading);

            var data = JSON.stringify({
                taskid: param.task.task_id,
                screen: screen,
                dom: obj.content
            });

            page_screen.open(config.url.send_data, 'POST', data, function (status) {
                result = {
                    status: status
                };
                deferred.resolve(result);
            });

        }, 1000);

    }else{

        var result = {
            status: 'success'
        };
        deferred.resolve(result);

    }

    return deferred.promise;

};
