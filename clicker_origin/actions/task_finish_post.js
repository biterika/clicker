var fs      = require('fs');

var time    = require('./../lib/time');
var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

//var setTimeoutStop = require('./../actions/events').setTimeoutStop;

exports.exec = function(task){

    param.step++;

    debug.log('');
    debug.log('Task finish:', task.code, 'Clicks:', task.click_ok, '/', task.click_task);

    var deferred = Q.defer();

    var page_task = require('webpage').create();

    var url = param.finish_url +
        '&code='    + task.code +
        '&server='  + task.server +
        '&b='       + task.login +
        '&token='   + task.token +
        '&balancer=' + task.balancer +
        '&site='    + task.site +
        '&trader='  + task.trader +
        '&task_type=' + task.task_type +
        '&click_task=' + task.click_task +
        '&click_ok=' + task.click_ok +
        '&delta='   + task.delta;

    var provider = '';
    for(var id in param.args){
        if(param.args.hasOwnProperty(id)){
            if(param.args[id][0] == 'p'){
                provider = param.args[id][1];
            }
            url += '&'+param.args[id][0]+'='+param.args[id][1];
        }
    }

    debug.log('finish_url', url);

    var now = new Date().getTime();
    var time_start = parseInt(now, 10);

    var settings = {
        operation: "POST",
        encoding: "utf8",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify({
            task: param.task,
            provider: provider,
            url: url,
            log: param.log,
            url_log: param.url_log
        })
    };
    page_task.open(param.finish_url, settings, function(status) {
        console.log('Status: ' + status);
        page_task.open(url, function (status) {
            console.log(new Date(),status);
            console.log(JSON.stringify(param.task));
            setTimeout(function(){ phantom.exit(0); }, 0);
        });
    });


    return deferred.promise;

};
