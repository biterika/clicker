var config  = require('./../inc/config');
var param   = require('./../inc/param');
var time    = require('./../lib/time');
var debug   = require('./../lib/debug');
var random  = require('./../lib/random');
var Q       = require('./../modules/q/q.js');
var fs      = require('fs');

exports.exec = function(obj){

    debug.log('');
    debug.log('Make screenshot:', obj.id, obj.url);

    //var deferred = Q.defer();

    /*obj.evaluate(function() {
        document.body.bgColor = 'black';
    });*/

    var filename = '/usr/share/biterika/screen/'+Date.now()+'-'+obj.id+'-'+random.char(4)+'.jpg';

    debug.log('render '+filename);
    obj.render(filename, {format: 'jpeg', quality: '10'});

    //var content = fs.read(filename);
    //debug.log('send screen',content.length);
    //TODO send
    //fs.remove(filename);

    var result = {
        status: 'success'
    };
    //deferred.resolve(result);

    //return deferred.promise;

};
