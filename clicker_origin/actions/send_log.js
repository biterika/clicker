var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

var setTimeoutStop = require('./events').setTimeoutStop;

exports.exec = function(cb){

    if(config.const.send_log == true){

        param.step++;

        var page_log = require('webpage').create();
        page_log.customHeaders = {"Content-Type":"application/json"};
        setTimeoutStop(page_log, config.timeout.loading);

        var data = JSON.stringify({
            taskid: param.task.task_id,
            log: param.log
        });

        debug.log('');
        debug.log('Send log', '...');

        page_log.open(config.url.send_data, 'POST', data, function (status) {
            cb();
        });

    }else{

        cb();

    }

};
