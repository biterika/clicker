var config  = require('./../inc/config');
var param   = require('./../inc/param');
var random  = require('./../lib/random');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

var random_thumb    = require('./random_thumb').exec;
var click_xy        = require('./click_xy').exec;
var check_pages     = require('./check_pages').exec;
//var make_screen = require('./../actions/make_screen2file').exec;


exports.exec = function(obj){
    debug.log('');

    var obj_main = false;
    var obj_for_click = false;

    var deferred = Q.defer();

    //wait timeout before click
    setTimeout(function(){

        if(!obj_main){
            obj_main = obj;
        }
        if(!obj_for_click){
            obj_for_click = obj_main;
        }

        if(config.const.debug){
            //make_screen(obj_for_click);
        }

        var max_id = 0;
        for(var id in obj.pages){
            if(obj.pages.hasOwnProperty(id)){
                debug.log(obj.pages[id].id, obj.pages[id].domain, obj.pages[id].url);
                if(obj.pages[id].domain == param.task.trader && obj.pages[id].id > max_id){
                    max_id = obj.pages[id].id;
                    obj_for_click = obj.pages[id];
                }
            }
        }

        //console.log('obj_for_click',obj_for_click.url,obj_for_click.content.length);

        //var filename = '/usr/share/biterika/screen/'+Date.now()+'-'+obj.id+'-'+random.char(4)+'.jpg';
        //debug.log('render '+filename);
        //obj.render(filename, {format: 'jpeg', quality: '10'});

        //is a thumbs have in page?
        var thumb = random_thumb(obj_for_click);
        if(!thumb.x || !thumb.y){

            /*setTimeout(function(){
                make_screen(obj_for_click);
            }, 200);*/

            debug.log('click', '!thumb.x || !thumb.y');
            debug.log('url',obj_for_click.url);
            //debug.log('content',obj_for_click.content);

            obj_for_click = obj_main;

            deferred.resolve({
                status: 'error'
            });

        }else{

            param.have_thumb++;

            var x = random.int(thumb.x+5,thumb.x+thumb.width-5);
            var y = random.int(thumb.y+5,thumb.y+thumb.height-5);

            param.popupLoading = false;
            param.page_created = false;

            click_xy(obj_for_click, x, y, 'middle');

            var timeout_wait1 = setTimeout(function(){
                if(!param.page_created){

                    debug.log('click', 'elementFromPoint');
                    obj_for_click.evaluate(function (x, y) {

                        function rand(min,max) {
                            return Math.floor(Math.random() * (max - min + 1)) + min;
                        }
                        function getOffsetRect(elem) {
                            var box = elem.getBoundingClientRect();
                            var body = document.body;
                            var docElem = document.documentElement;
                            var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
                            var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
                            var clientTop = docElem.clientTop || body.clientTop || 0;
                            var clientLeft = docElem.clientLeft || body.clientLeft || 0;
                            var top  = box.top +  scrollTop - clientTop;
                            var left = box.left + scrollLeft - clientLeft;
                            var width = elem.offsetWidth;
                            var height = elem.offsetHeight;
                            return { el:elem, x: Math.round(left), y: Math.round(top), width: width, height: height }
                        }
                        var thumb_top_res = [];
                        var mass = document.querySelectorAll("a img");
                        var sizes = {};
                        for (var i = 0; i < mass.length; i++) {
                            if(mass[i].height >= 50){
                                var rect = getOffsetRect(mass[i]);
                                var size = rect.width + 'x' + rect.height;
                                if (typeof sizes[size] == 'undefined') sizes[size] = [];
                                sizes[size].push(rect);
                            }
                        }
                        var len = 0;
                        for (var size in sizes) {
                            if (len < sizes[size].length) {
                                len = sizes[size].length;
                                thumb_top_res = sizes[size].slice(0, 10);
                            }
                        }

                        var ev = document.createEvent("MouseEvent");
                        var el = thumb_top_res[rand(0,thumb_top_res.length-1)].el.parentNode;
                        el.setAttribute('target', '_blank');
                        ev.initMouseEvent(
                            "click",
                            true /* bubble */, true /* cancelable */,
                            window, null,
                            0, 0, 0, 0, /* coordinates */
                            false, false, false, false, /* modifier keys */
                            0 /*left*/, null
                        );
                        el.dispatchEvent(ev);

                    }, x, y);

                }
            },100);

            //wait timeout before open popups
            setTimeout(function(){

                //wait when popup loading will be finished
                waitFor(function(){
                    return !param.popupLoading;
                }, function(){

                    obj_for_click = check_pages(obj_main);

                    deferred.resolve({
                        status: 'success'
                    });

                }, config.timeout.loading_popup);

            }, config.timeout.wait_popup);

        }

    }, config.timeout.before_click);

    return deferred.promise;

};

//wait for something
function waitFor(testFx, onReady, timeOut, timeFrequency) {
    var maxtimeOutMillis = timeOut ? timeOut : 3000,
        start = new Date().getTime(),
        condition = false,
        interval = setInterval(function() {
            if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx());
            } else {
                if(!condition) {
                    //console.log("'waitFor()' timeout");
                    clearInterval(interval);
                    typeof(onReady) === "string" ? eval(onReady) : onReady();
                } else {
                    //console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                    clearInterval(interval);
                    typeof(onReady) === "string" ? eval(onReady) : onReady();
                }
            }
        }, timeFrequency);
}
