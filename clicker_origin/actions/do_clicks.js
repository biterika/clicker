var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

var make_click  = require('./make_click').exec;

exports.exec = function(obj, x, y, button){

    var deferred = Q.defer();

    var task_finished = false;
    var try_count_max = config.const.try_count_max + param.task.click_task;
    var click_task = param.task.click_task;

    var try_count = 0;
    promiseWhile(function () { return !task_finished; }, function (result) {

        try_count++;

        if(try_count <= try_count_max){
            debug.log('');
            debug.log('Running task try:', try_count+'/'+try_count_max, 'clicks:', param.task.click_ok+'/'+click_task);
        }

        //is it enough?
        if(param.task.click_ok >= click_task){
            deferred.resolve({
                status: 'success'
            });
            task_finished = true;
            return;
        }

        //reached the limit?
        if(try_count > try_count_max){
            deferred.resolve({
                status: 'error'
            });
            task_finished = true;
            return;
        }

        return make_click(obj);

    }).then(function () {

        deferred.resolve({
            status: 'error'
        });

    }).done();

    return deferred.promise;

};

//while loop with promises
function promiseWhile(condition, body) {
    var done = Q.defer();
    function loop() {
        if (!condition()) return done.resolve();
        Q.when(body(), loop, done.reject);
    }
    Q.nextTick(loop);
    return done.promise;
}
