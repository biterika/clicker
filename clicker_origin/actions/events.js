var domain  = require('./../lib/domain');
var debug   = require('./../lib/debug');
var random  = require('./../lib/random');
var config  = require('./../inc/config');
var uagent  = require('./../inc/uagent');
var screens = require('./../inc/screen');
var param   = require('./../inc/param');
var Q       = require('./../modules/q/q.js');



var timers = {};
var useragent = uagent.list[random.int(0,uagent.list.length-1)];
var screen = screens.list[random.int(0,screens.list.length-1)].split('x');
param.task.sx = screen[0];
param.task.sy = screen[1];

var url_actions = {};

debug.log('screen',screen,'useragent',useragent);

exports.exec = function(obj){

    param.step++;

    debug.log('');
    debug.log('Apply events...');

    var deferred = Q.defer();

    onPageCreated(obj);

    var result = {
        status: 'success'
    };
    deferred.resolve(result);

    return deferred.promise;

};

exports.setTimeoutStop = function(obj, timeout){

    setTimeout(function(){
        if(typeof obj != 'undefined' && obj){
            try{
                obj.stop();
            }catch (e){
            }
        }
    }, timeout);

};

function onPageCreated(obj){

    if(param.close_popup){

        debug.log('  ', 'Event:', 'ClosePopup');
        obj.close();

    }else{

        param.page_id++;
        debug.log('  ', 'DEBUG URL Created', param.page_id);

        param.popupLoading = true;
        param.page_created = true;

        obj.domain = domain.getDomain(obj.url);
        obj.id = param.page_id;
        obj.requests = 0;
        obj.changed = 0;


        url_actions[obj.id] = {
            page_id: obj.id,
            code: 0,
            url: '',
            domain: '',
            refer_url: '',
            refer_domain: '',
            createdAt: new Date(),
            redirect: []
        };

        obj.viewportSize = {
            width: param.task.sx||1366,
            height: param.task.sy||768
        };

        obj.settings.userAgent = useragent;
        obj.settings.webSecurityEnabled = false;

        var platform = '';
        if(useragent.indexOf("welcome")){

        }

        obj.onInitialized = function () {
            obj.evaluate(function (sx, sy,lang,platform) {
                window.screen = {
                    width: sx,
                    height: sy,
                    colorDepth: 24
                };
                window.navigator.__defineGetter__('javaEnabled', function () {
                    return function() { return true; };
                });
                var newNavigator = Object.create(window.navigator);
                newNavigator._userLanguage = lang;
                newNavigator.language = lang;
                newNavigator.platform = platform;
                window.navigator = newNavigator;
            }, param.task.sx||1366, param.task.sy||768, param.task.lang||'en-US',platform||'Win32');
            debug.log('Set JS variables',param.task.sx||1366, param.task.sy||768, param.task.lang||'en-US',platform||'Win32');
        };

        timers[obj.id] = setTimeout(function(){
            if(typeof obj != 'undefined' && obj){
                try{
                    obj.stop();
                }catch (e){
                }
            }
        }, config.timeout.loading);

        obj.onError = function(msg, trace) {
            var msgStack = ['ERROR: ' + msg];
            if (trace && trace.length) {
                msgStack.push('TRACE:');
                trace.forEach(function(t) {
                    msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function +'")' : ''));
                });
            }
            debug.error(msgStack.join('\n'));
        };

        obj.onPageCreated = function(obj){
            onPageCreated(obj);
        };
        obj.onResourceRequested = function(requestData, networkRequest) {
            onResourceRequested(obj,requestData,networkRequest);
        };
        obj.onResourceReceived = function(response) {
            onResourceReceived(obj,response);
        };
        obj.onUrlChanged = function(targetUrl) {
            onUrlChanged(obj,targetUrl);
        };
        obj.onClosing = function(closingPage) {
            onClosing(closingPage);
        };
        obj.onLoadFinished = function(status) {
            onLoadFinished(obj,status);
        };
        obj.onConsoleMessage = function(msg,lineNum,sourceId) {
            onConsoleMessage(obj,msg,lineNum,sourceId)
        };
    }
}

function onResourceRequested(obj,requestData,networkRequest){

    if(!param.task.task_type && requestData.url.indexOf('/redirect_task.html?') >= 0){

        param.args = qsParseArray(requestData.url);

        /*param.task.task_type = getQueryVariable(requestData.url,'action');
        if(!param.task.task_type || param.task.task_type == ''){
            param.task.task_type = getQueryVariable(requestData.url,'task');
        }*/

        //param.task.ip = getQueryVariable(requestData.url,'ip');
        //param.task.fake = getQueryVariable(requestData.url,'fake');
        //param.task.token_cache = getQueryVariable(requestData.url,'token_cache');

        param.task.task_type = getQueryVariable(requestData.url,'action');
        param.task.site = domain.getDomain(getQueryVariable(requestData.url,'site'));
        param.task.trader = domain.getDomain(getQueryVariable(requestData.url,'trader'));
        param.task.param = domain.getDomain(getQueryVariable(requestData.url,'param'));
        param.task.lang = domain.getDomain(getQueryVariable(requestData.url,'lang').split(',')[0]);
        param.task.useragent = domain.getDomain(getQueryVariable(requestData.url,'useragent'));

        useragent = param.task.useragent;

        debug.log('  ', 'Set task_type:', param.task.task_type, JSON.stringify(param.args));
        debug.log('  ', 'Set task:', JSON.stringify(param.task));

    }

    //debug.log('  ', 'Event:', 'Requested in page:', obj.id, requestData.url);
    //debug.log('  ', 'freeze', obj.freeze);

    if(requestData.url == 'data:text/html,%3Cscript%3Ewindow.close();%3C/script%3E'){
        networkRequest.abort();
        debug.log('  ', 'Event:', 'Requested in page: ABORT', obj.id, requestData.url);
    }

    //obj.freeze

    //if(config.const.debug == true){
    var tmp = requestData.url.split(".");
    var ext = tmp[tmp.length-1];
    if(ext != 'gif' && ext != 'jpg' && ext != 'png' && ext != 'jpeg' && ext != 'js' && ext != 'css') {

        debug.log('  ', 'Event:', 'Requested in page:', obj.id, requestData.url);

        if (url_actions[obj.id]){
            //url_actions[obj.id].requested = requestData.url;

            for(var id in requestData.headers){
                if(requestData.headers.hasOwnProperty(id)){
                    if(requestData.headers[id].name.toLowerCase() == 'referer'){
                        //debug.log('  ', 'DEBUG URL >>:', obj.id, 'Referer', requestData.headers[id].value);
                        url_actions[obj.id].refer_url = requestData.headers[id].value;
                        url_actions[obj.id].refer_domain = domain.getDomain(requestData.headers[id].value);
                    }
                }
            }

        }

    }
    //}
    //debug.log('  ', 'Event:', 'Requested in page:', obj.id, requestData.url);
    obj.requests++;

}

function onResourceReceived(obj,response){

    var tmp = response.url.split(".");
    var ext = tmp[tmp.length-1];
    if(ext != 'gif' && ext != 'jpg' && ext != 'png' && ext != 'jpeg' && ext != 'js' && ext != 'css') {

        //debug.log('  ', 'DEBUG URL <<:', obj.id, response.status, response.url);

        if (url_actions[obj.id]){
            //url_actions[obj.id].received = response.url;

            url_actions[obj.id].code = response.status;

            for(var id in response.headers){
                if(response.headers.hasOwnProperty(id)){
                    if(response.headers[id].name.toLowerCase() == 'location'){
                        //debug.log('  ', 'DEBUG URL <<:', obj.id, 'Location', response.headers[id].value);
                        url_actions[obj.id].redirect.push({
                            refer: response.url,
                            url: response.headers[id].value
                        });
                        /*url_actions[obj.id].url = response.headers[id].value;
                        url_actions[obj.id].domain = domain.getDomain(response.headers[id].value);*/
                    }
                }
            }

        }

        /*console.log(obj.content);
        for(var id in response.headers){
            if(response.headers.hasOwnProperty(id)){
                debug.log('  ', '  ', response.headers[id].name, response.headers[id].value);
            }
        }*/
    }

    //if(response.status != 200)
    //    debug.log('  ', 'Event:', 'Received in page:', obj.id, 'status:', response.status, 'url:', response.url);
}

function check_domain(obj){

    if(!param.task.tds){

        debug.log('  ', 'set tds domain',obj.domain);
        //console.log('  ', 'set tds domain',obj.domain);
        param.task.tds = obj.domain;

    }else if(!param.task.site){

        debug.log('  ', 'set site domain',obj.domain);
        //console.log('  ', 'set site domain',obj.domain);
        param.task.site = obj.domain;

    }else if(obj.domain == param.task.site){

        debug.log('  ', 'skip domain',obj.domain);
        //console.log('  ', 'skip domain',obj.domain);

    }else{

        debug.log('  ', 'Set trader domain',obj.domain);
        //console.log('  ', 'Set trader domain',obj.domain);
        param.task.trader = obj.domain;

    }

}

function onUrlChanged(obj,targetUrl){

    /*if(targetUrl.indexOf('http://no.task/') >= 0){
        throw new Error('NO_TASK');
    }*/
    obj.changed++;
    param.popupLoading = true;

    obj.domain = domain.getDomain(targetUrl);

    url_actions[obj.id].url = targetUrl;
    url_actions[obj.id].domain = domain.getDomain(targetUrl);

    debug.log('  ', 'DEBUG URL ==:', obj.id, targetUrl);

    if(param.task.task_type && param.task.task_type.substr(0,7) == 'trader.'){
        if(param.task.trader && obj.domain != param.task.trader && obj.domain != param.task.site && targetUrl != 'about:blank' && obj.domain != '185.31.210.60'){
            debug.log('  ', 'Event:', 'STOP', obj.id, targetUrl);
            obj.stop();
        }
    }

}

function onLoadFinished(obj,status){

    if(url_actions[obj.id]){

        var log = {
            page_id: url_actions[obj.id].page_id,
            code: url_actions[obj.id].code,
            url: url_actions[obj.id].url,
            domain: url_actions[obj.id].domain,
            refer_url: url_actions[obj.id].refer_url,
            refer_domain: url_actions[obj.id].refer_domain,
            createdAt: url_actions[obj.id].createdAt,
            content: obj.content.length,
            redirect: url_actions[obj.id].redirect
        };
        param.url_log.push(log);
        //debug.json(JSON.stringify(log));
        url_actions[obj.id].code = 0;
        url_actions[obj.id].url = '';
        url_actions[obj.id].domain = '';
        url_actions[obj.id].refer_url = '';
        url_actions[obj.id].refer_domain = '';
        url_actions[obj.id].redirect = [];

    }

    if(!param.task.trader){

        //check_domain(obj);

    }else{

        clearTimeout(timers[obj.id]);
        delete timers[obj.id];
        obj.freeze = true;
        //obj.navigationLocked = true;

        param.popupLoading = false;

        debug.log('  ', 'Event:', 'LoadFinished', obj.id, 'requests:', obj.requests, 'content len:', obj.content.length);
        //debug.log('  ', obj.content);

        if(obj.content.length < 1500){

            debug.log();
            debug.log('content:');
            debug.log('==================================================================================');
            debug.log(obj.content);
            debug.log('==================================================================================');
            debug.log();

        }

    }

}
function onClosing(closingPage){

    debug.log('  ', 'Event:', 'Closing', closingPage.id, closingPage.url);

}
function onConsoleMessage(obj,msg,lineNum,sourceId){

    debug.log('  ', 'ConsoleMessage:', msg);

}

function getQueryVariable(query,variable) {
    var vars = query.split("?")[1].split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
}

function qsParseArray(query) {
    var res = [];
    var vars = query.split("?")[1].split("&");
    for (var i = 0; i < vars.length; i++) {
        res.push(vars[i].split("="));
    }
    return res;
}
