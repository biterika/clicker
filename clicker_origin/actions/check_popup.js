var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var random  = require('./../lib/random');
var Q       = require('./../modules/q/q.js');

var click_xy   = require('./click_xy').exec;

exports.exec = function(obj){
    param.step++;
    debug.log('');
    debug.log('Check for popup...');

    var deferred = Q.defer();

    param.close_popup = true;

    click_xy(obj, random.int(1,300), random.int(1,300), 'middle');

    setTimeout(function(){

        param.close_popup = false;
        var result = {
            status: 'success'
        };
        deferred.resolve(result);

    }, config.timeout.wait_popup);

    return deferred.promise;

};
