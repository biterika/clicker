var config  = require('./../inc/config');
var param   = require('./../inc/param');
var postload = require('./../inc/postload');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

var click_selector = require('./click_selector').exec;

exports.exec = function(obj){

    param.step++;

    debug.log('');
    debug.log('Postloader for:', param.task.domain);

    var deferred = Q.defer();

    if(postload.click[param.task.domain]){

        var selector = postload.click[param.task.domain].selector;

        //debug.log('postload.click',selector,'\n',obj.content);

        Q.fcall(function(){

            return click_selector(obj, selector, 'left');

        }).then(function (result) {

            debug.log(result);

            setTimeout(function(){

                result = {
                    status: 'success'
                };
                deferred.resolve(result);
                return;

            }, config.timeout.before_click);


        }).catch(function (e) {

            if(e.message){
                debug.error('ERROR:', e.message, config.errors[e.message]);
            }
            result = {
                status: 'error1'
            };
            deferred.resolve(result);
            return;


        }).done(function(){
        });

    }else{

        result = {
            status: 'error'
        };
        deferred.resolve(result);

    }

    return deferred.promise;

};
