var config  = require('./../inc/config');
var param   = require('./../inc/param');
var preload = require('./../inc/preload');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

var click_selector = require('./click_selector').exec;

exports.exec = function(obj){

    param.step++;

    debug.log('');
    debug.log('Preloader for:', param.task.domain);

    var deferred = Q.defer();

    if(preload.cookies[param.task.domain]){

        var cookies = preload.cookies[param.task.domain];
        for(var id in cookies){
            if(cookies.hasOwnProperty(id)){
                var name = cookies[id].name;
                var val = cookies[id].val;
                debug.log('  ', 'Add cookie:', name, '=', val);
                phantom.addCookie({
                    'name'     : name,
                    'value'    : val,
                    'domain'   : '.' + param.task.domain
                });
            }
        }

        result = {
            status: 'success'
        };
        deferred.resolve(result);
        return;

    }

    result = {
        status: 'error'
    };
    deferred.resolve(result);

    return deferred.promise;

};
