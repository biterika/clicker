var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

exports.exec = function(obj, x, y, button){
    debug.log('Click :', x, y, 'button:', button, 'in page:', obj.id, obj.url);

    var deferred = Q.defer();

    /*if(config.const.debug){

        var rect = {
            top: y-50,
            left: x-50,
            width: 100,
            height: 100
        };
        make_screen_rect(obj, rect);

    }*/

    obj.sendEvent('click', x, y, button);

    var result = {
        status: 'success'
    };
    deferred.resolve(result);

    return deferred.promise;


};
