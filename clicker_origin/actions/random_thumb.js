var config  = require('./../inc/config');
var param   = require('./../inc/param');
var random  = require('./../lib/random');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

exports.exec = function(obj){

    var thumbs_all = [];
    if(typeof obj != 'undefined' && obj){
        thumbs_all = obj.evaluate(function(thumb_top_count) {
            function getOffsetRect(elem) {
                var box = elem.getBoundingClientRect();
                var body = document.body;
                var docElem = document.documentElement;
                var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
                var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
                var clientTop = docElem.clientTop || body.clientTop || 0;
                var clientLeft = docElem.clientLeft || body.clientLeft || 0;
                var top  = box.top +  scrollTop - clientTop;
                var left = box.left + scrollLeft - clientLeft;
                var width = elem.offsetWidth;
                var height = elem.offsetHeight;
                return { x: Math.round(left), y: Math.round(top), width: width, height: height }
            }
            var thumb_top_res = [];
            var mass = document.querySelectorAll("a img");
            var sizes = {};
            for (var i = 0; i < mass.length; i++) {
                if(mass[i].height >= 50){
                    var rect = getOffsetRect(mass[i]);
                    if(rect.x > 0 && rect.y > 0 && rect.width > 0 && rect.height > 0){
                        console.log(JSON.stringify(rect));
                        var size = rect.width + 'x' + rect.height;
                        if (typeof sizes[size] == 'undefined') sizes[size] = [];
                        sizes[size].push(rect);
                    }
                }
            }
            var len = 0;
            for (var size in sizes) {
                if (len < sizes[size].length) {
                    len = sizes[size].length;
                    thumb_top_res = sizes[size].slice(0, thumb_top_count);
                }
            }
            return thumb_top_res;
        }, config.const.thumb_top_count);
    }

    if(thumbs_all.length < 1) {

        debug.log('Thumbs not found');
        return {};
    }
    else{

        return thumbs_all[random.int(0, thumbs_all.length-1)];

    }

};
