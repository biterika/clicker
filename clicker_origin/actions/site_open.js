var config  = require('./../inc/config');
var param   = require('./../inc/param');
var domain  = require('./../lib/domain');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');



var make_screen = require('./make_screen').exec;

exports.exec = function(page){

    param.step++;

    debug.log('');
    debug.log('Open site:', param.task_url);

    var deferred = Q.defer();

    try {

        var result = {};
        page.open(param.task_url, function (status) {
            debug.log('First site open:', status);
            if (page.url.indexOf('no.task') >= 0) {

                result = {status: 'NO_TASK'};
                deferred.resolve(result);

            } else if (page.domain == 'bad.proxy') {

                result = {status: 'BAD_PROXY'};
                deferred.resolve(result);

            } else if (status == 'success') {

                check();

            } else {

                result = {status: 'TASK_GET_FAIL'};
                deferred.resolve(result);

            }
        });

        var check_count = 0;

        function check() {

            debug.log('  ', 'check:', check_count, 'changed:', page.changed, 'domain:', page.domain, 'loading:', param.popupLoading, 'site:', param.task.site, 'trader:', param.task.trader);
            debug.log('  ', 'cookies:', JSON.stringify(phantom.cookies));

            if(page.domain == 'no.task' || param.task.site == 'no.task'){
                deferred.resolve({status: 'NO_TASK'});
            }else if(page.domain == 'request.error' || param.task.site == 'request.error'){
                deferred.resolve({status: 'REQUEST_ERROR'});
            }else{
                setTimeout(function () {
                    check_count++;
                    //TODO  && page.domain != param.task.site
                    if (page.changed >= 3 && page.domain == param.task.trader && !param.popupLoading && param.task.site != 'blank') {
                        finish();
                    } else {
                        if (check_count > 60) {
                            //TODO  && param.task.site != param.task.trader
                            if (page.changed >= 3 && page.domain == param.task.trader && param.task.site != 'blank') {
                                debug.log('Open trader half success', page.content.length);
                                debug.log('Open trader half success', page.content);
                                result = {status: 'success'};
                                deferred.resolve(result);
                            } else {
                                debug.log('Open trader error', page.content.length);
                                debug.log('Open trader error', page.content);
                                result = {status: 'TASK_GET_TIMEOUT'};
                                deferred.resolve(result);
                            }
                        } else {
                            check();
                        }
                    }
                }, 1000);
            }

        }

        function finish() {
            debug.log('Open trader success', page.content.length, 'wait:', config.timeout.trader_after_open/1000+'s');
            setTimeout(function () {
                //debug.log('content',page.content);
                //make_screen(page);
                result = {status: 'success'};
                deferred.resolve(result);
            }, config.timeout.trader_after_open);
        }

    }catch(e){
        deferred.resolve(e.message);
    }

    return deferred.promise;

};
