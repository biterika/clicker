# clicker-headless
####INSTALL:

***Ubuntu 16.04 LTS (Xenial Xerus)***

1. установить timezone
```
dpkg-reconfigure tzdata

```

2. cmd+c - cmd-v в консоль
```
apt-get update

apt-get install -y language-pack-en-base software-properties-common \
python-software-properties build-essential upstart
```

3. cmd+c - cmd-v в консоль
```
apt-get update

add-apt-repository main -y
add-apt-repository universe -y
add-apt-repository restricted -y
add-apt-repository multiverse -y
```

4. cmd+c - cmd-v в консоль
```
apt-get update

apt-get install mc htop atop iftop \
    git ntp psmisc curl unzip wget nano -y
```

5. cmd+c - cmd-v в консоль
```
apt-get install -y g++ flex bison gperf ruby perl \
  libsqlite3-dev libfontconfig1-dev libicu-dev \
  libfreetype6 libssl-dev \
 libpng-dev libjpeg-dev python libx11-dev libxext-dev
```

6. тут в диалоге нужно согласится с лицензией
```
apt-get install -y ttf-mscorefonts-installer
```

7. php7
```
add-apt-repository ppa:ondrej/php -y
LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php -y

apt-get update

apt-get install  -y php7.0-common php7.0-cli \
    php7.0-curl \
    php7.0-gd \
    php7.0-json \
    php7.0-mcrypt \
    php7.0-mysql \
    php7.0-readline \
    php7.0-mbstring \
    php7.0-bcmath \
    php7.0-sqlite3 \
    php7.0-curl
```


8. сам продукт (нужно ввести пароль git-репозитария от biterika-user)
```
mkdir -p /usr/share/biterika/clicker-headless2
cd /usr/share/biterika/clicker-headless2
git init
git remote add origin https://biterika-user@bitbucket.org/biterika/clicker.git
git fetch origin
git checkout -b master --track origin/master
git fetch --all && git reset --hard origin/master
```

9. прописать в crontab -e
```
# установка лок-файла для предотвращения взятия новых заданий
25 12 * * * touch /usr/share/biterika/clicker-headless2/lock

# перезагрузка сервера
30 12 * * * /sbin/reboot

# удаление лок-файла
39 12 * * * rm /usr/share/biterika/clicker-headless2/lock

# запуск 50 инстансов
40 12 * * * nohup /usr/share/biterika/clicker-headless2/run_demon.sh 50 >/dev/null 2>&1 &
```

10. прописать ip этого сервера в proxy-node в конфиге haproxy


###Start (50 штук)
```
nohup /usr/share/biterika/clicker-headless2/run_demon.sh 50 >/dev/null 2>&1 &
```

###Stop
```
nohup /usr/share/biterika/clicker-headless2/run_demon.sh STOP >/dev/null 2>&1 &
```

###Update
(нужно ввести пароль git-репозитария от biterika-user)
```
nohup /usr/share/biterika/clicker-headless2/run_demon.sh STOP >/dev/null 2>&1 &
cd /usr/share/biterika/clicker-headless2
rm -rf .git
git init
git remote add origin https://biterika-user@bitbucket.org/biterika/clicker.git
git fetch --all && git reset --hard origin/master
```


###Config
clicker-php/config.php
```
    'TASK_GET_URL' => 'http://get.task', 
    'TASK_FINISH_URL' => 'http://c.cleanburg.com/postback?',

    'TASK_FINISH_URL_PROXY' => 'http://finish.task',

    'PROXY_USER' => 'phantom',
    'PROXY'=> '163.172.160.155:789', // proxy-node
    'WORK_DIR' => '/usr/share/biterika/clicker-headless2/',
    'LOCK_FILE' => '/usr/share/biterika/clicker-headless2/lock',
```

### Получение задания
- кликер обращается в proxy-node по адресу http://get.task/ (прописано в конфиге выше) и передает в качестве пароля к прокси свой token (уникальный для задания), к токену подбирается и привязывается внешний прокси через который кликер будет ходить
- proxy-node конвертирует этот URL в http://tds.tds.master.cleanburg.com/ctask_new? (задается в конфиге proxy-node router)
- получение задания из раздачи заданий осуществляет proxy-node
- к раздаче заданий proxy-node обращается в следующем формате:
    ```http://tds.tds.master.cleanburg.com/ctask_new?&p={proxy_proider_name}&token={clicker_token}```

- от раздачи заданий ожидается 302-ой редирект на URL формата
```http://go.task/?go=http://target-site.com&referer=refer-site.com&action=trader.usb&task_uniq_id={subid}&ip={ip}&country={country}&tz={time_zone}```
где, 
- go - url на который нужно пойти
- referer - домен рефера с которым нужно прийти на указанный выше url
- action - название сценария который нужно выполнить (trader.usb, popunder.biterika, skim.biterika),
- task_uniq_id - уникальный id задачи (позже с этим id будет произведен postback-отчет)
- ip - ip-адрес с которым пришли на url получения задания
- country - двух-буквенное обозначение страны ip адреса (пример RU)
- tz - time-зона ip-адреса (по ней кликер выставит свою тайм зону в JS), пример Asia/Bangkok


### Postback отчет о выполнении задания
После выполнения сценария кликером, запустившая его php-шка делает отчет о выполнении задания (в тч и если кликер завис/аварийно завершился) на адрес указанный в конфиге
'TASK_FINISH_URL' => 'http://c.cleanburg.com/postback?',
со следующими параметрами:

- task_result - буквенный код результат выполнения задачи согласно сценарию или код аварийного завершения DIE_CRASH (выход кликера до завершения задания) DIE_HANGUP (зависание кликера - более 50 сек кликер ничего не писал в консоль)
- subid - полученный ранее от раздачи заданий task_uniq_id
- task_report - JSON объект закодированный url_encode - может содержать опциональные параметры согласно сценарию, а также следующие общие:
  * runtime_sec - время работы php и кликера по данному заданию
  * history - массив URL через которые прошел кликер
  * navigator_id - id объекта навигатор, с которым работал кликер (пример ru/ru/undefined/windows/7/undefined/undefined/chrome/51/amd64__51.0.2704.103__95.json)
  * task_server - id сервера на котором работал кликер
  * token - токен кликера по которому proxy-node привязывал внешний прокси к данному заданию
  * task_click_ok - кол попыток кликов
  * task_click_content - кол-во кликов по внутренним ссылкам (для трейдер сценария)
  * task_click_external - кол-во кликов по внутренним ссылкам (для трейдер сценария)


###Запуск в debug-mode для откладки сценариев
развернуть локально proxy-node
```
mkdir /usr/share/biterika/proxy-node
cd /usr/share/biterika/proxy-node

git init
git remote add origin https://biterika-user@bitbucket.org/biterika/proxy.git
git fetch origin
git checkout -b master --track origin/master
git fetch --all && git reset --hard origin/master
```
запустить эмулятор прокси-ноды
```
cd /usr/share/biterika/proxy-node/proxy/
node router_emulator.js
```
запустить кликер
```
php clicker-demon.php 127.0.0.1:8080 'http://ftt2.biterika.com' 'trader.usb'
```
где, 127.0.0.1:8080 адрес эмулятора прокси-ноды, сайт на который идем, сценарий который выполняем

### настройка less и просмотр креш-логов c ASCII escape-символами (цвета текста и фона) ####
```
nano ~/.lessfilter

#!/bin/sh
case "$1" in
    *.awk|*.groff|*.java|*.js|*.m4|*.php|*.pl|*.pm|*.pod|*.sh|\
    *.ad[asb]|*.asm|*.inc|*.[ch]|*.[ch]pp|*.[ch]xx|*.cc|*.hh|\
    *.lsp|*.l|*.pas|*.p|*.xml|*.xps|*.xsl|*.axp|*.ppd|*.pov|\
    *.diff|*.patch|*.py|*.rb|*.sql|*.ebuild|*.eclass)
        pygmentize -f 256 "$1";;
    .bashrc|.bash_aliases|.bash_environment)
        pygmentize -f 256 -l sh "$1"
        ;;
    *)
        grep "#\!/bin/bash" "$1" > /dev/null
        if [ "$?" -eq "0" ]; then
            pygmentize -f 256 -l sh "$1"
        else
            exit 1
        fi
esac

exit 0

# ----------------

nano ~/.bashrc

export LESS='-R'
export LESSOPEN='|~/.lessfilter %s'

# ----------------

chmod u+x ~/.lessfilter

source ~/.bashrc
```
пример, обнаруженной записи в алерт-сервисе (отдельный проект - журнал):
```
	srv_clicker-new-2	DIE_HANGUP on site log: /usr/share/biterika/clicker-headless2/clicker-php/crash_log/crash_2017-01-25_16-45-39.log
```
	
пример, просмотра лога выполнения задания (из записи выше) на сервере с хостом clicker-new-2, которое принудительно завершившилось с ошибкой DIE_HANGUP (завис)
```
less /usr/share/biterika/clicker-headless2/clicker-php/crash_log/crash_2017-01-25_16-45-39.log
```

### удаление логов ###
```
rm /usr/share/biterika/clicker-headless2/clicker-php/*.log
rm /usr/share/biterika/clicker-headless2/clicker-php/crash_log/*.log
```

