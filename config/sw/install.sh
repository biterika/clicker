#!/bin/bash

sed -i -e '$i export http_proxy=http://10.1.20.150:777 https_proxy=http://10.1.20.150:777\n' ~/.bashrc
sed -i -e '$i /usr/share/biterika/clicker-headless2/autorun.sh >/dev/null &\n' /etc/rc.local

apt-get install libfreetype6 libfontconfig1 -y

reboot
